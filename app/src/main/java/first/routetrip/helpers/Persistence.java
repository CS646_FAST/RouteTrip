package first.routetrip.helpers;

import android.content.Context;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.support.annotation.Nullable;
import android.util.Log;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;

import first.routetrip.helpers.sql.TripDatabase;

/**
 * Object containing helper methods for persisting data:
 * 1. Serializing/De-Serializing objects into/from byte arrays
 * 2. Storing/Retrieving byte arrays to/from a device's internal storage
 */

public class Persistence {

    private static final String DEBUG_TAG = Persistence.class.getSimpleName();
    private Context mContext;

    public Persistence(Context context) {
        this.mContext = context; //we need context to access internal storage
    }

    @Nullable
    public byte[] serialize(Object object) {
            /* This method serializes a provided object into a byte array */

        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        try {
            ObjectOutput objectOutput = new ObjectOutputStream(byteArrayOutputStream);
            objectOutput.writeObject(object);
            objectOutput.close();
            return byteArrayOutputStream.toByteArray();
        } catch (IOException e) {
            Log.e(DEBUG_TAG, "IO error serializing objects " + e.toString());
            return null;
        }
    }

    @Nullable
    public Object deSerialize(byte[] serializedObject) {

            /* Deserialize the route; credits: http://stackoverflow.com/questions/7803762/storing-large-arraylists-to-sqlite */

        Object object = null;
        try {
            ObjectInputStream in = new ObjectInputStream(new ByteArrayInputStream(serializedObject));
            object = in.readObject();
            in.close();
            return object;
        } catch (ClassNotFoundException e) {
            Log.e(DEBUG_TAG, "Failed to de-serialize object; not found! " + e.toString());
            return null;
        } catch (IOException e) {
            Log.e(DEBUG_TAG, "IO error de-serializing objects! " + e.toString());
            return null;
        }

    }

    public boolean storeToInternalStorage(byte[] serializedObject, String FILENAME) {
            /* Saved a serialized object (as a byte-array) into a file and stores it
                onto device's internal storage */

        try {
            FileOutputStream fileOutputStream = mContext
                    .openFileOutput(FILENAME, Context.MODE_PRIVATE);
                /* MODE_PRIVATE will create the file (or replace a file of the same name) and
                    make it private to your application */
            fileOutputStream.write(serializedObject);
            fileOutputStream.close();

            //tmp for debug
            String[] storedFiles = mContext.fileList(); //tmp; see stored strings:

            return true;
        } catch (IOException e) {
            Log.e(DEBUG_TAG, "IO error serializing objects " + e.toString());

            return false;
        }
    }

    @Nullable
    public byte[] readFromInternalStorage(String FILENAME) {
            /* Reads a saved serialized object from internal storage as
                specified by the FILENAME. Returns a byte[] array representation
                 of the serialized object. Will need to de-serialize */

        try {

            //tmp for debug
            String[] storedFiles = mContext.fileList(); //tmp; see stored strings:

                /* Read serialized route object from device's internal storage */
            FileInputStream fileInputStream = mContext
                    .openFileInput(FILENAME);
            byte[] serializedObject = new byte[(int) fileInputStream.getChannel().size()]; //read fill buffer size
            //noinspection ResultOfMethodCallIgnored
            fileInputStream.read(serializedObject);
            fileInputStream.close();
            return serializedObject;

        } catch (FileNotFoundException e) {
            Log.e(DEBUG_TAG, "Could not find saved route! " + e.toString());
            return null;
        } catch (IOException e) {
            Log.e(DEBUG_TAG, "IO error de-serializing objects! " + e.toString());
            return null;
        }
    }

    public boolean storeSavedRouteToDB(String FILENAME) {
        /* This method stores a UNIQUE string (a fileName) into a database exposed by
            the dbHelper object. */

        /* Right now, it's used to store the fileName of a saved Route that was stored
            onto the device's internal storage. We save the FileName into
             the database's SavedTrips table so we may access the
              route from internal storage at a later point in time. */

        try {
            TripDatabase dbHelper = new TripDatabase(mContext);
            dbHelper.getWritableDatabase();
            if (!dbHelper.storeSavedRouteFileName(FILENAME)) {
                dbHelper.close();
                return false;
            }
            dbHelper.close();
            return true;
        } catch (SQLException e) {
            Log.e(DEBUG_TAG, "Failed to get WritableDatabase access");
            return false;
        }

    }

    public boolean storeCurrentRouteToDB(String FILENAME) {
        /* This method stores a UNIQUE string (a fileName) into a database exposed by
            the dbHelper object.
            NOTE: CurrentTrip table only has one row; we update contents of that row with
                    currently-selected-trips. */

        /* Right now, it's used to store the fileName of the currently selected route
            into the database. The current route is stored on the device's internal
             storage as a file so data is persisted. We store the FileName to this
              file so we may retrieve it when we need it.
              NOTE: We retrieve this from our MapsAcitivty to display route */

        //todo:
        try {
            TripDatabase dbHelper = new TripDatabase(mContext);
            dbHelper.getWritableDatabase();
            if (!dbHelper.storeCurrentRouteFileName(FILENAME)) {
                dbHelper.close();
                return false;
            }
            dbHelper.close();
            return true;
        } catch (SQLException e) {
            Log.e(DEBUG_TAG, "Failed to get WritableDatabase access");
            return false;
        }
    }

}
