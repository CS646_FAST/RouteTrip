package first.routetrip.helpers.trip;

import android.os.Parcel;
import android.os.Parcelable;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * A helper class containing all information regarding a leg of a route. (A "part" of the route)
 * NOTE: If a route has no waypoints, there is only one leg inside the legs[] field (so only one leg object for a route)
 * See all available fields here: https://developers.google.com/maps/documentation/directions/intro#Legs
 */

public class Leg implements Parcelable, Serializable {

    private ArrayList<Step> mLegSteps;
    private Distance mLegDistance;
    private PlaceLocation mLegStartLoc;
    private PlaceLocation mLegDstLoc;

    //duration of this leg? (= Make object: value (int) and String representation)
    //duration in traffic? (= Make object: value (int) and String representation)

    public Leg() {
        //Empty constructor, use setters to populate the leg object.
        mLegSteps = new ArrayList<>();
    }

    private Leg(Parcel in) {
        /* Constructor to re-generate an object from a Parcel;
            private as it's called internally by CREATOR */

        this.mLegSteps = new ArrayList<>();

        //IMPORTANT: Access in the same order as they were written in writeToParcel!
        in.readList(mLegSteps, Step.class.getClassLoader()); //read mLegSteps
        this.mLegDistance = in.readParcelable(Distance.class.getClassLoader()); //read mLegDsitance
        this.mLegStartLoc = in.readParcelable(PlaceLocation.class.getClassLoader()); //read mLegStartLoc
        this.mLegDstLoc = in.readParcelable(PlaceLocation.class.getClassLoader()); //read mLegDstLoc
    }

    public void addLegStep(Step step) {
        this.mLegSteps.add(step);
    }

    public Step getLegStep(int index) {
        return this.mLegSteps.get(index);
    }

    public int getNumLegSteps() {
        return this.mLegSteps.size();
    }

    public ArrayList<Step> getLegSteps() {
        return this.mLegSteps;
    }

    public Distance getLegDistance() {
        return this.mLegDistance;
    }

    public void setLegDistance(Distance distance) {
        this.mLegDistance = distance;
    }

    public PlaceLocation getLegStartLoc() {
        return this.mLegStartLoc;
    }

    public void setLegStartLoc(PlaceLocation start) {
        this.mLegStartLoc = start;
    }

    public PlaceLocation getLegDstLoc() {
        return this.mLegDstLoc;
    }

    public void setLegDstLoc(PlaceLocation destination) {
        this.mLegDstLoc = destination;
    }

    //========================================PARCELABLE============================================

    public static final Parcelable.Creator<Leg> CREATOR =
            new Parcelable.Creator<Leg>() {
                /* more info about this in link @ start of this class */

                public Leg createFromParcel(Parcel in) {
                    /* re-create this object from a parcel via special constructor */
                    return new Leg(in);
                }

                public Leg[] newArray(int size) {
                    /* allows an array of this object to be parceled */
                    return new Leg[size];
                }
            };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        /* writes object fields to a parcel in a particular order */
        dest.writeList(mLegSteps);
        dest.writeParcelable(mLegDistance, 0);
        dest.writeParcelable(mLegStartLoc, 0);
        dest.writeParcelable(mLegDstLoc, 0);
    }

}
