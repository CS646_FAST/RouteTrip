package first.routetrip.helpers.sql;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

/**
 * SQLite Database Helper; helps create and manage the database used by our App.
 * Can be considered the "middle-layer" between the application layer and database layer.
 * NOTE: The database is created ONLY when the app needs access to it!
 * RouteTrip's database stores the fileNames of saved routes onto device's internal storage.
 * TODO: make consistent query calls, either write string yourself or use helper functions
 */

public class TripDatabase extends SQLiteOpenHelper {

    private static final String DEBUG_TAG = TripDatabase.class.getSimpleName();

    /* Database name & version required */
    private static final String DATABASE_NAME = "RouteTrip";
    private static final int DATABASE_VERSION = 1;

    /* Define database Table(s) */
    private static final String TABLE_SAVED_TRIPS = "SavedTrips";
    private static final String TABLE_CURRENT_TRIP = "CurrentTrip"; //MUST only contain one row; only update operations allowed on this row.

    /* Define individual table constants;
        each one needs a primary key column called "_id" storing an int.  */
    private static final String COLUMN_ID = "_id";
    private static final String COLUMN_SAVED_TRIPS_FILENAME = "savedRoutesFileName";
    private static final String COLUMN_CURRENT_TRIP_FILENAME = "currentRouteFileName";

    /* Constructor */
    public TripDatabase(Context context) {
        /* SQLiteOpenHelper sub-class must pass context, Database name, and version to super */
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        /* NOTE: The null parameter specifies the SQLiteDatabase.CursorFactory to allow
            returning sub-classes of Cursor when calling query. */

    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        updateDatabase(db, 0, DATABASE_VERSION);

        /*  NOTE: We only insert one row into CurrentTrip table at creation:

            Automatically create one empty entry into the CurrentTrip database that will
             be UPDATED accordingly. */
        ContentValues value = new ContentValues();
        value.put(COLUMN_CURRENT_TRIP_FILENAME, "");
        db.insert(TABLE_CURRENT_TRIP, null, value);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        /* This should never run for now; throwing assertion error if it does.
            Don't touch the DATABASE_VERSION */
        throw new AssertionError("Error: don't update database");
//        updateDatabase(db, oldVersion, newVersion);
    }

    private void updateDatabase(SQLiteDatabase db, int oldVersion, int newVersion) {
        /* This method handles all database creation and updates based on version numbers */

        if (oldVersion < 1) { //runs once when Database is first created ( 0 < 1 )
            /* Create SQL command(s) to create our tables */
            String CREATE_SAVED_TRIPS_TABLE = "CREATE TABLE " + TABLE_SAVED_TRIPS + "("
                    + COLUMN_ID + " INTEGER PRIMARY KEY,"
                    + COLUMN_SAVED_TRIPS_FILENAME + " TEXT,"
                    + "UNIQUE(" + COLUMN_SAVED_TRIPS_FILENAME + ")"
                    + ");";

            String CREATE_CURRENT_TRIP_TABLE = "CREATE TABLE " + TABLE_CURRENT_TRIP + "("
                    + COLUMN_ID + " INTEGER PRIMARY KEY,"
                    + COLUMN_CURRENT_TRIP_FILENAME + " TEXT,"
                    + "UNIQUE(" + COLUMN_CURRENT_TRIP_FILENAME + ")"
                    + ");";

            /* Now execute those SQL quarries */
            db.execSQL(CREATE_SAVED_TRIPS_TABLE);
            db.execSQL(CREATE_CURRENT_TRIP_TABLE);

        }

//        if (newVersion < 2){
//            //Code to update / change /rename database and/or tables here
//
//            /* IMPORTANT: Be very careful of control flow here. Keep an eye on if conditions,
//            because more than one scope will run and database will not be updated properly */
//        }


    }

    public boolean storeSavedRouteFileName(String FILENAME) {
        /* Store the file-name of a saved route (uniquely identified by 3 tuple in parameters)
            NOTE: FileName contains Route Position + Destination
             to ensure uniqueness; will just have to extract RoutePosition as a string
              since hashes were appended inside parentheses as a string and may be removed
               ie: "Route 2(-1354463785SAVE)" */
        //todo: update FileName uniqueness; see todo in ItineraryList.java -> private boolean saveRoute(..){..}

        try {
            ContentValues values = new ContentValues();
            values.put(COLUMN_SAVED_TRIPS_FILENAME, FILENAME); //key = column where we want to save value (inside appropriate table)
            SQLiteDatabase db = this.getWritableDatabase();
            db.insertOrThrow(TABLE_SAVED_TRIPS, null, values);
            return true;
        } catch (SQLException e) {
            Log.e(DEBUG_TAG, "Error: Failed to insert savedRoute into Database!");
            return false;
        }

    }

    public boolean storeCurrentRouteFileName(String FILENAME) {
        /* Update the file-name stored in the singleton row of the CurrentTrip table.
             NOTE: FileName contains Route Position + Destination
              to ensure uniqueness; will just have to extract RoutePosition as a string
               since hashes were appended inside parentheses as a string and may be removed
                ie: "Route 2(-1354463785)" */
        //todo: update FileName uniqueness; see todo in ItineraryList.java -> private boolean saveRoute(..){..}

        try {
            ContentValues values = new ContentValues();
            values.put(COLUMN_CURRENT_TRIP_FILENAME, FILENAME); //key = column where we want to update
            SQLiteDatabase db = this.getWritableDatabase();
            db.update(TABLE_CURRENT_TRIP, values, null, null);
            db.close();
            return true;
        } catch (SQLException e) {
            Log.e(DEBUG_TAG, "Error: Failed to update currentTrip within Database!");
            return false;
        }


    }

    public boolean checkIfSavedRouteExists(String FILENAME) {
        /* This method returns true if FILENAME already exists inside the SavedTrips Table */

        String query = "SELECT * FROM " + TABLE_SAVED_TRIPS + " WHERE " + COLUMN_SAVED_TRIPS_FILENAME + " = \"" + FILENAME + "\"";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(query, null);
        if (cursor.moveToFirst()) {
            //We already have this FILENAME entered
            cursor.close();
            db.close();
            return true;
        } else {
            cursor.close();
            db.close();
            return false;
        }
    }

    public boolean checkIfHaveSavedRoutes(){
        /* This method returns true if we have at least one row in the SavedTrips table,
            false otherwise */

        String query = "SELECT * FROM " + TABLE_SAVED_TRIPS;
//        String query = "select exists(select 1 from " + TABLE_SAVED_TRIPS  + ");"; //todo: "select exists" is apparently faster than "select * from.." for checking if a table is empty or not, implement if ever have time
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(query, null);
        if (cursor.moveToFirst()) {
            //We have a result
            cursor.close();
            db.close();
            return true;
        } else {
            //Cursor has no rows
            cursor.close();
            db.close();
            return false;
        }

    }

    public String checkCurrentTableFileName() {
        /* This method returns the FILENAME currently stored in CurrentTrip Table, null otherwise */

        String query = "SELECT * FROM " + TABLE_CURRENT_TRIP; //get all entries in CurrentTable (only one; singleton row)
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(query, null);
        if (cursor.moveToFirst()) {
            String FILENAME = cursor.getString(cursor.getColumnIndex(COLUMN_CURRENT_TRIP_FILENAME)); //this should be CurrentTrip Name: "Route1(hash)"
            cursor.close();
            db.close();
            return FILENAME;
        } else {
            cursor.close();
            db.close();
            return "";
        }
    }

    public Cursor getAllSavedRoutes() {
        /* Returns a cursor with all saved routes within the "SavedTrips" table,
            or null of there are no saved routes (shouldn't happen) */

        try {

            String query = "SELECT * FROM " + TABLE_SAVED_TRIPS; //get all entries in SavedTable (all saved Route FILENAMEs)
            SQLiteDatabase db = this.getReadableDatabase();
            Cursor cursor = db.rawQuery(query, null);

            //Alternate method:
//            Cursor cursor = db.query(
//                    TABLE_SAVED_TRIPS,
//                    new String[]{COLUMN_SAVED_TRIPS_FILENAME},
//                    null, //String selection; ie: "NAME = ?"
//                    null, //String[] selectionArgs, ie: new String[] {"ColumnName Value"}
//                    null, //groupBy
//                    null, //having
//                    null); //orderBy

            if (cursor.moveToFirst()) {
                db.close();
                return cursor;
            } else {
                db.close();
                return null;
                /* Cursors can never be null; explicitly return null */
            }

        } catch (SQLException e) {
            Log.e(DEBUG_TAG, "Error: Failed to get database access when" +
                    " retrieving all SavedRoutes");
            return null;
            /* Cursors can never be null; explicitly return null */
        }

    }

    public boolean deleteSavedRoute(String FILENAME){
        /* This method deleted the row containing FILENAME from the SavedTrips table */

        try {
            String query = "DELETE FROM " + TABLE_SAVED_TRIPS + " WHERE " + COLUMN_SAVED_TRIPS_FILENAME + " = \"" + FILENAME + "\"";
            SQLiteDatabase db = this.getWritableDatabase();
            db.execSQL(query);
            db.close();
            return true;
        } catch (SQLException e){
            Log.e(DEBUG_TAG, "ERROR: failed to delete route from database");
            return false;
        }
    }

    public boolean resetCurrentRoute(){
        /* this method empties the singleton row containing the CurrentRoute FILENAME */

        try {
            ContentValues values = new ContentValues();
            values.put(COLUMN_CURRENT_TRIP_FILENAME, "");
            SQLiteDatabase db = this.getWritableDatabase();
            db.update(TABLE_CURRENT_TRIP, values, null, null);
            db.close();
            return true;
        } catch (SQLException e) {
            Log.e(DEBUG_TAG, "Error: Failed to reset currentTrip within Database!");
            return false;
        }

    }

}
