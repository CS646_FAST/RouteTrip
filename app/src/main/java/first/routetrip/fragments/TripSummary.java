package first.routetrip.fragments;


import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;

import first.routetrip.R;
import first.routetrip.activities.MainActivity;
import first.routetrip.helpers.FourSquareCategory;

/**
 * A fragment that displays the destination and category list the user previous selected.
 * Upon button click, will generate list of itineraries with the two data sets.
 *
 * Next fragment in trip-selection hierarchy after this should be "ItineraryList"
 */

public class TripSummary extends Fragment {

    private static final String DEBUG_TAG = TripSummary.class.getSimpleName();
    private static final String FRAGMENT_TAG = "FragTag:";

    //Define Variables
    private String userDestination;
    private ArrayList<FourSquareCategory> selectedCategories;
    private handleSummaryData fragmentListener;

    public interface handleSummaryData {
        void generateItinerariesButton();
    }

    public TripSummary() {
        // Required empty public constructor
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d(DEBUG_TAG, "onDestroy");
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        //NOTE: we know we're attaching this to activity, therefore following cast is safe.
        //      Otherwise, this line could cause exceptions.
        Activity activity = (Activity) context;

        //We make sure listeners have successfully "identified" themselves before fragment functions
        //Otherwise, we throw an exception.

        try {
            fragmentListener = (handleSummaryData) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement handleSummaryData");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_trip_summary, container, false);

        //Retrieve Parameters:
        Bundle bundle = getArguments();
        userDestination = bundle.getString(MainActivity.FRAG_PARAMETER_DESTINATION); //get user destination
        selectedCategories = bundle.getParcelableArrayList(MainActivity.FRAG_PARAMETER_CATEGORIES); //get selected category ArrayList

        /* Assert that two parameters aren't null */
        assert userDestination != null;
        assert selectedCategories != null;

        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        //Set listener for the search-button to generate itineraries:
        ImageButton searchButton = (ImageButton) view.findViewById(R.id.generateItineraryButton);
        searchButton.setOnClickListener(searchButtonListener);

        //populate destination string with userDestination:
        TextView userDestinationText = (TextView) view.findViewById(R.id.summary_userDestination);
        userDestinationText.setText(userDestination);

        //populate listView with selectedCategories: (recreate adapter each time)
        ListView summaryCategoryList = (ListView) view.findViewById(R.id.summary_categoryList);
        SummaryListAdapter adapter = new SummaryListAdapter(getContext(), selectedCategories);
        summaryCategoryList.setAdapter(adapter);
    }

    //========================================LISTENERS=============================================

    View.OnClickListener searchButtonListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            /* let activity know that user wants to generate itineraries */
            fragmentListener.generateItinerariesButton();
        }
    };

    //========================================INNER CLASSES=========================================

    public class SummaryListAdapter extends BaseAdapter {

        private final class CategoryViewHolder {
            /* final viewHolder for categories */

            private ImageView mIcon;
            private TextView mDescription;
        }

        private CategoryViewHolder viewHolder;
        private ArrayList<FourSquareCategory> categories;
        private LayoutInflater inflater;

        public SummaryListAdapter(Context context, ArrayList<FourSquareCategory> data) {
            this.inflater = LayoutInflater.from(context); //cache inflater
            this.categories = data;
        }

        @Override
        public int getCount() {
            return categories.size();
        }

        @Override
        public Object getItem(int position) {
            return categories.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            View view = convertView;

            if (view == null) {
                //inflate layout view and store it into a viewHolder
                view = inflater.inflate(R.layout.list_foursquare_category_summary_row, parent, false);

                viewHolder = new CategoryViewHolder();
                viewHolder.mIcon = (ImageView) view.findViewById(R.id.foursquare_selectedcategory_icon);
                viewHolder.mDescription = (TextView) view.findViewById(R.id.foursquare_selectedcategory_description);

                //attach viewHolder to view
                view.setTag(viewHolder);
            } else {

                //load attached viewHolder if a recycled view exists
                viewHolder = (CategoryViewHolder) view.getTag();
            }

            //populate category icon and text:
            viewHolder.mIcon.setImageResource(categories.get(position).getCategoryImgID());
            viewHolder.mDescription.setText(categories.get(position).getCategoryName());

            return view;
        }

        @Override
        public boolean isEnabled(int position) {
            /* prevent the summary list from being clickable */
            return false;
            //todo: change this or leave it un-clickable?
        }
    }

}
