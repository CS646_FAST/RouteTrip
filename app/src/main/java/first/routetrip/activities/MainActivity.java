package first.routetrip.activities;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;

import java.util.ArrayList;

import first.routetrip.R;
import first.routetrip.fragments.CategoryInput;
import first.routetrip.fragments.DestinationInput;
import first.routetrip.fragments.ItineraryList;
import first.routetrip.fragments.SavedRoutes;
import first.routetrip.helpers.FourSquareCategory;
import first.routetrip.fragments.TripSummary;
import first.routetrip.helpers.sql.TripDatabase;

public class MainActivity extends AppCompatActivity
        implements DestinationInput.handleDestinationData,
        CategoryInput.handleCategoryData,
        TripSummary.handleSummaryData {

    /*
    IMPORTANT TODOs
    todo Handle back-stack on mainFrame for ALL fragment swaps (including nav clicks)
    todo: implement a file that constantly appends user clicks / actions / database saves / deletions (a log).. makes debugging easier
    todo: check this out for cleaner fragment instantiation.. http://stackoverflow.com/questions/9245408/best-practice-for-instantiating-a-new-android-fragment
    todo: regarding fragment instantiation: create a new blank fragment WITH FACTORY METHODS, see their structure and the "newInstance" method. Can change ALL fragments to follow that structure.
    todo: debugging: current trip consistency by starting current trip in itineraryList and savedRoutes should say "this is the current trip" consistently.
    todo: once have unique identifier on routes, keep track of adding / deletion. NEED to refactor ALL of those functions to keep track of them easier.
    todo: If we have a current trip, after splash we want to open to an "info" frag that shows details about current trip. NOTE: This should be displayed after EVERY new activity is started with intent, other fragments should be cleared from back-stack

    TODO FOR RELEASE: (after above ones are complete)
    - permissions check wherever needed
    - API KEY: need unlimited queries (or 150k/day) as opposed to 1k / day
    - refactor whole app such that each activity/fragment/helper does one thing
    - THEN further refactor each method within those such that the method's functionality is easily seen:
            ie: method to open HTTP connection, extract JSON object, etc..
    - Icon color opacity 60% ? (re-download all at theme "holo light - 60%")
     */

    //Static Variables:
    private static final String DEBUG_TAG = "MainActivity";
    private static final String FRAGMENT_TAG = "FragTag:";
    public static final String FRAG_PARAMETER_DESTINATION = "Parameter_1";
    public static final String FRAG_PARAMETER_CATEGORIES = "Parameter_2";
    public static final String ROUTE_FILENAME = "routeFile";

    //Define Variables:
    private Toolbar toolbar;
    private DrawerLayout drawerLayout;
    private NavigationView navView;
    private ActionBarDrawerToggle actionBarDrawerToggle;
    private Bundle userInputBundle; // bundled parameters: [destination, categoryList]
    private DestinationInput destinationFragment;
    private CategoryInput categoryFragment;

    /*
     * NOTE: These two data sets below could be treated as "Object" types to make them universal
     *  and then cast as needed?
     * Problem I see with this is that if there's a bunch of data objects, we have to keep track of what we're casting it to
     *  in which case it defeats the purpose of having it of type "Object"
     */

    //DataSets containing user information (received from fragments); to be used wherever needed
    private String userData1;
    private ArrayList<FourSquareCategory> userData2;
    //todo: above two can be removed if we don't need to use it anywhere else; can be passed directly from tripSummary fragment to generate itineraries

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // TODO: 2016-06-16 Important: Icons are free to use, BUT MUST LINK authors in "about" section of app: https://icons8.com/

        //Initialize toolbar and assign it to the activity:
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        //Initialize the "home" button -> to be converted into a "hamburger" icon
        if (getSupportActionBar() == null) {
            throw new AssertionError("ERROR: " + DEBUG_TAG + " supportActionBar null");
        } else {
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

        //Initialize the navigationView:
        navView = (NavigationView) findViewById(R.id.nav_view);

        //Set the itemSelectedListener for the NavigationView:
        if (navView == null) {
            throw new AssertionError("ERROR: " + DEBUG_TAG + " navigationView null");
        } else {
            navView.setNavigationItemSelectedListener(navMenuClickListener);
        }

        //Initialize DrawerLayout
        drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);

        //Set the DrawerToggle
        setupDrawerToggle();

        //When restoring state, first check if it's the initial set-up:
        if (savedInstanceState == null) {
            //initial setup; populate main content:

            //todo: if the user has saved trips, or has a "chosen" trip, we want to display a different fragment by default if we open the app.
            /* on first load, start with navItem: enter new Trip information */
            navView.setCheckedItem(R.id.nav_newTrip);
            navView.getMenu().performIdentifierAction(R.id.nav_newTrip, 0);

        } else {
            //previous state was restored; correctly update main content titles, data, etc..

            //update summarArgs bundle as well

            // TODO: restoring state, load from savedInstanceState and restore
        }

        /**
         * TODO: see below
         * Add a back-stack listener to the supportFragmentManager to correctly update content titles
         * using the back button. (as we navigate through pages using the nav menu)
         *
         * NOTE: Will have to tag any created fragments and then get a reference to them here.
         */

    }

    protected void setupDrawerToggle() {

        /*
         * Method to handle setting the ActionBarDrawerToggle
         */

        /* The actionBarDrawerToggle provides the hamburger icon.
            If we remove it, will need to provide our own*/
        actionBarDrawerToggle = new ActionBarDrawerToggle(
                this,
                drawerLayout,
                toolbar,
                R.string.navigation_drawer_open,
                R.string.navigation_drawer_close) {

            @Override
            public void onDrawerOpened(View drawerView) {

                /*
                 * Called when the drawer is being opened.
                 * We can change change state during this action (change toolbar "alwaysShow" item), etc..
                 */

                //We could call invalidateOptionsMenu() to request a system call to onCreateOptionsMenu(Menu), more @ http://developer.android.com/reference/android/app/Activity.html#invalidateOptionsMenu()
                super.onDrawerOpened(drawerView);
            }

            //Called when drawer is being closed
            @Override
            public void onDrawerClosed(View drawerView) {

                /*
                 * Called when the drawer is being closed
                 * Again, can change state if needed
                 */

                //We could call invalidateOptionsMenu() to request a system call to onCreateOptionsMenu(Menu), more @ http://developer.android.com/reference/android/app/Activity.html#invalidateOptionsMenu()
                super.onDrawerClosed(drawerView);
            }
        };

        //Set the actionBarToggleListener to the drawerLayout:
        drawerLayout.addDrawerListener(actionBarDrawerToggle);
    }

    @Override
    protected void onPostCreate(@Nullable Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);

        //Synchronize the home button icon: "hamburger" icon when drawer closed; back arrow when it's open
        if (actionBarDrawerToggle != null) {
            actionBarDrawerToggle.syncState();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        //Inflate the toolbar menu: (toolbar_menu.xml)
        getMenuInflater().inflate(R.menu.toolbar_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    private void clearFragBackStackHistory() {

        /* clear back-stack history at end of a workflow */
        //todo: PUT THIS IN A HELPER so it's reachable from all modules without creating tight coupling

        /* http://stackoverflow.com/questions/17107005/how-to-clear-fragment-backstack-in-android */
        FragmentManager manager = getSupportFragmentManager();
        if (manager.getBackStackEntryCount() > 0) {
            FragmentManager.BackStackEntry first = manager.getBackStackEntryAt(0);
            manager.popBackStack(first.getId(), FragmentManager.POP_BACK_STACK_INCLUSIVE);
        }
    }

    private void closeSoftKeyboard() {
        //close soft-keyboard:

        //todo: first check if soft keyboard is showing
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        if (getCurrentFocus() != null) {
            imm.hideSoftInputFromWindow(getCurrentFocus().
                    getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        }
    }

    //==================================INTERFACE METHODS===========================================

    @Override
    public void destinationContinueButtonClicked(String userDestination) {

        /*
         * User pressed the "continue" button on the DestinationInput fragment, handle the string
         *  received by storing it as dataSet1 and save it into an bundle.
         * Start the "CategoryInput" fragment
         */

        closeSoftKeyboard();

        //save user-specified destination as dataSet1; use it directly from arg to pass into bundle; remove if not needed
        userData1 = userDestination;

        if (userInputBundle == null) {
            //initialize bundle object if null (to be used for summaryFragment)
            userInputBundle = new Bundle();
        }

        //store a string in a bundle
        userInputBundle.putString(FRAG_PARAMETER_DESTINATION, userDestination);


        /* if user returned to destinationInput from categoryInput back,
            we want to load that same categoryInput fragment when we return to it;
             so one create new one when activity first loaded */
        if (categoryFragment == null) {
            categoryFragment = new CategoryInput();
        }

        /* Also swap out destinationInput fragment with categoryInput fragment */
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.mainFrame, categoryFragment, FRAGMENT_TAG);
        transaction.setTransition(FragmentTransaction.TRANSIT_NONE);
        transaction.addToBackStack(null);
        transaction.commit();
    }

    @Override
    public void categoryContinueButtonClicked(ArrayList<FourSquareCategory> selectedCategories) {

        /*
         * User pressed "continue" button on the categoryList fragment, handle the ArrayList
         *  received by storing it as dataSet2, and save it to a bundle.
         * Start the "TripSummary" fragment with bundle: userInputBundle
         */

        /* We need to make sure user entered a destination before
            we can continue to "tripSummary" frag.
             NOTE: no selected categories same as selecting all */

        if (userData1 == null) {
            /* how would we reach this? CategoryFragment
                only displayed after we retrieved userData1 */
            Toast.makeText(this, "Please Enter Destination!", Toast.LENGTH_SHORT).show();
            Log.d(DEBUG_TAG, "Reached userData1 == null inside categoryContinueButtonClicked");
            return;
        }

        //save user-selected categories of interest; use it directly from arg to pass into bundle; remove if not needed
        userData2 = selectedCategories;

        /* By now we need to have both bundle parameters */
        if (userInputBundle == null) {
            /* we should not reach this point; display error and log it if we do (don't crash) */

            Toast.makeText(this, "Error: Unrecognized workflow.", Toast.LENGTH_SHORT).show();
            Log.d(DEBUG_TAG, "Reached bundle == null inside categoryContinueButtonClicked");

            userInputBundle = new Bundle();
        }

        //store an array of parcelable objects in a bundle
        userInputBundle.putParcelableArrayList(FRAG_PARAMETER_CATEGORIES, selectedCategories);

        /* By now we need to have both bundle parameters */
        if (userInputBundle.size() != 2) {
            //NOTE: have to explicitly check in fragments that actual data isn't null

            //todo: refactor this function into a method
            throw new AssertionError("ERROR: "
                    + DEBUG_TAG
                    + " bundleSize != 2 inside categoryContinueButtonClicked.");
        }

        /* Create a new TripSummary fragment each time (old one destroyed on: back, etc..) */
        TripSummary tripSummaryFragment = new TripSummary();
        //todo: see notes on generateItineraryButton listener; apply same principle here regarding if we create new fragment or not

        /* Add bundle to TripSummary fragment */
        tripSummaryFragment.setArguments(userInputBundle);

        /* Also swap out categoryInput fragment with tripSummary fragment */
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.mainFrame, tripSummaryFragment, FRAGMENT_TAG);
        transaction.setTransition(FragmentTransaction.TRANSIT_NONE);
        transaction.addToBackStack(null); //todo: see condition on new fragment; update accordingly
        transaction.commit();

    }

    @Override
    public void generateItinerariesButton() {

        /* User pressed the "generateItinerariesButton inside tripSummary fragment;
            We will now pass the two dataSets to the last fragment in this workflow,
             to display a list of generated itineraries. */

        //todo: refactor this bundle null check into method; it's called form all 4 fragment callbacks
        if (userInputBundle == null) {
            /* we should not reach this point; display error and log it if we do (don't crash) */

            Toast.makeText(this, "Error: Unrecognized workflow.", Toast.LENGTH_SHORT).show();
            Log.d(DEBUG_TAG, "Reached bundle == null inside generateItinerariesButton");

            userInputBundle = new Bundle();
        }

        /* By now we need to have both bundle parameters */
        if (userInputBundle.size() != 2) {
            //NOTE: have to explicitly check in fragments that actual data isn't null

            //todo: refactor this function into a method
            throw new AssertionError("ERROR: "
                    + DEBUG_TAG
                    + " bundleSize != 2 inside generateItineraryButton.");
        }


        //we again pass dataSet1 and dataSet2 into next fragment, replace mainFrame with new fragment and place it on backstack.
        //NOTE: placing tripSummary fragment on backstack so we can return to it if needed.
        //HOWEVER, we always create new one from "categoryContinueButtonClicked" in-case data changed.

        /* Create a new ItineraryList (ie: show progressDialog) each time the user
            wishes to generate it -> The ItineraryList fragment will NOT keep state FOR NOW */
        //todo: need to keep track if EITHER bundle parameter changed, THEN re-generate itinerarylist
        //todo: otherwise, it WILL Keep state and re-display the same list (because nothing should have changed)
        //todo: This will also apply to the TripSUmmary fragment.
        ItineraryList itineraryListFragment = new ItineraryList();

        /* Add bundle to ItineraryListFragment */
        itineraryListFragment.setArguments(userInputBundle);

        /* NOW swap out tripSummaryFragment with itineraryListFragment fragment */
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.mainFrame, itineraryListFragment, FRAGMENT_TAG);
        transaction.setTransition(FragmentTransaction.TRANSIT_NONE);
        transaction.addToBackStack(null); //todo: see condition on new fragment; update accordingly
        transaction.commit();

    }

    //========================================LISTENERS=============================================

    NavigationView.OnNavigationItemSelectedListener navMenuClickListener
            = new NavigationView.OnNavigationItemSelectedListener() {

        /*
         * Listener for clicks on navigation drawer menu items
         */

        @Override
        public boolean onNavigationItemSelected(MenuItem item) {

            drawerLayout.closeDrawers();
            closeSoftKeyboard();

            switch (item.getItemId()) {

                //insert cases & handle them here
                // TODO: 2016-06-16 manage nav menu item clicks here
                // TODO: update toolbar's menu accordingly

                //---------tmp-------------:
                case R.id.nav_currentTrip:

                    /* when user selects the currentTrip, if it exists, open MapsActivity
                        Otherwise give a dialog that there is no current route. */
                    new StartCurrentRouteTask().execute();
                    break;

                case R.id.nav_newTrip:

                    /* input for a new trip, clear existing input fragments / reset workflow */
                    if ((destinationFragment != null)
                            || (categoryFragment != null)) {
                        destinationFragment = null;
                        categoryFragment = null;
                        clearFragBackStackHistory();
                    }

                    FragmentTransaction transactionNewTrip = getSupportFragmentManager().beginTransaction();
                    destinationFragment = new DestinationInput();
                    transactionNewTrip.replace(R.id.mainFrame, destinationFragment, FRAGMENT_TAG);
                    transactionNewTrip.setTransition(FragmentTransaction.TRANSIT_NONE);
                    transactionNewTrip.commit();
                    break;

                case R.id.nav_savedTrips:

                    /* Display a RecyclerView of our saved Routes if there are any; if not, display dialog to user and do nothing */
                    new CheckSavedRoutesTask().execute();
                    break;


                case R.id.nav_manage:
                    Toast.makeText(MainActivity.this, "Clicked on nav_manage", Toast.LENGTH_SHORT).show();
                    break;

                case R.id.nav_about_us:
                    Toast.makeText(MainActivity.this, "Clicked on \"About\"", Toast.LENGTH_SHORT).show();
                    break;
                //---------tmp-------------

                default:
                    //When we're in the pre-release stage, throw AssertionError if we reach this case. We shouldn't reach this.
                    if (android.support.design.BuildConfig.DEBUG) {
                        //This is treated as empty when we're in the release stage.
                        Log.d(DEBUG_TAG, "ERROR - Reached default case");
                    }
                    break;
            }
            return true; //menu item - click was handles successfully.
        }
    };

    private class CheckSavedRoutesTask extends AsyncTask<Void, Void, Boolean> {
        /* AsyncTask to retrieve all saved Routes from database to check if any exist */

        protected void onPreExecute() {
            /* Code to run before executing the task (This is in main thread) */
        }

        protected Boolean doInBackground(Void... params) {
            /* Code to run in a worker thread */

            return new TripDatabase(MainActivity.this).checkIfHaveSavedRoutes();
        }

        protected void onProgressUpdate(Void... values) {
            /* can show progress bar for route generation here */
        }

        protected void onPostExecute(Boolean result) {
            /* Code to run after the task is complete; in main thread */

            if (result) {
                /* Display a recyclerView list of previously saved routes */
                clearFragBackStackHistory();
                FragmentTransaction transactionSavedTrips = getSupportFragmentManager().beginTransaction();
                SavedRoutes savedRoutes = new SavedRoutes();
                transactionSavedTrips.replace(R.id.mainFrame, savedRoutes, FRAGMENT_TAG);
                transactionSavedTrips.setTransition(FragmentTransaction.TRANSIT_NONE);
                transactionSavedTrips.commit();
            } else {
                /* we have no saved routes; display alert dialog to user and do nothing */

                final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(MainActivity.this);
                alertDialogBuilder.setMessage("No Saved Routes!");
                alertDialogBuilder.setNeutralButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        //do nothing
                    }
                });
                alertDialogBuilder.show();
            }
        }
    }

    private class StartCurrentRouteTask extends AsyncTask<Void, Void, String> {
        /* AsyncTask to check for currently selected route and open
            MapsActivity with the FILENAME as an intent */

        protected void onPreExecute() {
            /* Code to run before executing the task (This is in main thread) */
        }

        protected String doInBackground(Void... params) {
            /* Code to run in a worker thread */

            // Check if there is a current route stored
           return new TripDatabase(MainActivity.this).checkCurrentTableFileName();

        }

        protected void onProgressUpdate(Void... values) {
            /* can show progress bar for route generation here */
        }

        protected void onPostExecute(String result) {
            /* Code to run after the task is complete; in main thread */

            if(!result.equals("")){
                /* We have a current route stored */

                //Clear fragment backstack
                clearFragBackStackHistory();

                //Start MapsActivity with FILENAME if there is a current route
                Intent intent = new Intent(MainActivity.this, MapsActivity.class);
                intent.putExtra(ROUTE_FILENAME, result);
                MainActivity.this.startActivity(intent);
            } else {
                /* We don't have a current route stored, give a alert dialog to the user */
                final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(MainActivity.this);
                alertDialogBuilder.setMessage("No Current Route!");
                alertDialogBuilder.setNeutralButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        //do nothing
                    }
                });
                alertDialogBuilder.show();
            }

        }

    }

}
