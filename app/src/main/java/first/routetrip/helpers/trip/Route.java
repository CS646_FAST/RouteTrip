package first.routetrip.helpers.trip;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.android.gms.maps.model.LatLng;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import first.routetrip.helpers.LatLngSerializable;

/**
 * A helper class containing all information regarding a single route of a trip.
 * NOTE: To get multiple routes, must have "alteriatives=true" set in the HTTP GET URL inside "TripGenerator.java"
 * See all available fields here: https://developers.google.com/maps/documentation/directions/intro#Routes
 * More info on Parcelable here: https://newfivefour.com/android-parcelable.html
 */
public class Route implements Parcelable, Serializable {

    private ArrayList<Leg> mRouteLegs; //ArrayList of all legs within this route
//    private List<LatLng> mOverviewPolyLinePoints; //list of polyline-points for this route; single points object representing route path
    private ArrayList<LatLngSerializable> mOverviewPolyLinePointsSerializable;

    //NOTE: Add anything else we would need (see link above)
    //copyright? *
    //way-point order?
    //bounds?
    //warnings?
    //fare?
    //route-duration? ( = summary of durations of each leg in mRouteLegs)

    public Route() {
        /* empty constructor; use setters to populate object */
        mRouteLegs = new ArrayList<>();
        mOverviewPolyLinePointsSerializable = new ArrayList<>();
    }

    private Route(Parcel in) {
        /* Constructor to re-generate an object from a Parcel;
            private as it's called internally by CREATOR */

        this.mRouteLegs = new ArrayList<>();
        this.mOverviewPolyLinePointsSerializable = new ArrayList<>(); // cast issue ? (list -> ArrayList)

        //IMPORTANT: Access in the same order as they were written in writeToParcel!
        in.readList(this.mRouteLegs, Leg.class.getClassLoader());
        in.readList(this.mOverviewPolyLinePointsSerializable, LatLngSerializable.class.getClassLoader());
    }

    public List<LatLng> getOverviewPolyLinePoints() {
        /* converts the List<LatLngSerializable> objects into the required
            List<LatLng> using Google's LatLng objects. */
        List<LatLng> overViewPolyLinePoints = new ArrayList<>();
        for(LatLngSerializable serializable : this.mOverviewPolyLinePointsSerializable){
            overViewPolyLinePoints.add(new LatLng(serializable.mLatitude, serializable.mLongitude));
        }
        return overViewPolyLinePoints;
    }

    public void setOverviewPolyLinePoints(List<LatLng> polyLinePoints) {
        /* uses the List<LatLng> entries and populates
            an ArrayList of LatLngSerializable objects */
        for(LatLng unserializable : polyLinePoints){
            this.mOverviewPolyLinePointsSerializable
                    .add(new LatLngSerializable(
                            unserializable.latitude,
                            unserializable.longitude));
        }
    }

    public void addRouteLeg(Leg leg) {
        this.mRouteLegs.add(leg);
    }

    public int getNumRouteLegs() {
        return this.mRouteLegs.size();
    }

    public ArrayList<Leg> getRouteLegs() {
        return this.mRouteLegs;
    }

    public Leg getRouteLeg(int index) {
        return this.mRouteLegs.get(index);
    }

    //========================================PARCELABLE============================================

    public static final Parcelable.Creator<Route> CREATOR =
            new Parcelable.Creator<Route>() {
                /* more info about this in link @ start of this class */

                public Route createFromParcel(Parcel in) {
                    /* re-create this object from a parcel via special constructor */
                    return new Route(in);
                }

                public Route[] newArray(int size) {
                    /* allows an array of this object to be parceled */
                    return new Route[size];
                }
            };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        /* writes object fields to a parcel in a particular order */
        dest.writeList(mRouteLegs);
        dest.writeList(mOverviewPolyLinePointsSerializable);
    }

}
