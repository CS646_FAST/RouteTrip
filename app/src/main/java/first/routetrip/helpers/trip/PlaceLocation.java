package first.routetrip.helpers.trip;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.android.gms.maps.model.LatLng;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;

import first.routetrip.helpers.LatLngSerializable;

/**
 * Helper object that represents a location along a trip-route-leg. Contains:
 * 1: A String address
 * 2: LatLng coordinates
 */
public class PlaceLocation implements Parcelable, Serializable {

    private String mLocAddress; /* IMPORTANT: This is null in Step Objects! */
    private LatLngSerializable mLocLatLng;

    public PlaceLocation() {
        /* empty constructor; use setters to populate object */
    }

    private PlaceLocation(Parcel in){
        /* Constructor to re-generate an object from a Parcel;
            private as it's called internally by CREATOR */

        //IMPORTANT: Access in the same order as they were written in writeToParcel!
        this.mLocAddress = in.readString();
        this.mLocLatLng = in.readParcelable(LatLng.class.getClassLoader());
    }

    public void setLocAddress(String address) {
        this.mLocAddress = address;
    }

    public void setLocLatLng(double lat, double lng) {
        /* converts LatLng to a serializable object to store */
        this.mLocLatLng = new LatLngSerializable(lat, lng);
    }

    public String getLocAddress() {
        return this.mLocAddress;
    }

    public LatLng getLocLatLng() {
        /* returns Google's LatLng object from custom serializable one */
        return new LatLng(this.mLocLatLng.mLatitude, this.mLocLatLng.mLongitude);
    }

//    //=======================================SERIALIZABLE===========================================
//
//    /* Google's LatLng class isn't serializable, so extract the doubles and serialize those
//        More Info @: http://stackoverflow.com/questions/14220554/how-to-serialize-a-third-party-non-serializable-final-class-e-g-googles-latln */
//
//    private void writeObject(ObjectOutputStream outputStream) throws IOException {
//        outputStream.defaultWriteObject();
//        outputStream.writeUTF(this.mLocAddress);
//        outputStream.writeDouble(this.mLocLatLng.latitude);
//        outputStream.writeDouble(this.mLocLatLng.longitude);
//    }
//
//    private void readObject(ObjectInputStream inputStream) throws IOException, ClassNotFoundException {
//        inputStream.defaultReadObject();
//        this.mLocAddress = inputStream.readUTF();
//        this.mLocLatLng = new LatLng(inputStream.readDouble(), inputStream.readDouble());
//    }

    //========================================PARCELABLE============================================

    public static final Parcelable.Creator<PlaceLocation> CREATOR =
            new Parcelable.Creator<PlaceLocation>() {
                /* more info about this in link @ start of this class */

                public PlaceLocation createFromParcel(Parcel in) {
                    /* re-create this object from a parcel via special constructor */
                    return new PlaceLocation(in);
                }

                public PlaceLocation[] newArray(int size) {
                    /* allows an array of this object to be parceled */
                    return new PlaceLocation[size];
                }
            };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        /* writes object fields to a parcel in a particular order */
        dest.writeString(mLocAddress);
        dest.writeParcelable(mLocLatLng, 0);
    }
}
