package first.routetrip.activities;

import android.app.ListActivity;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import first.routetrip.R;
import first.routetrip.helpers.venue.FoursquareVenue;

public class FoursquareResults extends ListActivity {

    ArrayList<FoursquareVenue> venuesList;

    // the foursquare client_id and the client_secret

    // ============== YOU SHOULD MAKE NEW KEYS ====================//
    final String CLIENT_ID = "todo";
    final String CLIENT_SECRET = "todo";

    // we will need to take the latitude and the logntitude from a certain point
    // this is the center of New York
    final String latitude = "40.7463956";
    final String longtitude = "-73.9852992";

    ArrayAdapter<String> myAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // start the AsyncTask that makes the call for the venus search.
        new FourSquare().execute();
    }

    private class FourSquare extends AsyncTask<View, Void, String> {

        String temp;

        @Override
        protected String doInBackground(View... urls) {
            // make Call to the url
            //temp = makeCall("https://api.foursquare.com/v2/venues/search?client_id=" + CLIENT_ID + "&client_secret=" + CLIENT_SECRET + "&v=20130815&ll=40.7463956,-73.9852992");
            temp = "{\n" +
                    "meta: {\n" +
                    "code: 200,\n" +
                    "requestId: \"575fbbfd498e825b30e4a929\"\n" +
                    "},\n" +
                    "response: {\n" +
                    "venues: [\n" +
                    "{\n" +
                    "id: \"525345218bbdde49a3249e0d\",\n" +
                    "name: \"NoMad\",\n" +
                    "contact: { },\n" +
                    "location: {\n" +
                    "lat: 40.7442,\n" +
                    "lng: -73.9883,\n" +
                    "distance: 351,\n" +
                    "cc: \"US\",\n" +
                    "city: \"New York\",\n" +
                    "state: \"NY\",\n" +
                    "country: \"United States\",\n" +
                    "formattedAddress: [\n" +
                    "\"New York, NY\",\n" +
                    "\"United States\"\n" +
                    "]\n" +
                    "},\n" +
                    "categories: [\n" +
                    "{\n" +
                    "id: \"4f2a25ac4b909258e854f55f\",\n" +
                    "name: \"Neighborhood\",\n" +
                    "pluralName: \"Neighborhoods\",\n" +
                    "shortName: \"Neighborhood\",\n" +
                    "icon: {\n" +
                    "prefix: \"https://ss3.4sqi.net/img/categories_v2/parks_outdoors/neighborhood_\",\n" +
                    "suffix: \".png\"\n" +
                    "},\n" +
                    "primary: true\n" +
                    "}\n" +
                    "],\n" +
                    "verified: false,\n" +
                    "stats: {\n" +
                    "checkinsCount: 9935,\n" +
                    "usersCount: 4366,\n" +
                    "tipCount: 7\n" +
                    "},\n" +
                    "specials: {\n" +
                    "count: 0,\n" +
                    "items: [ ]\n" +
                    "},\n" +
                    "hereNow: {\n" +
                    "count: 1,\n" +
                    "summary: \"One other person is here\",\n" +
                    "groups: [\n" +
                    "{\n" +
                    "type: \"others\",\n" +
                    "name: \"Other people here\",\n" +
                    "count: 1,\n" +
                    "items: [ ]\n" +
                    "}\n" +
                    "]\n" +
                    "},\n" +
                    "referralId: \"v-1465891837\",\n" +
                    "venueChains: [ ]\n" +
                    "},\n" +
                    "{\n" +
                    "id: \"4ca4e519a6e08cfa884d6c94\",\n" +
                    "name: \"Midtown Manhattan\",\n" +
                    "contact: { },\n" +
                    "location: {\n" +
                    "lat: 40.747,\n" +
                    "lng: -73.986,\n" +
                    "distance: 89,\n" +
                    "postalCode: \"10001\",\n" +
                    "cc: \"US\",\n" +
                    "city: \"New York\",\n" +
                    "state: \"NY\",\n" +
                    "country: \"United States\",\n" +
                    "formattedAddress: [\n" +
                    "\"New York, NY 10001\",\n" +
                    "\"United States\"\n" +
                    "]\n" +
                    "},\n" +
                    "categories: [\n" +
                    "{\n" +
                    "id: \"4f2a25ac4b909258e854f55f\",\n" +
                    "name: \"Neighborhood\",\n" +
                    "pluralName: \"Neighborhoods\",\n" +
                    "shortName: \"Neighborhood\",\n" +
                    "icon: {\n" +
                    "prefix: \"https://ss3.4sqi.net/img/categories_v2/parks_outdoors/neighborhood_\",\n" +
                    "suffix: \".png\"\n" +
                    "},\n" +
                    "primary: true\n" +
                    "}\n" +
                    "],\n" +
                    "verified: false,\n" +
                    "stats: {\n" +
                    "checkinsCount: 66131,\n" +
                    "usersCount: 23880,\n" +
                    "tipCount: 35\n" +
                    "},\n" +
                    "specials: {\n" +
                    "count: 0,\n" +
                    "items: [ ]\n" +
                    "},\n" +
                    "hereNow: {\n" +
                    "count: 0,\n" +
                    "summary: \"Nobody here\",\n" +
                    "groups: [ ]\n" +
                    "},\n" +
                    "referralId: \"v-1465891837\",\n" +
                    "venueChains: [ ]\n" +
                    "},\n" +
                    "{\n" +
                    "id: \"5266a90711d23056d7c6dc67\",\n" +
                    "name: \"Bread & Butter\",\n" +
                    "contact: { },\n" +
                    "location: {\n" +
                    "address: \"303 5th Ave\",\n" +
                    "crossStreet: \"31st St & 5th Ave\",\n" +
                    "lat: 40.74653920998748,\n" +
                    "lng: -73.98575483525792,\n" +
                    "distance: 41,\n" +
                    "postalCode: \"10016\",\n" +
                    "cc: \"US\",\n" +
                    "city: \"New York\",\n" +
                    "state: \"NY\",\n" +
                    "country: \"United States\",\n" +
                    "formattedAddress: [\n" +
                    "\"303 5th Ave (31st St & 5th Ave)\",\n" +
                    "\"New York, NY 10016\",\n" +
                    "\"United States\"\n" +
                    "]\n" +
                    "},\n" +
                    "categories: [\n" +
                    "{\n" +
                    "id: \"4bf58dd8d48988d146941735\",\n" +
                    "name: \"Deli / Bodega\",\n" +
                    "pluralName: \"Delis / Bodegas\",\n" +
                    "shortName: \"Deli / Bodega\",\n" +
                    "icon: {\n" +
                    "prefix: \"https://ss3.4sqi.net/img/categories_v2/food/deli_\",\n" +
                    "suffix: \".png\"\n" +
                    "},\n" +
                    "primary: true\n" +
                    "}\n" +
                    "],\n" +
                    "verified: false,\n" +
                    "stats: {\n" +
                    "checkinsCount: 3974,\n" +
                    "usersCount: 1490,\n" +
                    "tipCount: 51\n" +
                    "},\n" +
                    "url: \"http://breadbutternyc.com\",\n" +
                    "delivery: {\n" +
                    "id: \"29882\",\n" +
                    "url: \"http://www.seamless.com/food-delivery/restaurant.29882.r?a=1026&utm_source=Foursquare&utm_medium=affiliate&utm_campaign=SeamlessOrderDeliveryLink\",\n" +
                    "provider: {\n" +
                    "name: \"seamless\"\n" +
                    "}\n" +
                    "},\n" +
                    "allowMenuUrlEdit: true,\n" +
                    "specials: {\n" +
                    "count: 0,\n" +
                    "items: [ ]\n" +
                    "},\n" +
                    "hereNow: {\n" +
                    "count: 0,\n" +
                    "summary: \"Nobody here\",\n" +
                    "groups: [ ]\n" +
                    "},\n" +
                    "referralId: \"v-1465891837\",\n" +
                    "venueChains: [ ]\n" +
                    "},\n" +
                    "{\n" +
                    "id: \"4b7578d6f964a5206b0e2ee3\",\n" +
                    "name: \"Cafe M\",\n" +
                    "contact: {\n" +
                    "phone: \"2125639444\",\n" +
                    "formattedPhone: \"(212) 563-9444\"\n" +
                    "},\n" +
                    "location: {\n" +
                    "address: \"315 5th Ave\",\n" +
                    "crossStreet: \"on E 32nd St btw 5th & Madison Ave\",\n" +
                    "lat: 40.746915580644455,\n" +
                    "lng: -73.98525416851044,\n" +
                    "distance: 58,\n" +
                    "postalCode: \"10016\",\n" +
                    "cc: \"US\",\n" +
                    "city: \"New York\",\n" +
                    "state: \"NY\",\n" +
                    "country: \"United States\",\n" +
                    "formattedAddress: [\n" +
                    "\"315 5th Ave (on E 32nd St btw 5th & Madison Ave)\",\n" +
                    "\"New York, NY 10016\",\n" +
                    "\"United States\"\n" +
                    "]\n" +
                    "},\n" +
                    "categories: [\n" +
                    "{\n" +
                    "id: \"4bf58dd8d48988d16d941735\",\n" +
                    "name: \"Café\",\n" +
                    "pluralName: \"Cafés\",\n" +
                    "shortName: \"Café\",\n" +
                    "icon: {\n" +
                    "prefix: \"https://ss3.4sqi.net/img/categories_v2/food/cafe_\",\n" +
                    "suffix: \".png\"\n" +
                    "},\n" +
                    "primary: true\n" +
                    "}\n" +
                    "],\n" +
                    "verified: false,\n" +
                    "stats: {\n" +
                    "checkinsCount: 3430,\n" +
                    "usersCount: 1240,\n" +
                    "tipCount: 43\n" +
                    "},\n" +
                    "url: \"http://cafemstudio.com\",\n" +
                    "hasMenu: true,\n" +
                    "menu: {\n" +
                    "type: \"Menu\",\n" +
                    "label: \"Menu\",\n" +
                    "anchor: \"View Menu\",\n" +
                    "url: \"https://foursquare.com/v/cafe-m/4b7578d6f964a5206b0e2ee3/menu\",\n" +
                    "mobileUrl: \"https://foursquare.com/v/4b7578d6f964a5206b0e2ee3/device_menu\"\n" +
                    "},\n" +
                    "allowMenuUrlEdit: true,\n" +
                    "specials: {\n" +
                    "count: 0,\n" +
                    "items: [ ]\n" +
                    "},\n" +
                    "hereNow: {\n" +
                    "count: 0,\n" +
                    "summary: \"Nobody here\",\n" +
                    "groups: [ ]\n" +
                    "},\n" +
                    "referralId: \"v-1465891837\",\n" +
                    "venueChains: [ ]\n" +
                    "},\n" +
                    "{\n" +
                    "id: \"4af60f11f964a520b10022e3\",\n" +
                    "name: \"Ichi Umi\",\n" +
                    "contact: {\n" +
                    "phone: \"2127251333\",\n" +
                    "formattedPhone: \"(212) 725-1333\",\n" +
                    "twitter: \"ichiuminyc\"\n" +
                    "},\n" +
                    "location: {\n" +
                    "address: \"6 E 32nd St\",\n" +
                    "crossStreet: \"btwn Madison & 5th Ave\",\n" +
                    "lat: 40.74689526022255,\n" +
                    "lng: -73.98500740528107,\n" +
                    "distance: 60,\n" +
                    "postalCode: \"10016\",\n" +
                    "cc: \"US\",\n" +
                    "city: \"New York\",\n" +
                    "state: \"NY\",\n" +
                    "country: \"United States\",\n" +
                    "formattedAddress: [\n" +
                    "\"6 E 32nd St (btwn Madison & 5th Ave)\",\n" +
                    "\"New York, NY 10016\",\n" +
                    "\"United States\"\n" +
                    "]\n" +
                    "},\n" +
                    "categories: [\n" +
                    "{\n" +
                    "id: \"4bf58dd8d48988d1d2941735\",\n" +
                    "name: \"Sushi Restaurant\",\n" +
                    "pluralName: \"Sushi Restaurants\",\n" +
                    "shortName: \"Sushi\",\n" +
                    "icon: {\n" +
                    "prefix: \"https://ss3.4sqi.net/img/categories_v2/food/sushi_\",\n" +
                    "suffix: \".png\"\n" +
                    "},\n" +
                    "primary: true\n" +
                    "}\n" +
                    "],\n" +
                    "verified: true,\n" +
                    "stats: {\n" +
                    "checkinsCount: 18334,\n" +
                    "usersCount: 9560,\n" +
                    "tipCount: 148\n" +
                    "},\n" +
                    "url: \"http://www.ichiumi.com/\",\n" +
                    "hasMenu: true,\n" +
                    "delivery: {\n" +
                    "id: \"43532\",\n" +
                    "url: \"http://www.seamless.com/food-delivery/restaurant.43532.r?a=1026&utm_source=Foursquare&utm_medium=affiliate&utm_campaign=SeamlessOrderDeliveryLink\",\n" +
                    "provider: {\n" +
                    "name: \"seamless\"\n" +
                    "}\n" +
                    "},\n" +
                    "menu: {\n" +
                    "type: \"Menu\",\n" +
                    "label: \"Menu\",\n" +
                    "anchor: \"View Menu\",\n" +
                    "url: \"https://foursquare.com/v/ichi-umi/4af60f11f964a520b10022e3/menu\",\n" +
                    "mobileUrl: \"https://foursquare.com/v/4af60f11f964a520b10022e3/device_menu\"\n" +
                    "},\n" +
                    "allowMenuUrlEdit: true,\n" +
                    "specials: {\n" +
                    "count: 0,\n" +
                    "items: [ ]\n" +
                    "},\n" +
                    "venuePage: {\n" +
                    "id: \"63862959\"\n" +
                    "},\n" +
                    "hereNow: {\n" +
                    "count: 0,\n" +
                    "summary: \"Nobody here\",\n" +
                    "groups: [ ]\n" +
                    "},\n" +
                    "referralId: \"v-1465891837\",\n" +
                    "venueChains: [ ]\n" +
                    "},\n" +
                    "{\n" +
                    "id: \"534491a7498ef6e319f1a84e\",\n" +
                    "name: \"Throwback Fitness\",\n" +
                    "contact: { },\n" +
                    "location: {\n" +
                    "address: \"303 5th Ave\",\n" +
                    "crossStreet: \"31st Street\",\n" +
                    "lat: 40.74658238567336,\n" +
                    "lng: -73.98565727114978,\n" +
                    "distance: 36,\n" +
                    "postalCode: \"10016\",\n" +
                    "cc: \"US\",\n" +
                    "city: \"New York\",\n" +
                    "state: \"NY\",\n" +
                    "country: \"United States\",\n" +
                    "formattedAddress: [\n" +
                    "\"303 5th Ave (31st Street)\",\n" +
                    "\"New York, NY 10016\",\n" +
                    "\"United States\"\n" +
                    "]\n" +
                    "},\n" +
                    "categories: [\n" +
                    "{\n" +
                    "id: \"52e81612bcbc57f1066b7a2e\",\n" +
                    "name: \"Sports Club\",\n" +
                    "pluralName: \"Sports Clubs\",\n" +
                    "shortName: \"Sports Club\",\n" +
                    "icon: {\n" +
                    "prefix: \"https://ss3.4sqi.net/img/categories_v2/shops/sports_outdoors_\",\n" +
                    "suffix: \".png\"\n" +
                    "},\n" +
                    "primary: true\n" +
                    "}\n" +
                    "],\n" +
                    "verified: false,\n" +
                    "stats: {\n" +
                    "checkinsCount: 1386,\n" +
                    "usersCount: 429,\n" +
                    "tipCount: 10\n" +
                    "},\n" +
                    "specials: {\n" +
                    "count: 0,\n" +
                    "items: [ ]\n" +
                    "},\n" +
                    "hereNow: {\n" +
                    "count: 0,\n" +
                    "summary: \"Nobody here\",\n" +
                    "groups: [ ]\n" +
                    "},\n" +
                    "referralId: \"v-1465891837\",\n" +
                    "venueChains: [ ]\n" +
                    "},\n" +
                    "{\n" +
                    "id: \"4a8a13b8f964a520d20920e3\",\n" +
                    "name: \"Hotel Chandler\",\n" +
                    "contact: {\n" +
                    "phone: \"2128896363\",\n" +
                    "formattedPhone: \"(212) 889-6363\",\n" +
                    "twitter: \"HotelChandler\",\n" +
                    "facebook: \"122254353642\",\n" +
                    "facebookUsername: \"hotelchandler\",\n" +
                    "facebookName: \"Hotel Chandler - New York\"\n" +
                    "},\n" +
                    "location: {\n" +
                    "address: \"12 E 31st St\",\n" +
                    "crossStreet: \"btwn 5th & Madison\",\n" +
                    "lat: 40.745971,\n" +
                    "lng: -73.9852329,\n" +
                    "distance: 47,\n" +
                    "postalCode: \"10016\",\n" +
                    "cc: \"US\",\n" +
                    "city: \"New York\",\n" +
                    "state: \"NY\",\n" +
                    "country: \"United States\",\n" +
                    "formattedAddress: [\n" +
                    "\"12 E 31st St (btwn 5th & Madison)\",\n" +
                    "\"New York, NY 10016\",\n" +
                    "\"United States\"\n" +
                    "]\n" +
                    "},\n" +
                    "categories: [\n" +
                    "{\n" +
                    "id: \"4bf58dd8d48988d1fa931735\",\n" +
                    "name: \"Hotel\",\n" +
                    "pluralName: \"Hotels\",\n" +
                    "shortName: \"Hotel\",\n" +
                    "icon: {\n" +
                    "prefix: \"https://ss3.4sqi.net/img/categories_v2/travel/hotel_\",\n" +
                    "suffix: \".png\"\n" +
                    "},\n" +
                    "primary: true\n" +
                    "}\n" +
                    "],\n" +
                    "verified: true,\n" +
                    "stats: {\n" +
                    "checkinsCount: 2914,\n" +
                    "usersCount: 1265,\n" +
                    "tipCount: 28\n" +
                    "},\n" +
                    "url: \"http://www.HotelChandler.com\",\n" +
                    "allowMenuUrlEdit: true,\n" +
                    "specials: {\n" +
                    "count: 0,\n" +
                    "items: [ ]\n" +
                    "},\n" +
                    "storeId: \"\",\n" +
                    "hereNow: {\n" +
                    "count: 0,\n" +
                    "summary: \"Nobody here\",\n" +
                    "groups: [ ]\n" +
                    "},\n" +
                    "referralId: \"v-1465891837\",\n" +
                    "venueChains: [ ]\n" +
                    "},\n" +
                    "{\n" +
                    "id: \"4c56d1106418a59371de7d0e\",\n" +
                    "name: \"Textile Building\",\n" +
                    "contact: { },\n" +
                    "location: {\n" +
                    "address: \"295 5th Ave\",\n" +
                    "lat: 40.74583508694546,\n" +
                    "lng: -73.98603604917463,\n" +
                    "distance: 88,\n" +
                    "postalCode: \"10016\",\n" +
                    "cc: \"US\",\n" +
                    "city: \"New York\",\n" +
                    "state: \"NY\",\n" +
                    "country: \"United States\",\n" +
                    "formattedAddress: [\n" +
                    "\"295 5th Ave\",\n" +
                    "\"New York, NY 10016\",\n" +
                    "\"United States\"\n" +
                    "]\n" +
                    "},\n" +
                    "categories: [\n" +
                    "{\n" +
                    "id: \"4bf58dd8d48988d124941735\",\n" +
                    "name: \"Office\",\n" +
                    "pluralName: \"Offices\",\n" +
                    "shortName: \"Office\",\n" +
                    "icon: {\n" +
                    "prefix: \"https://ss3.4sqi.net/img/categories_v2/building/default_\",\n" +
                    "suffix: \".png\"\n" +
                    "},\n" +
                    "primary: true\n" +
                    "}\n" +
                    "],\n" +
                    "verified: false,\n" +
                    "stats: {\n" +
                    "checkinsCount: 1122,\n" +
                    "usersCount: 271,\n" +
                    "tipCount: 4\n" +
                    "},\n" +
                    "specials: {\n" +
                    "count: 0,\n" +
                    "items: [ ]\n" +
                    "},\n" +
                    "hereNow: {\n" +
                    "count: 0,\n" +
                    "summary: \"Nobody here\",\n" +
                    "groups: [ ]\n" +
                    "},\n" +
                    "referralId: \"v-1465891837\",\n" +
                    "venueChains: [ ]\n" +
                    "},\n" +
                    "{\n" +
                    "id: \"4b6b5337f964a5200b012ce3\",\n" +
                    "name: \"303 5th Ave\",\n" +
                    "contact: { },\n" +
                    "location: {\n" +
                    "address: \"303 5th Ave\",\n" +
                    "crossStreet: \"31st St\",\n" +
                    "lat: 40.74669148099298,\n" +
                    "lng: -73.9857089227426,\n" +
                    "distance: 47,\n" +
                    "postalCode: \"10016\",\n" +
                    "cc: \"US\",\n" +
                    "city: \"New York\",\n" +
                    "state: \"NY\",\n" +
                    "country: \"United States\",\n" +
                    "formattedAddress: [\n" +
                    "\"303 5th Ave (31st St)\",\n" +
                    "\"New York, NY 10016\",\n" +
                    "\"United States\"\n" +
                    "]\n" +
                    "},\n" +
                    "categories: [\n" +
                    "{\n" +
                    "id: \"4bf58dd8d48988d124941735\",\n" +
                    "name: \"Office\",\n" +
                    "pluralName: \"Offices\",\n" +
                    "shortName: \"Office\",\n" +
                    "icon: {\n" +
                    "prefix: \"https://ss3.4sqi.net/img/categories_v2/building/default_\",\n" +
                    "suffix: \".png\"\n" +
                    "},\n" +
                    "primary: true\n" +
                    "}\n" +
                    "],\n" +
                    "verified: false,\n" +
                    "stats: {\n" +
                    "checkinsCount: 632,\n" +
                    "usersCount: 127,\n" +
                    "tipCount: 0\n" +
                    "},\n" +
                    "specials: {\n" +
                    "count: 0,\n" +
                    "items: [ ]\n" +
                    "},\n" +
                    "hereNow: {\n" +
                    "count: 0,\n" +
                    "summary: \"Nobody here\",\n" +
                    "groups: [ ]\n" +
                    "},\n" +
                    "referralId: \"v-1465891837\",\n" +
                    "venueChains: [ ]\n" +
                    "},\n" +
                    "{\n" +
                    "id: \"4d83d942d5fab60c3b42d59b\",\n" +
                    "name: \"Lot18 HQ\",\n" +
                    "contact: { },\n" +
                    "location: {\n" +
                    "address: \"6 E 32nd St\",\n" +
                    "crossStreet: \"at Madison Ave.\",\n" +
                    "lat: 40.7467703142969,\n" +
                    "lng: -73.98494562556753,\n" +
                    "distance: 51,\n" +
                    "postalCode: \"10016\",\n" +
                    "cc: \"US\",\n" +
                    "city: \"New York\",\n" +
                    "state: \"NY\",\n" +
                    "country: \"United States\",\n" +
                    "formattedAddress: [\n" +
                    "\"6 E 32nd St (at Madison Ave.)\",\n" +
                    "\"New York, NY 10016\",\n" +
                    "\"United States\"\n" +
                    "]\n" +
                    "},\n" +
                    "categories: [\n" +
                    "{\n" +
                    "id: \"4bf58dd8d48988d125941735\",\n" +
                    "name: \"Tech Startup\",\n" +
                    "pluralName: \"Tech Startups\",\n" +
                    "shortName: \"Tech Startup\",\n" +
                    "icon: {\n" +
                    "prefix: \"https://ss3.4sqi.net/img/categories_v2/shops/technology_\",\n" +
                    "suffix: \".png\"\n" +
                    "},\n" +
                    "primary: true\n" +
                    "}\n" +
                    "],\n" +
                    "verified: false,\n" +
                    "stats: {\n" +
                    "checkinsCount: 2437,\n" +
                    "usersCount: 252,\n" +
                    "tipCount: 10\n" +
                    "},\n" +
                    "specials: {\n" +
                    "count: 0,\n" +
                    "items: [ ]\n" +
                    "},\n" +
                    "hereNow: {\n" +
                    "count: 0,\n" +
                    "summary: \"Nobody here\",\n" +
                    "groups: [ ]\n" +
                    "},\n" +
                    "referralId: \"v-1465891837\",\n" +
                    "venueChains: [ ]\n" +
                    "},\n" +
                    "{\n" +
                    "id: \"5091347fe4b0d49482a12c5e\",\n" +
                    "name: \"Operative\",\n" +
                    "contact: {\n" +
                    "phone: \"2129948930\",\n" +
                    "formattedPhone: \"(212) 994-8930\",\n" +
                    "twitter: \"operativeinc\"\n" +
                    "},\n" +
                    "location: {\n" +
                    "address: \"6 E 32nd St\",\n" +
                    "crossStreet: \"at Madison Ave\",\n" +
                    "lat: 40.74684901662829,\n" +
                    "lng: -73.98494562556753,\n" +
                    "distance: 58,\n" +
                    "postalCode: \"10016\",\n" +
                    "cc: \"US\",\n" +
                    "city: \"New York\",\n" +
                    "state: \"NY\",\n" +
                    "country: \"United States\",\n" +
                    "formattedAddress: [\n" +
                    "\"6 E 32nd St (at Madison Ave)\",\n" +
                    "\"New York, NY 10016\",\n" +
                    "\"United States\"\n" +
                    "]\n" +
                    "},\n" +
                    "categories: [\n" +
                    "{\n" +
                    "id: \"4bf58dd8d48988d125941735\",\n" +
                    "name: \"Tech Startup\",\n" +
                    "pluralName: \"Tech Startups\",\n" +
                    "shortName: \"Tech Startup\",\n" +
                    "icon: {\n" +
                    "prefix: \"https://ss3.4sqi.net/img/categories_v2/shops/technology_\",\n" +
                    "suffix: \".png\"\n" +
                    "},\n" +
                    "primary: true\n" +
                    "}\n" +
                    "],\n" +
                    "verified: false,\n" +
                    "stats: {\n" +
                    "checkinsCount: 1527,\n" +
                    "usersCount: 70,\n" +
                    "tipCount: 4\n" +
                    "},\n" +
                    "url: \"http://www.operative.com\",\n" +
                    "specials: {\n" +
                    "count: 0,\n" +
                    "items: [ ]\n" +
                    "},\n" +
                    "hereNow: {\n" +
                    "count: 0,\n" +
                    "summary: \"Nobody here\",\n" +
                    "groups: [ ]\n" +
                    "},\n" +
                    "referralId: \"v-1465891837\",\n" +
                    "venueChains: [ ]\n" +
                    "},\n" +
                    "{\n" +
                    "id: \"50e76118e4b0651ebd7d21f0\",\n" +
                    "name: \"Cafe G\",\n" +
                    "contact: {\n" +
                    "phone: \"2126851003\",\n" +
                    "formattedPhone: \"(212) 685-1003\"\n" +
                    "},\n" +
                    "location: {\n" +
                    "address: \"315 5th Ave\",\n" +
                    "crossStreet: \"at E 32nd St\",\n" +
                    "lat: 40.74696231759129,\n" +
                    "lng: -73.98538291454315,\n" +
                    "distance: 63,\n" +
                    "postalCode: \"10016\",\n" +
                    "cc: \"US\",\n" +
                    "city: \"New York\",\n" +
                    "state: \"NY\",\n" +
                    "country: \"United States\",\n" +
                    "formattedAddress: [\n" +
                    "\"315 5th Ave (at E 32nd St)\",\n" +
                    "\"New York, NY 10016\",\n" +
                    "\"United States\"\n" +
                    "]\n" +
                    "},\n" +
                    "categories: [\n" +
                    "{\n" +
                    "id: \"4bf58dd8d48988d16d941735\",\n" +
                    "name: \"Café\",\n" +
                    "pluralName: \"Cafés\",\n" +
                    "shortName: \"Café\",\n" +
                    "icon: {\n" +
                    "prefix: \"https://ss3.4sqi.net/img/categories_v2/food/cafe_\",\n" +
                    "suffix: \".png\"\n" +
                    "},\n" +
                    "primary: true\n" +
                    "}\n" +
                    "],\n" +
                    "verified: false,\n" +
                    "stats: {\n" +
                    "checkinsCount: 402,\n" +
                    "usersCount: 243,\n" +
                    "tipCount: 13\n" +
                    "},\n" +
                    "url: \"http://ginsengus.com/servlet/StoreFront/\",\n" +
                    "allowMenuUrlEdit: true,\n" +
                    "specials: {\n" +
                    "count: 0,\n" +
                    "items: [ ]\n" +
                    "},\n" +
                    "hereNow: {\n" +
                    "count: 0,\n" +
                    "summary: \"Nobody here\",\n" +
                    "groups: [ ]\n" +
                    "},\n" +
                    "referralId: \"v-1465891837\",\n" +
                    "venueChains: [ ]\n" +
                    "},\n" +
                    "{\n" +
                    "id: \"556b232f498e644506d9259c\",\n" +
                    "name: \"Glimpx NYC\",\n" +
                    "contact: {\n" +
                    "phone: \"9176171791\",\n" +
                    "formattedPhone: \"(917) 617-1791\",\n" +
                    "twitter: \"glimpxnyc\"\n" +
                    "},\n" +
                    "location: {\n" +
                    "address: \"303 5th Ave\",\n" +
                    "crossStreet: \"31st.\",\n" +
                    "lat: 40.7465467371456,\n" +
                    "lng: -73.98535310037681,\n" +
                    "distance: 17,\n" +
                    "postalCode: \"10016\",\n" +
                    "cc: \"US\",\n" +
                    "city: \"New York\",\n" +
                    "state: \"NY\",\n" +
                    "country: \"United States\",\n" +
                    "formattedAddress: [\n" +
                    "\"303 5th Ave (31st.)\",\n" +
                    "\"New York, NY 10016\",\n" +
                    "\"United States\"\n" +
                    "]\n" +
                    "},\n" +
                    "categories: [\n" +
                    "{\n" +
                    "id: \"54541900498ea6ccd0202697\",\n" +
                    "name: \"Health & Beauty Service\",\n" +
                    "pluralName: \"Health & Beauty Services\",\n" +
                    "shortName: \"Health & Beauty\",\n" +
                    "icon: {\n" +
                    "prefix: \"https://ss3.4sqi.net/img/categories_v2/shops/salon_barber_\",\n" +
                    "suffix: \".png\"\n" +
                    "},\n" +
                    "primary: true\n" +
                    "}\n" +
                    "],\n" +
                    "verified: false,\n" +
                    "stats: {\n" +
                    "checkinsCount: 2,\n" +
                    "usersCount: 2,\n" +
                    "tipCount: 0\n" +
                    "},\n" +
                    "url: \"http://www.glimpxnyc.com\",\n" +
                    "allowMenuUrlEdit: true,\n" +
                    "specials: {\n" +
                    "count: 0,\n" +
                    "items: [ ]\n" +
                    "},\n" +
                    "hereNow: {\n" +
                    "count: 0,\n" +
                    "summary: \"Nobody here\",\n" +
                    "groups: [ ]\n" +
                    "},\n" +
                    "referralId: \"v-1465891837\",\n" +
                    "venueChains: [ ]\n" +
                    "},\n" +
                    "{\n" +
                    "id: \"4faa8549e4b04f61e2ceb0b7\",\n" +
                    "name: \"Dataminr\",\n" +
                    "contact: {\n" +
                    "twitter: \"dataminr\"\n" +
                    "},\n" +
                    "location: {\n" +
                    "address: \"99 Madison Ave\",\n" +
                    "crossStreet: \"29th Street\",\n" +
                    "lat: 40.74648194664604,\n" +
                    "lng: -73.98510058087857,\n" +
                    "distance: 19,\n" +
                    "postalCode: \"10016\",\n" +
                    "cc: \"US\",\n" +
                    "city: \"New York\",\n" +
                    "state: \"NY\",\n" +
                    "country: \"United States\",\n" +
                    "formattedAddress: [\n" +
                    "\"99 Madison Ave (29th Street)\",\n" +
                    "\"New York, NY 10016\",\n" +
                    "\"United States\"\n" +
                    "]\n" +
                    "},\n" +
                    "categories: [\n" +
                    "{\n" +
                    "id: \"4bf58dd8d48988d125941735\",\n" +
                    "name: \"Tech Startup\",\n" +
                    "pluralName: \"Tech Startups\",\n" +
                    "shortName: \"Tech Startup\",\n" +
                    "icon: {\n" +
                    "prefix: \"https://ss3.4sqi.net/img/categories_v2/shops/technology_\",\n" +
                    "suffix: \".png\"\n" +
                    "},\n" +
                    "primary: true\n" +
                    "}\n" +
                    "],\n" +
                    "verified: false,\n" +
                    "stats: {\n" +
                    "checkinsCount: 236,\n" +
                    "usersCount: 37,\n" +
                    "tipCount: 0\n" +
                    "},\n" +
                    "url: \"http://www.dataminr.com\",\n" +
                    "specials: {\n" +
                    "count: 0,\n" +
                    "items: [ ]\n" +
                    "},\n" +
                    "hereNow: {\n" +
                    "count: 0,\n" +
                    "summary: \"Nobody here\",\n" +
                    "groups: [ ]\n" +
                    "},\n" +
                    "referralId: \"v-1465891837\",\n" +
                    "venueChains: [ ]\n" +
                    "},\n" +
                    "{\n" +
                    "id: \"5477c694498e910f8bc93582\",\n" +
                    "name: \"강호동백정\",\n" +
                    "contact: { },\n" +
                    "location: {\n" +
                    "lat: 40.74620989531765,\n" +
                    "lng: -73.98528423145568,\n" +
                    "distance: 20,\n" +
                    "cc: \"US\",\n" +
                    "city: \"New York\",\n" +
                    "state: \"NY\",\n" +
                    "country: \"United States\",\n" +
                    "formattedAddress: [\n" +
                    "\"New York, NY\",\n" +
                    "\"United States\"\n" +
                    "]\n" +
                    "},\n" +
                    "categories: [\n" +
                    "{\n" +
                    "id: \"4bf58dd8d48988d113941735\",\n" +
                    "name: \"Korean Restaurant\",\n" +
                    "pluralName: \"Korean Restaurants\",\n" +
                    "shortName: \"Korean\",\n" +
                    "icon: {\n" +
                    "prefix: \"https://ss3.4sqi.net/img/categories_v2/food/korean_\",\n" +
                    "suffix: \".png\"\n" +
                    "},\n" +
                    "primary: true\n" +
                    "}\n" +
                    "],\n" +
                    "verified: false,\n" +
                    "stats: {\n" +
                    "checkinsCount: 5,\n" +
                    "usersCount: 5,\n" +
                    "tipCount: 0\n" +
                    "},\n" +
                    "allowMenuUrlEdit: true,\n" +
                    "specials: {\n" +
                    "count: 0,\n" +
                    "items: [ ]\n" +
                    "},\n" +
                    "hereNow: {\n" +
                    "count: 0,\n" +
                    "summary: \"Nobody here\",\n" +
                    "groups: [ ]\n" +
                    "},\n" +
                    "referralId: \"v-1465891837\",\n" +
                    "venueChains: [ ]\n" +
                    "},\n" +
                    "{\n" +
                    "id: \"4b61cde6f964a520e5232ae3\",\n" +
                    "name: \"Willoughby's Camera Emporium\",\n" +
                    "contact: {\n" +
                    "phone: \"2125641600\",\n" +
                    "formattedPhone: \"(212) 564-1600\",\n" +
                    "twitter: \"Willoughbys\",\n" +
                    "facebook: \"142858539113100\",\n" +
                    "facebookUsername: \"willoughbyscamera\",\n" +
                    "facebookName: \"Willoughby's Camera\"\n" +
                    "},\n" +
                    "location: {\n" +
                    "address: \"298 5th Ave\",\n" +
                    "crossStreet: \"btwn 5th And 6th Ave.\",\n" +
                    "lat: 40.7464766581497,\n" +
                    "lng: -73.98635387420654,\n" +
                    "distance: 89,\n" +
                    "postalCode: \"10001\",\n" +
                    "cc: \"US\",\n" +
                    "city: \"New York\",\n" +
                    "state: \"NY\",\n" +
                    "country: \"United States\",\n" +
                    "formattedAddress: [\n" +
                    "\"298 5th Ave (btwn 5th And 6th Ave.)\",\n" +
                    "\"New York, NY 10001\",\n" +
                    "\"United States\"\n" +
                    "]\n" +
                    "},\n" +
                    "categories: [\n" +
                    "{\n" +
                    "id: \"4eb1bdf03b7b55596b4a7491\",\n" +
                    "name: \"Camera Store\",\n" +
                    "pluralName: \"Camera Stores\",\n" +
                    "shortName: \"Camera Store\",\n" +
                    "icon: {\n" +
                    "prefix: \"https://ss3.4sqi.net/img/categories_v2/shops/camerastore_\",\n" +
                    "suffix: \".png\"\n" +
                    "},\n" +
                    "primary: true\n" +
                    "}\n" +
                    "],\n" +
                    "verified: true,\n" +
                    "stats: {\n" +
                    "checkinsCount: 405,\n" +
                    "usersCount: 231,\n" +
                    "tipCount: 7\n" +
                    "},\n" +
                    "url: \"http://www.willoughbys.com/\",\n" +
                    "allowMenuUrlEdit: true,\n" +
                    "specials: {\n" +
                    "count: 0,\n" +
                    "items: [ ]\n" +
                    "},\n" +
                    "venuePage: {\n" +
                    "id: \"61761468\"\n" +
                    "},\n" +
                    "storeId: \"\",\n" +
                    "hereNow: {\n" +
                    "count: 0,\n" +
                    "summary: \"Nobody here\",\n" +
                    "groups: [ ]\n" +
                    "},\n" +
                    "referralId: \"v-1465891837\",\n" +
                    "venueChains: [ ]\n" +
                    "},\n" +
                    "{\n" +
                    "id: \"4e01202ad4c0836163dac19b\",\n" +
                    "name: \"99¢ Fresh Pizza\",\n" +
                    "contact: {\n" +
                    "phone: \"2124811413\",\n" +
                    "formattedPhone: \"(212) 481-1413\"\n" +
                    "},\n" +
                    "location: {\n" +
                    "address: \"325 5th Ave\",\n" +
                    "crossStreet: \"btw 32nd and 33rd St\",\n" +
                    "lat: 40.747305731541104,\n" +
                    "lng: -73.98517370223999,\n" +
                    "distance: 101,\n" +
                    "postalCode: \"10016\",\n" +
                    "cc: \"US\",\n" +
                    "city: \"New York\",\n" +
                    "state: \"NY\",\n" +
                    "country: \"United States\",\n" +
                    "formattedAddress: [\n" +
                    "\"325 5th Ave (btw 32nd and 33rd St)\",\n" +
                    "\"New York, NY 10016\",\n" +
                    "\"United States\"\n" +
                    "]\n" +
                    "},\n" +
                    "categories: [\n" +
                    "{\n" +
                    "id: \"4bf58dd8d48988d1ca941735\",\n" +
                    "name: \"Pizza Place\",\n" +
                    "pluralName: \"Pizza Places\",\n" +
                    "shortName: \"Pizza\",\n" +
                    "icon: {\n" +
                    "prefix: \"https://ss3.4sqi.net/img/categories_v2/food/pizza_\",\n" +
                    "suffix: \".png\"\n" +
                    "},\n" +
                    "primary: true\n" +
                    "}\n" +
                    "],\n" +
                    "verified: false,\n" +
                    "stats: {\n" +
                    "checkinsCount: 1081,\n" +
                    "usersCount: 410,\n" +
                    "tipCount: 19\n" +
                    "},\n" +
                    "allowMenuUrlEdit: true,\n" +
                    "specials: {\n" +
                    "count: 0,\n" +
                    "items: [ ]\n" +
                    "},\n" +
                    "hereNow: {\n" +
                    "count: 0,\n" +
                    "summary: \"Nobody here\",\n" +
                    "groups: [ ]\n" +
                    "},\n" +
                    "referralId: \"v-1465891837\",\n" +
                    "venueChains: [ ]\n" +
                    "},\n" +
                    "{\n" +
                    "id: \"512b87e4e4b066f6b4f3639e\",\n" +
                    "name: \"Popdust HQ 3.0\",\n" +
                    "contact: { },\n" +
                    "location: {\n" +
                    "address: \"6 E 32nd St\",\n" +
                    "crossStreet: \"Fifth Avenue\",\n" +
                    "lat: 40.746198027142064,\n" +
                    "lng: -73.9852268406692,\n" +
                    "distance: 22,\n" +
                    "postalCode: \"10016\",\n" +
                    "cc: \"US\",\n" +
                    "city: \"New York\",\n" +
                    "state: \"NY\",\n" +
                    "country: \"United States\",\n" +
                    "formattedAddress: [\n" +
                    "\"6 E 32nd St (Fifth Avenue)\",\n" +
                    "\"New York, NY 10016\",\n" +
                    "\"United States\"\n" +
                    "]\n" +
                    "},\n" +
                    "categories: [\n" +
                    "{\n" +
                    "id: \"4bf58dd8d48988d125941735\",\n" +
                    "name: \"Tech Startup\",\n" +
                    "pluralName: \"Tech Startups\",\n" +
                    "shortName: \"Tech Startup\",\n" +
                    "icon: {\n" +
                    "prefix: \"https://ss3.4sqi.net/img/categories_v2/shops/technology_\",\n" +
                    "suffix: \".png\"\n" +
                    "},\n" +
                    "primary: true\n" +
                    "}\n" +
                    "],\n" +
                    "verified: false,\n" +
                    "stats: {\n" +
                    "checkinsCount: 39,\n" +
                    "usersCount: 4,\n" +
                    "tipCount: 0\n" +
                    "},\n" +
                    "specials: {\n" +
                    "count: 0,\n" +
                    "items: [ ]\n" +
                    "},\n" +
                    "hereNow: {\n" +
                    "count: 0,\n" +
                    "summary: \"Nobody here\",\n" +
                    "groups: [ ]\n" +
                    "},\n" +
                    "referralId: \"v-1465891837\",\n" +
                    "venueChains: [ ]\n" +
                    "},\n" +
                    "{\n" +
                    "id: \"4eb270352c5b5915cdeacd1e\",\n" +
                    "name: \"6 East 32nd Street\",\n" +
                    "contact: { },\n" +
                    "location: {\n" +
                    "address: \"6 E 32nd St\",\n" +
                    "crossStreet: \"Between 5th and Madison\",\n" +
                    "lat: 40.74645356150643,\n" +
                    "lng: -73.98503745095212,\n" +
                    "distance: 22,\n" +
                    "postalCode: \"10016\",\n" +
                    "cc: \"US\",\n" +
                    "city: \"New York\",\n" +
                    "state: \"NY\",\n" +
                    "country: \"United States\",\n" +
                    "formattedAddress: [\n" +
                    "\"6 E 32nd St (Between 5th and Madison)\",\n" +
                    "\"New York, NY 10016\",\n" +
                    "\"United States\"\n" +
                    "]\n" +
                    "},\n" +
                    "categories: [\n" +
                    "{\n" +
                    "id: \"4bf58dd8d48988d130941735\",\n" +
                    "name: \"Building\",\n" +
                    "pluralName: \"Buildings\",\n" +
                    "shortName: \"Building\",\n" +
                    "icon: {\n" +
                    "prefix: \"https://ss3.4sqi.net/img/categories_v2/building/default_\",\n" +
                    "suffix: \".png\"\n" +
                    "},\n" +
                    "primary: true\n" +
                    "}\n" +
                    "],\n" +
                    "verified: false,\n" +
                    "stats: {\n" +
                    "checkinsCount: 197,\n" +
                    "usersCount: 20,\n" +
                    "tipCount: 1\n" +
                    "},\n" +
                    "specials: {\n" +
                    "count: 0,\n" +
                    "items: [ ]\n" +
                    "},\n" +
                    "hereNow: {\n" +
                    "count: 0,\n" +
                    "summary: \"Nobody here\",\n" +
                    "groups: [ ]\n" +
                    "},\n" +
                    "referralId: \"v-1465891837\",\n" +
                    "venueChains: [ ]\n" +
                    "},\n" +
                    "{\n" +
                    "id: \"5687765a498ef6dcc999305e\",\n" +
                    "name: \"Bared Monkey Laser Spa\",\n" +
                    "contact: {\n" +
                    "phone: \"2122569777\",\n" +
                    "formattedPhone: \"(212) 256-9777\"\n" +
                    "},\n" +
                    "location: {\n" +
                    "address: \"303 5th Ave Rm 514\",\n" +
                    "lat: 40.74660545082304,\n" +
                    "lng: -73.98535883945247,\n" +
                    "distance: 23,\n" +
                    "postalCode: \"10016\",\n" +
                    "cc: \"US\",\n" +
                    "city: \"New York\",\n" +
                    "state: \"NY\",\n" +
                    "country: \"United States\",\n" +
                    "formattedAddress: [\n" +
                    "\"303 5th Ave Rm 514\",\n" +
                    "\"New York, NY 10016\",\n" +
                    "\"United States\"\n" +
                    "]\n" +
                    "},\n" +
                    "categories: [\n" +
                    "{\n" +
                    "id: \"54541900498ea6ccd0202697\",\n" +
                    "name: \"Health & Beauty Service\",\n" +
                    "pluralName: \"Health & Beauty Services\",\n" +
                    "shortName: \"Health & Beauty\",\n" +
                    "icon: {\n" +
                    "prefix: \"https://ss3.4sqi.net/img/categories_v2/shops/salon_barber_\",\n" +
                    "suffix: \".png\"\n" +
                    "},\n" +
                    "primary: true\n" +
                    "}\n" +
                    "],\n" +
                    "verified: false,\n" +
                    "stats: {\n" +
                    "checkinsCount: 10,\n" +
                    "usersCount: 6,\n" +
                    "tipCount: 0\n" +
                    "},\n" +
                    "url: \"http://www.baredmonkey.com\",\n" +
                    "allowMenuUrlEdit: true,\n" +
                    "specials: {\n" +
                    "count: 0,\n" +
                    "items: [ ]\n" +
                    "},\n" +
                    "hereNow: {\n" +
                    "count: 0,\n" +
                    "summary: \"Nobody here\",\n" +
                    "groups: [ ]\n" +
                    "},\n" +
                    "referralId: \"v-1465891837\",\n" +
                    "venueChains: [ ]\n" +
                    "},\n" +
                    "{\n" +
                    "id: \"56aa5c9c498e99059a995323\",\n" +
                    "name: \"Taryn Brows\",\n" +
                    "contact: {\n" +
                    "phone: \"9172971002\",\n" +
                    "formattedPhone: \"(917) 297-1002\"\n" +
                    "},\n" +
                    "location: {\n" +
                    "address: \"303 5th Ave Rm 1915\",\n" +
                    "lat: 40.746622,\n" +
                    "lng: -73.985287,\n" +
                    "distance: 25,\n" +
                    "postalCode: \"10016\",\n" +
                    "cc: \"US\",\n" +
                    "city: \"New York\",\n" +
                    "state: \"NY\",\n" +
                    "country: \"United States\",\n" +
                    "formattedAddress: [\n" +
                    "\"303 5th Ave Rm 1915\",\n" +
                    "\"New York, NY 10016\",\n" +
                    "\"United States\"\n" +
                    "]\n" +
                    "},\n" +
                    "categories: [\n" +
                    "{\n" +
                    "id: \"54541900498ea6ccd0202697\",\n" +
                    "name: \"Health & Beauty Service\",\n" +
                    "pluralName: \"Health & Beauty Services\",\n" +
                    "shortName: \"Health & Beauty\",\n" +
                    "icon: {\n" +
                    "prefix: \"https://ss3.4sqi.net/img/categories_v2/shops/salon_barber_\",\n" +
                    "suffix: \".png\"\n" +
                    "},\n" +
                    "primary: true\n" +
                    "}\n" +
                    "],\n" +
                    "verified: false,\n" +
                    "stats: {\n" +
                    "checkinsCount: 4,\n" +
                    "usersCount: 2,\n" +
                    "tipCount: 0\n" +
                    "},\n" +
                    "allowMenuUrlEdit: true,\n" +
                    "specials: {\n" +
                    "count: 0,\n" +
                    "items: [ ]\n" +
                    "},\n" +
                    "hereNow: {\n" +
                    "count: 0,\n" +
                    "summary: \"Nobody here\",\n" +
                    "groups: [ ]\n" +
                    "},\n" +
                    "referralId: \"v-1465891837\",\n" +
                    "venueChains: [ ]\n" +
                    "},\n" +
                    "{\n" +
                    "id: \"4e959c4b8b8105c15ea306af\",\n" +
                    "name: \"Delta Galil USA Inc.\",\n" +
                    "contact: {\n" +
                    "phone: \"2127106440\",\n" +
                    "formattedPhone: \"(212) 710-6440\"\n" +
                    "},\n" +
                    "location: {\n" +
                    "address: \"6 E 32nd St Fl 9\",\n" +
                    "lat: 40.746344279687364,\n" +
                    "lng: -73.98499727735177,\n" +
                    "distance: 26,\n" +
                    "postalCode: \"10016\",\n" +
                    "cc: \"US\",\n" +
                    "city: \"New York\",\n" +
                    "state: \"NY\",\n" +
                    "country: \"United States\",\n" +
                    "formattedAddress: [\n" +
                    "\"6 E 32nd St Fl 9\",\n" +
                    "\"New York, NY 10016\",\n" +
                    "\"United States\"\n" +
                    "]\n" +
                    "},\n" +
                    "categories: [\n" +
                    "{\n" +
                    "id: \"4bf58dd8d48988d174941735\",\n" +
                    "name: \"Coworking Space\",\n" +
                    "pluralName: \"Coworking Spaces\",\n" +
                    "shortName: \"Coworking Space\",\n" +
                    "icon: {\n" +
                    "prefix: \"https://ss3.4sqi.net/img/categories_v2/building/office_coworkingspace_\",\n" +
                    "suffix: \".png\"\n" +
                    "},\n" +
                    "primary: true\n" +
                    "}\n" +
                    "],\n" +
                    "verified: false,\n" +
                    "stats: {\n" +
                    "checkinsCount: 73,\n" +
                    "usersCount: 8,\n" +
                    "tipCount: 0\n" +
                    "},\n" +
                    "specials: {\n" +
                    "count: 0,\n" +
                    "items: [ ]\n" +
                    "},\n" +
                    "hereNow: {\n" +
                    "count: 0,\n" +
                    "summary: \"Nobody here\",\n" +
                    "groups: [ ]\n" +
                    "},\n" +
                    "referralId: \"v-1465891837\",\n" +
                    "venueChains: [ ]\n" +
                    "},\n" +
                    "{\n" +
                    "id: \"4e5e446ab993f2a09a9150b3\",\n" +
                    "name: \"Persistent Systems\",\n" +
                    "contact: {\n" +
                    "phone: \"2125615895\",\n" +
                    "formattedPhone: \"(212) 561-5895\"\n" +
                    "},\n" +
                    "location: {\n" +
                    "address: \"303 5th Ave\",\n" +
                    "crossStreet: \"31st St\",\n" +
                    "lat: 40.74659045314873,\n" +
                    "lng: -73.98565649986267,\n" +
                    "distance: 37,\n" +
                    "postalCode: \"10016\",\n" +
                    "cc: \"US\",\n" +
                    "city: \"New York\",\n" +
                    "state: \"NY\",\n" +
                    "country: \"United States\",\n" +
                    "formattedAddress: [\n" +
                    "\"303 5th Ave (31st St)\",\n" +
                    "\"New York, NY 10016\",\n" +
                    "\"United States\"\n" +
                    "]\n" +
                    "},\n" +
                    "categories: [\n" +
                    "{\n" +
                    "id: \"4bf58dd8d48988d125941735\",\n" +
                    "name: \"Tech Startup\",\n" +
                    "pluralName: \"Tech Startups\",\n" +
                    "shortName: \"Tech Startup\",\n" +
                    "icon: {\n" +
                    "prefix: \"https://ss3.4sqi.net/img/categories_v2/shops/technology_\",\n" +
                    "suffix: \".png\"\n" +
                    "},\n" +
                    "primary: true\n" +
                    "}\n" +
                    "],\n" +
                    "verified: false,\n" +
                    "stats: {\n" +
                    "checkinsCount: 436,\n" +
                    "usersCount: 13,\n" +
                    "tipCount: 0\n" +
                    "},\n" +
                    "url: \"http://www.persistentsystems.com\",\n" +
                    "specials: {\n" +
                    "count: 0,\n" +
                    "items: [ ]\n" +
                    "},\n" +
                    "hereNow: {\n" +
                    "count: 0,\n" +
                    "summary: \"Nobody here\",\n" +
                    "groups: [ ]\n" +
                    "},\n" +
                    "referralId: \"v-1465891837\",\n" +
                    "venueChains: [ ]\n" +
                    "},\n" +
                    "{\n" +
                    "id: \"4ba7b867f964a520e8ac39e3\",\n" +
                    "name: \"Sunshine Sachs\",\n" +
                    "contact: {\n" +
                    "phone: \"2126912800\",\n" +
                    "formattedPhone: \"(212) 691-2800\"\n" +
                    "},\n" +
                    "location: {\n" +
                    "address: \"136 Madison Ave\",\n" +
                    "crossStreet: \"btwn 31st ST and 32nd St\",\n" +
                    "lat: 40.74626532406351,\n" +
                    "lng: -73.98420810699463,\n" +
                    "distance: 93,\n" +
                    "postalCode: \"10016\",\n" +
                    "cc: \"US\",\n" +
                    "city: \"New York\",\n" +
                    "state: \"NY\",\n" +
                    "country: \"United States\",\n" +
                    "formattedAddress: [\n" +
                    "\"136 Madison Ave (btwn 31st ST and 32nd St)\",\n" +
                    "\"New York, NY 10016\",\n" +
                    "\"United States\"\n" +
                    "]\n" +
                    "},\n" +
                    "categories: [\n" +
                    "{\n" +
                    "id: \"4bf58dd8d48988d124941735\",\n" +
                    "name: \"Office\",\n" +
                    "pluralName: \"Offices\",\n" +
                    "shortName: \"Office\",\n" +
                    "icon: {\n" +
                    "prefix: \"https://ss3.4sqi.net/img/categories_v2/building/default_\",\n" +
                    "suffix: \".png\"\n" +
                    "},\n" +
                    "primary: true\n" +
                    "}\n" +
                    "],\n" +
                    "verified: false,\n" +
                    "stats: {\n" +
                    "checkinsCount: 3102,\n" +
                    "usersCount: 183,\n" +
                    "tipCount: 1\n" +
                    "},\n" +
                    "specials: {\n" +
                    "count: 0,\n" +
                    "items: [ ]\n" +
                    "},\n" +
                    "hereNow: {\n" +
                    "count: 0,\n" +
                    "summary: \"Nobody here\",\n" +
                    "groups: [ ]\n" +
                    "},\n" +
                    "referralId: \"v-1465891837\",\n" +
                    "venueChains: [ ]\n" +
                    "},\n" +
                    "{\n" +
                    "id: \"4b114be7f964a520e67923e3\",\n" +
                    "name: \"J.J. Hat Center\",\n" +
                    "contact: {\n" +
                    "phone: \"8006221911\",\n" +
                    "formattedPhone: \"(800) 622-1911\"\n" +
                    "},\n" +
                    "location: {\n" +
                    "address: \"310 5th Ave\",\n" +
                    "crossStreet: \"btw 31st and 32nd\",\n" +
                    "lat: 40.746997959946945,\n" +
                    "lng: -73.98561135861114,\n" +
                    "distance: 72,\n" +
                    "postalCode: \"10001\",\n" +
                    "cc: \"US\",\n" +
                    "city: \"New York\",\n" +
                    "state: \"NY\",\n" +
                    "country: \"United States\",\n" +
                    "formattedAddress: [\n" +
                    "\"310 5th Ave (btw 31st and 32nd)\",\n" +
                    "\"New York, NY 10001\",\n" +
                    "\"United States\"\n" +
                    "]\n" +
                    "},\n" +
                    "categories: [\n" +
                    "{\n" +
                    "id: \"4bf58dd8d48988d104951735\",\n" +
                    "name: \"Boutique\",\n" +
                    "pluralName: \"Boutiques\",\n" +
                    "shortName: \"Boutique\",\n" +
                    "icon: {\n" +
                    "prefix: \"https://ss3.4sqi.net/img/categories_v2/shops/apparel_boutique_\",\n" +
                    "suffix: \".png\"\n" +
                    "},\n" +
                    "primary: true\n" +
                    "}\n" +
                    "],\n" +
                    "verified: false,\n" +
                    "stats: {\n" +
                    "checkinsCount: 536,\n" +
                    "usersCount: 419,\n" +
                    "tipCount: 13\n" +
                    "},\n" +
                    "allowMenuUrlEdit: true,\n" +
                    "specials: {\n" +
                    "count: 0,\n" +
                    "items: [ ]\n" +
                    "},\n" +
                    "hereNow: {\n" +
                    "count: 0,\n" +
                    "summary: \"Nobody here\",\n" +
                    "groups: [ ]\n" +
                    "},\n" +
                    "referralId: \"v-1465891837\",\n" +
                    "venueChains: [ ]\n" +
                    "},\n" +
                    "{\n" +
                    "id: \"4efe024000396c3b17c070e1\",\n" +
                    "name: \"Take 31\",\n" +
                    "contact: {\n" +
                    "phone: \"6463989990\",\n" +
                    "formattedPhone: \"(646) 398-9990\"\n" +
                    "},\n" +
                    "location: {\n" +
                    "address: \"15 E 31st St\",\n" +
                    "crossStreet: \"btwn 5th & Madison\",\n" +
                    "lat: 40.746037732758055,\n" +
                    "lng: -73.98463726043701,\n" +
                    "distance: 68,\n" +
                    "postalCode: \"10016\",\n" +
                    "cc: \"US\",\n" +
                    "city: \"New York\",\n" +
                    "state: \"NY\",\n" +
                    "country: \"United States\",\n" +
                    "formattedAddress: [\n" +
                    "\"15 E 31st St (btwn 5th & Madison)\",\n" +
                    "\"New York, NY 10016\",\n" +
                    "\"United States\"\n" +
                    "]\n" +
                    "},\n" +
                    "categories: [\n" +
                    "{\n" +
                    "id: \"4bf58dd8d48988d113941735\",\n" +
                    "name: \"Korean Restaurant\",\n" +
                    "pluralName: \"Korean Restaurants\",\n" +
                    "shortName: \"Korean\",\n" +
                    "icon: {\n" +
                    "prefix: \"https://ss3.4sqi.net/img/categories_v2/food/korean_\",\n" +
                    "suffix: \".png\"\n" +
                    "},\n" +
                    "primary: true\n" +
                    "}\n" +
                    "],\n" +
                    "verified: false,\n" +
                    "stats: {\n" +
                    "checkinsCount: 6574,\n" +
                    "usersCount: 2761,\n" +
                    "tipCount: 48\n" +
                    "},\n" +
                    "url: \"http://www.take31nyc.com\",\n" +
                    "hasMenu: true,\n" +
                    "menu: {\n" +
                    "type: \"Menu\",\n" +
                    "label: \"Menu\",\n" +
                    "anchor: \"View Menu\",\n" +
                    "url: \"https://foursquare.com/v/take-31/4efe024000396c3b17c070e1/menu\",\n" +
                    "mobileUrl: \"https://foursquare.com/v/4efe024000396c3b17c070e1/device_menu\"\n" +
                    "},\n" +
                    "allowMenuUrlEdit: true,\n" +
                    "specials: {\n" +
                    "count: 0,\n" +
                    "items: [ ]\n" +
                    "},\n" +
                    "hereNow: {\n" +
                    "count: 0,\n" +
                    "summary: \"Nobody here\",\n" +
                    "groups: [ ]\n" +
                    "},\n" +
                    "referralId: \"v-1465891837\",\n" +
                    "venueChains: [ ]\n" +
                    "},\n" +
                    "{\n" +
                    "id: \"5061a3f4e4b00bc612d42ae3\",\n" +
                    "name: \"Avalon Hotel\",\n" +
                    "contact: { },\n" +
                    "location: {\n" +
                    "address: \"16 E 32nd St\",\n" +
                    "crossStreet: \"5th Avenue\",\n" +
                    "lat: 40.74674976154123,\n" +
                    "lng: -73.98481936559202,\n" +
                    "distance: 56,\n" +
                    "postalCode: \"10016\",\n" +
                    "cc: \"US\",\n" +
                    "city: \"New York\",\n" +
                    "state: \"NY\",\n" +
                    "country: \"United States\",\n" +
                    "formattedAddress: [\n" +
                    "\"16 E 32nd St (5th Avenue)\",\n" +
                    "\"New York, NY 10016\",\n" +
                    "\"United States\"\n" +
                    "]\n" +
                    "},\n" +
                    "categories: [\n" +
                    "{\n" +
                    "id: \"4bf58dd8d48988d1fa931735\",\n" +
                    "name: \"Hotel\",\n" +
                    "pluralName: \"Hotels\",\n" +
                    "shortName: \"Hotel\",\n" +
                    "icon: {\n" +
                    "prefix: \"https://ss3.4sqi.net/img/categories_v2/travel/hotel_\",\n" +
                    "suffix: \".png\"\n" +
                    "},\n" +
                    "primary: true\n" +
                    "}\n" +
                    "],\n" +
                    "verified: false,\n" +
                    "stats: {\n" +
                    "checkinsCount: 924,\n" +
                    "usersCount: 406,\n" +
                    "tipCount: 9\n" +
                    "},\n" +
                    "allowMenuUrlEdit: true,\n" +
                    "specials: {\n" +
                    "count: 0,\n" +
                    "items: [ ]\n" +
                    "},\n" +
                    "hereNow: {\n" +
                    "count: 0,\n" +
                    "summary: \"Nobody here\",\n" +
                    "groups: [ ]\n" +
                    "},\n" +
                    "referralId: \"v-1465891837\",\n" +
                    "venueChains: [ ]\n" +
                    "},\n" +
                    "{\n" +
                    "id: \"52147461abdfe6d3e56334b3\",\n" +
                    "name: \"Juni\",\n" +
                    "contact: {\n" +
                    "phone: \"2129958599\",\n" +
                    "formattedPhone: \"(212) 995-8599\",\n" +
                    "twitter: \"juni_nyc\",\n" +
                    "facebook: \"211790578951161\",\n" +
                    "facebookUsername: \"JuniNYC\",\n" +
                    "facebookName: \"Juni\"\n" +
                    "},\n" +
                    "location: {\n" +
                    "address: \"12 E 31st St\",\n" +
                    "lat: 40.745919,\n" +
                    "lng: -73.985272,\n" +
                    "distance: 53,\n" +
                    "postalCode: \"10016\",\n" +
                    "cc: \"US\",\n" +
                    "city: \"New York\",\n" +
                    "state: \"NY\",\n" +
                    "country: \"United States\",\n" +
                    "formattedAddress: [\n" +
                    "\"12 E 31st St\",\n" +
                    "\"New York, NY 10016\",\n" +
                    "\"United States\"\n" +
                    "]\n" +
                    "},\n" +
                    "categories: [\n" +
                    "{\n" +
                    "id: \"4bf58dd8d48988d157941735\",\n" +
                    "name: \"New American Restaurant\",\n" +
                    "pluralName: \"New American Restaurants\",\n" +
                    "shortName: \"New American\",\n" +
                    "icon: {\n" +
                    "prefix: \"https://ss3.4sqi.net/img/categories_v2/food/newamerican_\",\n" +
                    "suffix: \".png\"\n" +
                    "},\n" +
                    "primary: true\n" +
                    "}\n" +
                    "],\n" +
                    "verified: true,\n" +
                    "stats: {\n" +
                    "checkinsCount: 648,\n" +
                    "usersCount: 564,\n" +
                    "tipCount: 25\n" +
                    "},\n" +
                    "url: \"http://www.juninyc.com\",\n" +
                    "hasMenu: true,\n" +
                    "menu: {\n" +
                    "type: \"Menu\",\n" +
                    "label: \"Menu\",\n" +
                    "anchor: \"View Menu\",\n" +
                    "url: \"https://foursquare.com/v/juni/52147461abdfe6d3e56334b3/menu\",\n" +
                    "mobileUrl: \"https://foursquare.com/v/52147461abdfe6d3e56334b3/device_menu\"\n" +
                    "},\n" +
                    "allowMenuUrlEdit: true,\n" +
                    "specials: {\n" +
                    "count: 0,\n" +
                    "items: [ ]\n" +
                    "},\n" +
                    "venuePage: {\n" +
                    "id: \"83868713\"\n" +
                    "},\n" +
                    "storeId: \"\",\n" +
                    "hereNow: {\n" +
                    "count: 0,\n" +
                    "summary: \"Nobody here\",\n" +
                    "groups: [ ]\n" +
                    "},\n" +
                    "referralId: \"v-1465891837\",\n" +
                    "venueChains: [ ]\n" +
                    "},\n" +
                    "{\n" +
                    "id: \"560b19b1498ee5e226c151f9\",\n" +
                    "name: \"Her Name is Han\",\n" +
                    "contact: {\n" +
                    "phone: \"2127799990\",\n" +
                    "formattedPhone: \"(212) 779-9990\",\n" +
                    "facebook: \"527673954047451\",\n" +
                    "facebookUsername: \"hernameishan\",\n" +
                    "facebookName: \"Her Name is Han\"\n" +
                    "},\n" +
                    "location: {\n" +
                    "address: \"17 E 31st St\",\n" +
                    "crossStreet: \"btwn Madison & 5th Ave\",\n" +
                    "lat: 40.746082,\n" +
                    "lng: -73.984874,\n" +
                    "distance: 50,\n" +
                    "postalCode: \"10016\",\n" +
                    "cc: \"US\",\n" +
                    "city: \"New York\",\n" +
                    "state: \"NY\",\n" +
                    "country: \"United States\",\n" +
                    "formattedAddress: [\n" +
                    "\"17 E 31st St (btwn Madison & 5th Ave)\",\n" +
                    "\"New York, NY 10016\",\n" +
                    "\"United States\"\n" +
                    "]\n" +
                    "},\n" +
                    "categories: [\n" +
                    "{\n" +
                    "id: \"4bf58dd8d48988d113941735\",\n" +
                    "name: \"Korean Restaurant\",\n" +
                    "pluralName: \"Korean Restaurants\",\n" +
                    "shortName: \"Korean\",\n" +
                    "icon: {\n" +
                    "prefix: \"https://ss3.4sqi.net/img/categories_v2/food/korean_\",\n" +
                    "suffix: \".png\"\n" +
                    "},\n" +
                    "primary: true\n" +
                    "}\n" +
                    "],\n" +
                    "verified: false,\n" +
                    "stats: {\n" +
                    "checkinsCount: 2391,\n" +
                    "usersCount: 1753,\n" +
                    "tipCount: 70\n" +
                    "},\n" +
                    "url: \"http://www.hernameishan.com\",\n" +
                    "allowMenuUrlEdit: true,\n" +
                    "specials: {\n" +
                    "count: 0,\n" +
                    "items: [ ]\n" +
                    "},\n" +
                    "hereNow: {\n" +
                    "count: 0,\n" +
                    "summary: \"Nobody here\",\n" +
                    "groups: [ ]\n" +
                    "},\n" +
                    "referralId: \"v-1465891837\",\n" +
                    "venueChains: [ ]\n" +
                    "},\n" +
                    "{\n" +
                    "id: \"4e27510d18384dd0a0d4d668\",\n" +
                    "name: \"MTA Bus - 5 Av & 32 St (M4/Q32/X10/X17)\",\n" +
                    "contact: { },\n" +
                    "location: {\n" +
                    "address: \"5 Avenue A\",\n" +
                    "crossStreet: \"btwn E 33rd & E 32nd St\",\n" +
                    "lat: 40.74727222607656,\n" +
                    "lng: -73.98518666710845,\n" +
                    "distance: 98,\n" +
                    "postalCode: \"10009\",\n" +
                    "cc: \"US\",\n" +
                    "city: \"New York\",\n" +
                    "state: \"NY\",\n" +
                    "country: \"United States\",\n" +
                    "formattedAddress: [\n" +
                    "\"5 Avenue A (btwn E 33rd & E 32nd St)\",\n" +
                    "\"New York, NY 10009\",\n" +
                    "\"United States\"\n" +
                    "]\n" +
                    "},\n" +
                    "categories: [\n" +
                    "{\n" +
                    "id: \"52f2ab2ebcbc57f1066b8b4f\",\n" +
                    "name: \"Bus Stop\",\n" +
                    "pluralName: \"Bus Stops\",\n" +
                    "shortName: \"Bus Stop\",\n" +
                    "icon: {\n" +
                    "prefix: \"https://ss3.4sqi.net/img/categories_v2/travel/busstation_\",\n" +
                    "suffix: \".png\"\n" +
                    "},\n" +
                    "primary: true\n" +
                    "}\n" +
                    "],\n" +
                    "verified: false,\n" +
                    "stats: {\n" +
                    "checkinsCount: 468,\n" +
                    "usersCount: 76,\n" +
                    "tipCount: 2\n" +
                    "},\n" +
                    "specials: {\n" +
                    "count: 0,\n" +
                    "items: [ ]\n" +
                    "},\n" +
                    "hereNow: {\n" +
                    "count: 0,\n" +
                    "summary: \"Nobody here\",\n" +
                    "groups: [ ]\n" +
                    "},\n" +
                    "referralId: \"v-1465891837\",\n" +
                    "venueChains: [ ]\n" +
                    "}\n" +
                    "],\n" +
                    "confident: false\n" +
                    "}\n" +
                    "}";
            return "";
        }

        @Override
        protected void onPreExecute() {
            // we can start a progress bar here
        }

        @Override
        protected void onPostExecute(String result) {
            if (temp == null) { //had a GET error

                /* Handle error (stop progress bar, etc..) */

            } else { //no GET errors

                //Parse FourSquareJSON to extract a venueList:
                venuesList = parseFourSquareResponse(temp);

                List<String> listTitle = new ArrayList<>();

                for (int i = 0; i < venuesList.size(); i++) {

                    /*Make an ArrayList to hold title we're displaying*/
                    listTitle.add(i, venuesList.get(i).getName() + ", " + venuesList.get(i).getCategory() + ", in " + venuesList.get(i).getCity());
                    // TODO: szabi-2016-06-16 Update this so we display complete info in cards
                }

                myAdapter = new ArrayAdapter<>(FoursquareResults.this, R.layout.list_itinerary_list_item, R.id.listText, listTitle);
                setListAdapter(myAdapter);
            }
        }
    }

//    public static String makeCall(String url) {
//
//        // string buffers the url
//        StringBuffer buffer_string = new StringBuffer(url);
//        String replyString = "";
//
//        // instanciate an HttpClient
//        HttpURLConnection httpclient = new DefaultHttpClient();
//        // instanciate an HttpGet
//        HttpGet httpget = new HttpGet(buffer_string.toString());
//
//        try {
//            // get the responce of the httpclient execution of the url
//            HttpResponse response = httpclient.execute(httpget);
//            InputStream is = response.getEntity().getContent();
//
//            // buffer input stream the result
//            BufferedInputStream bis = new BufferedInputStream(is);
//            ByteArrayBuffer baf = new ByteArrayBuffer(20);
//            int current = 0;
//            while ((current = bis.read()) != -1) {
//                baf.append((byte) current);
//            }
//            // the result as a string is ready for parsing
//            replyString = new String(baf.toByteArray());
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//        // trim the whitespaces
//        return replyString.trim();
//    }

    private static ArrayList<FoursquareVenue> parseFourSquareResponse(final String response) {

        // TODO: 2016-06-16 Build a better parser to extract information we need.
        
        ArrayList<FoursquareVenue> temp = new ArrayList<>();
        try {
            JSONObject jsonObject = new JSONObject(response); // make an jsonObject in order to parse the response

            if (jsonObject.has("response")) {

                if (jsonObject.getJSONObject("response").has("venues")) {
                    JSONArray jsonVenueArray = jsonObject.getJSONObject("response").getJSONArray("venues"); //gets array of all venues in GET response

                    for (int i = 0; i < jsonVenueArray.length(); i++) {
                        FoursquareVenue poi = new FoursquareVenue(); //Create a new "FourSquareVenue" object; holds venue information:

                        if (jsonVenueArray.getJSONObject(i).has("name")) {

                            /*set venue name*/
                            poi.setName(jsonVenueArray.getJSONObject(i).getString("name"));

                            if (jsonVenueArray.getJSONObject(i).has("location")) {

                                if (jsonVenueArray.getJSONObject(i).getJSONObject("location").has("address")) {

                                    if (jsonVenueArray.getJSONObject(i).getJSONObject("location").has("city")) {

                                        /*set venue city*/
                                        poi.setCity(jsonVenueArray.getJSONObject(i).getJSONObject("location").getString("city"));
                                    }
                                    if (jsonVenueArray.getJSONObject(i).has("categories")) {
                                        if (jsonVenueArray.getJSONObject(i).getJSONArray("categories").length() > 0) {
                                            if (jsonVenueArray.getJSONObject(i).getJSONArray("categories").getJSONObject(0).has("icon")) {

                                                /*set category name*/
                                                poi.setCategory(jsonVenueArray.getJSONObject(i).getJSONArray("categories").getJSONObject(0).getString("name"));
                                            }
                                        }
                                    }
                                    temp.add(poi);
                                }
                            }
                        }
                    }
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
            return new ArrayList<>();
        }
        return temp;
    }
}