package first.routetrip.fragments;


import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ExpandableListView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import first.routetrip.helpers.FourSquareCategory;
import first.routetrip.R;

/**
 * A fragment that contains all necessary code to handle user category selection.
 * Upon continue-button click, will have to pass selected category (as a list) to
 * this fragment's listeners.
 *
 * Next fragment in trip-selection hierarchy after this should be "TripSummary"
 */

public class CategoryInput extends Fragment {

    private static final String DEBUG_TAG = CategoryInput.class.getSimpleName();
    private static final String FRAGMENT_TAG = "FragTag:";

    //Define Variables:
    private ExpandableListView categoryList;
    private handleCategoryData fragmentListener;
    private FourSquareCategoryAdapter expListAdapter; //one global adapter for this fragment

    public interface handleCategoryData {
        void categoryContinueButtonClicked(ArrayList<FourSquareCategory> selectedCategories);
    }

    public CategoryInput() {
        // Required empty public constructor
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d(DEBUG_TAG, "onDestroy");
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        /* NOTE: we know we're attaching this to activity, therefore following cast is safe.
              Otherwise, this line could cause exceptions.*/
        Activity activity = (Activity) context;

        /*We make sure listeners have successfully "identified"
            themselves before fragment functions. Otherwise, we throw an exception.*/
        try {
            fragmentListener = (handleCategoryData) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement handleCategoryData");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_category_input, container, false);

        /*
         * Contains an ExpandableListView to display interests (categories) to the user.
         * Top level displays Major categories: Food, Music, Sports, etc..
         * Second level displays specifics of selected top-level category.
         * (Thai food, mexican food, EDM, classical music, hockey, soccer, etc..)
         */
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        /* Get references to Views from ViewGroup here */

        //Get a reference to the continue button and set it's listener
        ImageButton continueButton = (ImageButton) view.findViewById(R.id.categoryContinueButton);
        continueButton.setOnClickListener(continueButtonListener);

        //Get a reference to the clear button and set it's listener
        ImageButton clearButton = (ImageButton) view.findViewById(R.id.categoryClearButton);
        clearButton.setOnClickListener(clearButtonListener);

        //If fragment is created for the first time, create new ExpandableListAdapter
        if (categoryList == null) {
            expListAdapter = new FourSquareCategoryAdapter(getContext());
            expListAdapter.populateFourSquareMainCategories();
        }//otherwise we already have a created category list; re-attach adapter

        //Get a reference to the expandable list view & set its adapter
        categoryList = (ExpandableListView) view.findViewById(R.id.expandableCategoryList);
        categoryList.setAdapter(expListAdapter);

    }

    //========================================LISTENERS=============================================

    View.OnClickListener continueButtonListener = new View.OnClickListener() {
        /*
         * Handle clicks for "continue" button:
         * Populate a listArray with selected categories and pass it up to listener(s)
         */
        @Override
        public void onClick(View v) {

            /* get reference to hash-map storing child checked states */
            HashMap<Integer, boolean[]> childCheckedStatesCopy =
                    ((FourSquareCategoryAdapter) categoryList.getExpandableListAdapter())
                            .getChildCheckedStates();

            /* get a reference to our childCategories from the adapter */
            ArrayList<ArrayList<FourSquareCategory>> childCategories =
                    ((FourSquareCategoryAdapter) categoryList.getExpandableListAdapter())
                            .getChildCategories();

            /* create new arrayList to store only selected child categories */
            ArrayList<FourSquareCategory> selectedCategories = new ArrayList<>();

            /* loop through childCheckedStates hash map and store selected categories in ArrayList*/
            for (Object o : childCheckedStatesCopy.entrySet()) {
                /* each hash table entry; .next() returns
                    set element object, need to cast to HashMap Entry! */
                HashMap.Entry mapEntry = (HashMap.Entry) o;

                int mainCategoryIndex = (Integer) mapEntry.getKey(); //at this "groupPosition"
                boolean[] childStates = (boolean[]) mapEntry.getValue(); //get child bool array
                for (int childCategoryIndex = 1;
                     childCategoryIndex < childStates.length;
                     childCategoryIndex++) { //at "childPosition"

                    /*
                    TODO: If needed, change how we pass-up "all" selected child categories here
                        NOTE: If "All" option is chosen for a childCategory;
                        We chose to display all children of that mainCategory
                        This is because if not child category is selected, we choose the
                        main category itself and use any results from there.
                        However, if the user explicitly chose "all"-available subcategories, then
                        they are only interested in those.
                        (starting index = 1 ignores "all" option)

                        ex: if main category is food and NO main categories are
                                chosen, we may use any as filters for venues.
                            however, if we supply options "italian, chinese, greek" and user
                                chooses "all", then we only use those three as filters.
                     */

                    if (childStates[childCategoryIndex]) { //if the child state at index is checked
                        /*get the child FourSquareCategory:
                        using the two indices: mainCategoryIndex & index*/
                        selectedCategories.add(childCategories.get(mainCategoryIndex)
                                .get(childCategoryIndex));
                    }

                }
            }

             /* if no categories chosen, assume all main categories are of interest
                 (without specifics offered by child categories) */
            if (selectedCategories.size() == 0) {
                selectedCategories =
                        ((FourSquareCategoryAdapter) categoryList.getExpandableListAdapter())
                                .getMainCategories();
            }

            /* pass category list up to listener(s) */
            fragmentListener.categoryContinueButtonClicked(selectedCategories);
        }
    };

    View.OnClickListener clearButtonListener = new View.OnClickListener() {
        /*
         * Handle clicks for "clear categories" button:
         * Clear hash-map storing checked child states
         * and reset all childCategory check boxes to false
         */
        @Override
        public void onClick(View v) {
            ((FourSquareCategoryAdapter) categoryList.getExpandableListAdapter())
                    .resetSelectedChildCategories();
        }
    };

    /* NOTE: Sub-category selection in expandable list view via checkBoxes is handled inside the adapter inner-class's getChildView */

    //========================================INNER CLASSES=========================================

    public class FourSquareCategoryAdapter extends BaseExpandableListAdapter {

        private final class MainCategoryViewHolder {
            /* final viewHolder for main categories */

            private ImageView mainIcon;
            private TextView mainDescription;
        }

        private final class ChildCategoryViewHolder {
            /* final viewHolder for child categories */

            private ImageView childIcon;
            private TextView childDescription;
            private CheckBox childCategorySelection;
        }

        /*
        TODO: Important: change method of storing / accessing data here - probably from a database, see below: (TreeAdapters)
        http://stackoverflow.com/questions/10644914/android-expandablelistview-and-sqlite-database
         */

        /*
         * NOTE: Child check-box behaviour fix from: http://stackoverflow.com/questions/21494042/checkbox-states-in-expandablelistviews-children
         */

        private ArrayList<FourSquareCategory> mainCategories;
        private ArrayList<ArrayList<FourSquareCategory>> childCategories; //todo: hash-maps or sparse array here if have time  are needed
        private MainCategoryViewHolder mainCategoryViewHolder;
        private ChildCategoryViewHolder childCategoryViewHolder;
        private HashMap<Integer, boolean[]> childCheckedStates; //hash map to track child check-box states accross all childViews
        private HashMap<String, View> childViewReferences; //keep track of child views for "All" selection functionality: alter checkbox at ALL other child Views
        private LayoutInflater inflater;

        public FourSquareCategoryAdapter(Context context) { //adapter constructor
            childCheckedStates = new HashMap<>();
            childViewReferences = new HashMap<>();
            this.inflater = LayoutInflater.from(context);//cache the inflater to avoid asking for it each time
        }

        public void populateFourSquareMainCategories() {

            mainCategories = new ArrayList<>();

            mainCategories.add(new FourSquareCategory("Beach", R.drawable.ic_action_beach_96));
            mainCategories.add(new FourSquareCategory("Biking", R.drawable.ic_action_regular_biking_96));
            mainCategories.add(new FourSquareCategory("Camping", R.drawable.ic_action_camping_tent_96));
            mainCategories.add(new FourSquareCategory("Concerts", R.drawable.ic_action_musical_notes_104));
            mainCategories.add(new FourSquareCategory("Fishing", R.drawable.ic_action_fishing_96));
            mainCategories.add(new FourSquareCategory("NightClubs", R.drawable.ic_action_dancing_96));
            mainCategories.add(new FourSquareCategory("RockClimbing", R.drawable.ic_action_climbing_96));
            mainCategories.add(new FourSquareCategory("Skiing", R.drawable.ic_action_skiing_96));
            mainCategories.add(new FourSquareCategory("Shopping", R.drawable.ic_action_shopping_bag_96));

            populateFourSquareChildCategories(); //now populate children as well
            notifyDataSetChanged();
        }

        public void populateFourSquareChildCategories() {

            childCategories = new ArrayList<>(); //2D array list: to hold sub-categories for each existing top-level category

            for (FourSquareCategory topLevelCategory : mainCategories) { //for each top-level category, create an array list to hold sub-categories
                childCategories.add(new ArrayList<FourSquareCategory>());

                //now populate sub categories: ( this is hardcoded, fix later on once we have databases and / or the time)

                //two sub items per category (yes, the add command below is messy - temp for now)
                for (int subCategoryIndex = 0; subCategoryIndex < 3; subCategoryIndex++) {

                    /* first option in all childCategories = "Select All" */
                    if (subCategoryIndex == 0) {
                        childCategories.get(mainCategories.indexOf(topLevelCategory))
                                .add(new FourSquareCategory("Select All", R.drawable.ic_action_select_all_96));
                    } else {
                        childCategories.get(mainCategories.indexOf(topLevelCategory))
                                .add(new FourSquareCategory((mainCategories.get(mainCategories
                                        .indexOf(topLevelCategory))
                                        .getCategoryName() + " " + subCategoryIndex),
                                        R.drawable.ic_action_maintenance_104)); //temporary "under construction" icons
                    }
                }
            }

        }

        public ArrayList<FourSquareCategory> getMainCategories() {
            return mainCategories;
        }

        public ArrayList<ArrayList<FourSquareCategory>> getChildCategories() {
            return childCategories;
        }

        public HashMap<Integer, boolean[]> getChildCheckedStates() {
            return childCheckedStates;
        }

        public void resetSelectedChildCategories() {

            /*
             * 1. clear hash map that keeps track of selected childCategories
             * 2. reset all checkBoxes to be unchecked
             * 3. re-enable any if disabled
             */

            //1:
            childCheckedStates.clear();

            //2:
            notifyDataSetChanged(); //destroys all recycled views, therefore resetting checkboxes
            //todo: this might be inefficient, alternatives?

            //3: if any are disabled from "all" option; enable them again
            for (Object o : childViewReferences.entrySet()) {
                HashMap.Entry entry = (HashMap.Entry) o;

                View childView = (View) entry.getValue();
                ChildCategoryViewHolder childViewHolder =
                        (ChildCategoryViewHolder) childView.getTag();
                if(!childViewHolder.childCategorySelection.isEnabled()){
                    childViewHolder.childCategorySelection.setEnabled(true);
                }
            }

        }

        @Override
        public int getGroupCount() {
            /* Returns the number of main-categories. ("number of groups") */
            return mainCategories.size();
        }

        @Override
        public int getChildrenCount(int groupPosition) {
            /* Returns number of children (number of subcategories) for specified main category */
            return childCategories.get(groupPosition).size();
        }

        @Override
        public ArrayList<FourSquareCategory> getGroup(int groupPosition) {
            /* Returns all children (subcategories) associated with the specified main category */
            return childCategories.get(groupPosition);
        }

        @Override
        public FourSquareCategory getChild(int groupPosition, int childPosition) {
            /* Returns the subcategory within the specified position of the selected main category */
            return childCategories.get(groupPosition).get(childPosition);
        }

        @Override
        public long getGroupId(int groupPosition) {
            return groupPosition;
        }

        @Override
        public long getChildId(int groupPosition, int childPosition) {
            return childPosition;
        }

        @Override
        public boolean hasStableIds() {
            return true;
        }

        @Override
        public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
            /* Return view for main category row */

            View view = convertView; //recycle views!

            if (view == null) { //we have no views to recycle, create new one:

                /* inflate with given parent, but don't attach it. */
                view = inflater.inflate(R.layout.explist_foursquare_category_main_category_row, parent, false);
                //alternate way to get inflater: CategoryInput.this.getActivity().getLayoutInflater()

                /* initialize parent viewHolder & save views */
                mainCategoryViewHolder = new MainCategoryViewHolder();
                mainCategoryViewHolder.mainDescription =
                        (TextView) view.findViewById(R.id.foursquare_maincategory_description);
                mainCategoryViewHolder.mainIcon =
                        (ImageView) view.findViewById(R.id.foursquare_maincategory_icon);

                /* save viewHolder into convertView's memory for future access */
                view.setTag(mainCategoryViewHolder);

            } else { //recycling views; use mainCategoryViewHolder:

                /* load the viewHolder that must exist at this point */
                mainCategoryViewHolder = (MainCategoryViewHolder) view.getTag();

            }

            // now access views from viewHolder only

            mainCategoryViewHolder.mainIcon
                    .setImageResource(mainCategories.get(groupPosition).getCategoryImgID());

            mainCategoryViewHolder.mainDescription
                    .setText(mainCategories.get(groupPosition).getCategoryName());

            return view;

        }

        @Override
        public View getChildView(final int groupPosition, final int childPosition, boolean isLastChild, View convertView, final ViewGroup parent) {
            /* Return view for child category row & account for previous selections */

            /*
             * IMPORTANT: This assumes all child rows use the same layout.
             * To use multiple and differentiate we need to implement getChildType()
             * see: http://stackoverflow.com/questions/21169294/how-to-handle-various-types-of-rows-in-an-expandablelistview
             */

            View view;

            /* if we have an entry in childViewReferences at this
                    groupPosition & childPosition, retrieve and use it: */
            if ((childViewReferences.containsKey(Integer.toString(groupPosition)
                    + Integer.toString(childPosition)))) {

                view = childViewReferences.get(Integer.toString(groupPosition)
                        + Integer.toString(childPosition));

                /* load the viewHolder that must exist at this point */
                childCategoryViewHolder = (ChildCategoryViewHolder) view.getTag();

            } else {
                /* if we don't have a saved view at this
                    groupPosition & childPosition, create new one and save it: */

                /* inflate with given parent, but don't attach it. */
                view = inflater.inflate(R.layout.explist_foursquare_category_child_category_row, parent, false);
                //alternate way to get inflater: CategoryInput.this.getActivity().getLayoutInflater()

                /* initialize child viewHolder & save views */
                childCategoryViewHolder = new ChildCategoryViewHolder();
                childCategoryViewHolder.childDescription =
                        (TextView) view.findViewById(R.id.foursquare_childcategory_description);
                childCategoryViewHolder.childIcon =
                        (ImageView) view.findViewById(R.id.foursquare_childcategory_icon);
                childCategoryViewHolder.childCategorySelection =
                        (CheckBox) view.findViewById(R.id.foursquare_childcategory_checkBox);

                /* save viewHolder into convertView's memory for future access */
                view.setTag(childCategoryViewHolder);

                /* save created view for this groupPosition and childPosition
                     into a hashMap for easy access with the "all" functionality manipulation
                     NOTE: This keeps checkbox state independent from recycled views. */
                childViewReferences.put(Integer.toString(groupPosition)
                        + Integer.toString(childPosition), view);
            }

            // now we have a View object, access views from its viewHolder:
            childCategoryViewHolder.childIcon
                    .setImageResource(childCategories.get(groupPosition).get(childPosition)
                            .getCategoryImgID());
            childCategoryViewHolder.childDescription
                    .setText(childCategories.get(groupPosition).get(childPosition)
                            .getCategoryName());

            /*
             * Now restore checkbox states (if any), but we first have to set checkedChangeListener
             * to null, because each time we explicitly call "setChecked()",
             * the onCheckedChangeListener kicks in (we don't want this just yet)
             */

            childCategoryViewHolder.childCategorySelection.setOnCheckedChangeListener(null);

            /* if we previously set checkbox states at this groupPosition */
            if (childCheckedStates.containsKey(groupPosition)) {

                /* retrieve childCategories checked-states */
                boolean[] checkedStates = childCheckedStates.get(groupPosition);
                childCategoryViewHolder.childCategorySelection
                        .setChecked(checkedStates[childPosition]); //and restore childCategory checked-state (at childPosition)

            } else {

                /* otherwise we create boolean array for all childCategories
                    at this groupPosition to save checked states */
                boolean[] checkedStates = new boolean[getChildrenCount(groupPosition)];
                childCheckedStates.put(groupPosition, checkedStates);
                //NOTE: default value of new boolean(primitive) array is zero (false)

                /* since we haven't saved state at this groupPosition yet, we must not have
                    opened the childView before, so initialize all
                    childView checked-sates to default (false) */
                childCategoryViewHolder.childCategorySelection
                        .setChecked(false);
            }

            /* now set actual listener to handle checkbox changes */
            childCategoryViewHolder.childCategorySelection
                    .setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                        @Override
                        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                            /* get boolean array for category at groupPosition */
                            boolean checkedStates[] = childCheckedStates.get(groupPosition);

                            /* if we check the "All" option (at childPosition = 0):
                                    - set all other childView's checked state at that groupPosition
                                        to checked, and register their state as checked.
                                    - disable them while "all" option is checked. */
                            if (childPosition == 0) {

                                /* for all childViews at groupPosition */
                                for (int childIndex = 0;
                                     childIndex < checkedStates.length;
                                     childIndex++) {

                                    checkedStates[childIndex] = isChecked;

                                    if (childIndex != 0) {

                                        /* for all other childViews aside from the "all" option */
                                        View nextChildView = childViewReferences
                                                .get(Integer.toString(groupPosition)
                                                        + Integer.toString(childIndex));
                                        ChildCategoryViewHolder nextChildViewHolder
                                                = (ChildCategoryViewHolder) nextChildView.getTag();
                                        nextChildViewHolder.childCategorySelection
                                                .setChecked(isChecked);

                                        /* Disable other childCategories if chosen "all" = true,
                                            enable otherwise */
                                        nextChildViewHolder.childCategorySelection
                                                .setEnabled(!isChecked);
                                    }
                                }

                            } else {

                                /* if user didn't cloose "all", update the checked-state
                                    (in the stored boolean array) of the selected childCategory
                                     at groupPosition. */
                                checkedStates[childPosition] = isChecked;
                            }

                            /* now save the updated boolean array at the selected mainCategory */
                            childCheckedStates.put(groupPosition, checkedStates);
                        }
                    });

            return view;
        }

        @Override
        public boolean isChildSelectable(int groupPosition, int childPosition) {
            /* all child categories should be selectable, can change that here using indices from parameters */
            return true; //note: setting to false removes dividers between all children
        }


    }


}
