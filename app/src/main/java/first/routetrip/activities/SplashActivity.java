package first.routetrip.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class SplashActivity extends AppCompatActivity {

    /*
     * Splash Screen: Launcher Activity with a unique style attribute to display the splash screen.
     * Styles.xml has a separate style for this that adds a custom drawable.xml made up of
     *  a background image (splash image) - this style is set in the manifest.
     *
     *  Splash screen forwards us to our MainActivity after 2 seconds
     */

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Intent fwd = new Intent(SplashActivity.this, MainActivity.class);
        startActivity(fwd);
        finish(); //explicitly end this activity

    }
}
