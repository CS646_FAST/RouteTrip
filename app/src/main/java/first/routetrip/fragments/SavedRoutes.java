package first.routetrip.fragments;


import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Text;

import java.util.ArrayList;

import first.routetrip.R;
import first.routetrip.activities.MapsActivity;
import first.routetrip.helpers.Persistence;
import first.routetrip.helpers.RetrieveRoute;
import first.routetrip.helpers.sql.TripDatabase;
import first.routetrip.helpers.trip.Route;

/**
 * A Fragment that displays a RecyclerView populate with cursor data from an SQLiteDatabase.
 * Cursor returns ALL previously saved trips by the user and displayed in a cardView similar
 * to the ItineraryList layout, however instead of a "save" button, we now have a "delete" button.
 */

//todo: recyclerView-List fragment; adapter uses new cardView without "save" button; instead has a "DELETE" button; data comes from databased of saved trips (not selected trip)
//todo: have to extract saved trip origin (at 0th leg start location) and destination (at last leg's destination location) and display it along with route #
//todo: if nothing saved, display an empty page ; see MaterialDesign
//todo: add a "delete all" button to the top before the RecyclerView
//todo: Alert dialog when deleting LAST entry in savedRoutes (it must be deleting current entry too); also make sure you check if the route that's being deleted is the current route (I think there's a special SQL command to delete last row of a table..)
//todo: async task all Database requests

public class SavedRoutes extends Fragment {

    private static final String DEBUG_TAG = SavedRoutes.class.getSimpleName();
    private static final String FRAGMENT_TAG = "FragTag:";
    public static final String ROUTE_FILENAME = "routeFile";

    private RecyclerView mRecyclerView;
    private TripDatabase mDbHelper;
    private ArrayList<Route> mSavedRoutes;
    private ArrayList<String> mSavedRouteFileNames;

    public SavedRoutes() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_saved_routes, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        /* Get a reference to our SQLiteDatabaseHelper */
        mDbHelper = new TripDatabase(getContext());

        /* Get references to RecyclerView */
        mRecyclerView = (RecyclerView) view
                .findViewById(R.id.savedRecyclerView);

        /* Explicitly indicate Recycler-Adapter won't change recyclerView size;
            -> improves performance */
        //todo: see if this works after delete functionality is working
//        mRecyclerView.setHasFixedSize(true);

        /* Create a LayoutManager (Linear) and assign it to the RecyclerView */
        RecyclerView.LayoutManager linearLayoutMngr = new LinearLayoutManager(getContext());
        mRecyclerView.setLayoutManager(linearLayoutMngr);

        /* Set a default item animator to the RecyclerView */
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());

        new LoadSavedRoutesTask().execute();

    }

    //todo: pressing "Start" will override currently selected trip
    //todo: remember; need to display: "Route 1 - To <destination> in the placeholder TextView

    private boolean getAllSavedRoutes() {
        /* Populated adapter's data with all previously saved routes on device's internal storage */

        /* First, get a cursor of all SavedRoutes FILENAMES. This will never return null because
            MainActivity already before starting this fragment. */
        Cursor cursor = mDbHelper.getAllSavedRoutes();

        /* If we get an error; crash app. See log.e output from TripDatabase to fix */
        assert cursor != null;

        if (cursor.getCount() < 1) {
            /* We should already have results here due to check in MainActivity; crash if this happens and fix it */
            cursor.close();
            throw new AssertionError(DEBUG_TAG + "Error: have no saved routes; somehow bypassed check in case R.id.nav_savedTrips of MainActivity");
        }

        /* Loop through cursor containing SavedRoute FILENAMES,
            and retrieve corresponding route objects at each row*/
        this.mSavedRoutes = new ArrayList<>();
        this.mSavedRouteFileNames = new ArrayList<>();

        do {

            /* cursor returned already moved to first position,
                so handle that before looping */
            String FILENAME = cursor.getString(1); //first column = fileName; 0th column = _id

            /* Create a RetrieveRoute helper and invoke method to get RouteObject: */
            RetrieveRoute retrieveRoute = new RetrieveRoute(getContext());
            Route savedRoute = retrieveRoute.getSavedRoute(FILENAME);
            this.mSavedRoutes.add(savedRoute);
            this.mSavedRouteFileNames.add(FILENAME);

        } while (cursor.moveToNext());

        int numStoredFileNames = cursor.getCount();
        cursor.close();

        /* true only if we successfully retrieved a route object for each stored FILENAME */
        return mSavedRoutes.size() == numStoredFileNames;
    }

    private class RecyclerViewSavedTripDisplayAdapter extends
            RecyclerView.Adapter<RecyclerViewSavedTripDisplayAdapter.SavedItineraryViewHolder> {

        private ArrayList<Route> tripData;
        private ArrayList<String> tripLabels;
        private LayoutInflater layoutInflater;

        public RecyclerViewSavedTripDisplayAdapter(Context context,
                                                   ArrayList<Route> savedRoutes,
                                                   ArrayList<String> routeFileNames) {
            /* RecyclerView-Adapter constructor takes
                in context to cache a LayoutInflater so we don't ask for it multiple times */
            this.tripData = savedRoutes;
            this.tripLabels = routeFileNames;
            this.layoutInflater = LayoutInflater.from(context);
        }

        public final class SavedItineraryViewHolder extends RecyclerView.ViewHolder {
            /* RecyclerView ViewHolder inner-class */

            //todo: finish declaring views to save
            private TextView routePositionTextDisplay;
            private TextView routeDestinationTextDisplay;
            private Button deleteSavedTripButton;
            private Button startSavedTripButton;
            private Button moreInfoItineraryButton;

            public SavedItineraryViewHolder(View itemView) {
                /* RecyclerView-ViewHolder constructor takes in
                    an inflated-itemView that contains views to cache
                     (view for each row of the RecyclerView) */
                super(itemView);

                /* NOTE: Always have access to layout's RootView
                    in ANY RecyclerView.ViewHolder via the "itemView"
                     parameter; no need to explicitly store */

                //todo: finish saving views after they have been declared

//                cardView = (CardView) itemView //this might have to go, depending on rootView of card.
//                        .findViewById(R.id.itineraryCardView);

                //The CardView view is the itemView, see "NOTE" above.

                routePositionTextDisplay = (TextView) itemView
                        .findViewById(R.id.savedRoutePositionText);

                routeDestinationTextDisplay = (TextView) itemView
                        .findViewById(R.id.savedRouteDestinationText);

                deleteSavedTripButton = (Button) itemView
                        .findViewById(R.id.savedRouteDeleteButton);

                startSavedTripButton = (Button) itemView
                        .findViewById(R.id.savedRouteStartButton);

                moreInfoItineraryButton = (Button) itemView
                        .findViewById(R.id.saveRouteMoreInfoButton);
            }
        }

        @Override
        public SavedItineraryViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            /* Inflates a view from layout that will
                be used for each RecyclerView-row, and creates
                 (+ returns) a new RecyclerView-ViewHolder for
                  the inflated view */

            View itemView = layoutInflater
                    .inflate(R.layout.card_saved_itineraries, parent, false);

            return new SavedItineraryViewHolder(itemView);
        }

        @Override
        public void onBindViewHolder(SavedItineraryViewHolder holder, final int position) {
            /* This method is used to populate ViewHolder-views with
                appropriate data. Think of the "getView" method
                 from simple adapters. */

            //TODO: Update this whole method after it has been updated in "ItineraryList.java"

            StringBuilder routePositionText = new StringBuilder();
            routePositionText
                    .append(this.tripLabels.get(position).replaceAll("\\(.*\\)", "")) //remove data in parentheses: "Route 1(94534534231SAVE)" -> "Route 1"
                    .append(" - To: ");
            StringBuilder routeDestinationText = new StringBuilder();
            routeDestinationText
                    .append(this.tripData.get(position).getRouteLegs() //get the destination address of the last leg from the trip
                            .get(this.tripData.get(position).getRouteLegs().size() - 1) //last leg
                            .getLegDstLoc().getLocAddress());

            holder.routePositionTextDisplay.setText(routePositionText);
            holder.routeDestinationTextDisplay.setText(routeDestinationText);

            final String FILENAME = this.tripLabels.get(position);


//            //temp copy of ItineraryList.java - fragment adapter method for simplicity todo: refactor similar code into helpers - DO NOT HAVE SAME CODE FOR SAME PURPOSE IN TWO PLACES
            final Route tripRoute = tripData.get(position);
//            //TODO: Update uniqueString name with UUID; see ItineraryList.java fragment same method; copy changes here
//            final String routeStringName = "Route " + Integer.toString(position + 1);
//            final String routeDestinationHash = Integer.toString(("Destination = " + tripRoute.getRouteLegs()
//                    .get(tripRoute.getRouteLegs().size() - 1).getLegDstLoc().getLocAddress()).hashCode()); //destination address at last leg of trip
//            final String FILENAMEwithHASH = routeStringName + "(" + routeDestinationHash; //need to append "SAVE)" or "CURRENT)" before using for now.. (we seriously need to refactor this once I get the time)

            holder.deleteSavedTripButton.setOnClickListener(new View.OnClickListener() {
                /* User deletes the route at index "position"; remove it from internal storage,
                    and delete it's FILENAME from SavedTrips database, also FILENAME from
                     tripLabels array; update RecyclerView */

                @Override
                public void onClick(View v) {
                    /* Display an alert dialog to get confirmation */

                    //todo: FUNCTIONALITY BREAKING BUG: if we delete the "current trip", it deletes the serialized route, and current route file-handle still stays, can't read it afterwards.
                    //todo: need unique identifiers for routes using "route position, origin, and destination"


                    final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getContext());
                    alertDialogBuilder.setMessage("Are you sure?");
                    alertDialogBuilder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            /* continue with deletion */

                            final String storedInCurrentTable = mDbHelper.checkCurrentTableFileName(); //todo: async task ALL database handles; after this functionality has been refactored
                            if (storedInCurrentTable.compareTo(FILENAME) == 0) {
                                /* We're deleting current trip,
                                    give another alert dialog for confirmation */
                                AlertDialog.Builder alertDialogBuilderTwo =
                                        new AlertDialog.Builder(getContext());
                                alertDialogBuilderTwo.setMessage("This is the current route. \n\nContinue?");
                                alertDialogBuilderTwo.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        /* delete serialized route from internal storage, and FILENAME
                                            from BOTH tables. */

                                        if (deleteRoute(FILENAME) && resetCurrentRouteFILENAME()) {
                                           /* successfully deleted from internal storage + BOTH databases
                                               now we can remove it from adapter's ArrayList as well*/
                                            tripData.remove(position);
                                            tripLabels.remove(position);

                                            /* update adapter */
                                            notifyDataSetChanged();

                                            /* notify user of successful delete */
                                            Toast.makeText(getContext(), "Deleted", Toast.LENGTH_SHORT).show();
                                        } else {
                                            /* failed to delete, throw error and crash app so we can fix */
                                            throw new AssertionError(DEBUG_TAG + "ERROR: Failed to delete route! Need to debug");
                                        }


                                    }
                                });
                                alertDialogBuilderTwo.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        /* user canceled, do  nothing */
                                    }
                                });
                                alertDialogBuilderTwo.show();
                            } else {
                                //todo: refactor this, see most of it is a copy from yes button above (have extra line to remove CurrentTrip FILENAME)

                                if (deleteRoute(FILENAME)) {
                                /* successfully deleted from internal storage + database
                                    now we can remove it from adapter's ArrayList as well*/
                                    tripData.remove(position);
                                    tripLabels.remove(position);

                                /* update adapter */
                                    notifyDataSetChanged();

                                /* notify user of successful delete */
                                    Toast.makeText(getContext(), "Deleted", Toast.LENGTH_SHORT).show();
                                } else {
                                /* failed to delete, throw error and crash app so we can fix */
                                    throw new AssertionError(DEBUG_TAG + "ERROR: Failed to delete route! Need to debug");
                                }

                            }
                        }
                    });
                    alertDialogBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            /* user canceled, don't do anything */
                        }
                    });
                    alertDialogBuilder.show();

                }
            });

            holder.startSavedTripButton.setOnClickListener(new View.OnClickListener() {
                /* See startItineraryButton.setOnClickListener inside ItineraryList.java; same functionality */
                //todo: see frame and async issue on ItineraryList.java; implement fix here as well

                @Override
                public void onClick(View v) {
                    /* check if there is already a current trip */

                    final String storedInCurrentTable = mDbHelper.checkCurrentTableFileName(); //todo: async task ALL database handles; after this functionality has been refactored
                    if (storedInCurrentTable.compareTo(FILENAME) == 0) {
                        /* FILENAME already stored; above comparison == 0 means
                            strings are lexicographically equal */
                        Toast.makeText(getContext(), "Already on this route!", Toast.LENGTH_SHORT).show();
                        //todo: user should go to navItem "continueRoute" which basically starts mapsActivity for now with the CurrentRoute... todo: update properly based on whatever function we want
                    } else {

                        /* if we're starting a new trip, ask for confirmation */
                        final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getContext());
                        alertDialogBuilder.setMessage("This will replace any current trip. \n\nContinue?");
                        alertDialogBuilder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                /* continue starting trip */

                                /* Selecting a new CurrentRoute; first delete old one from
                                    the device's internal storage (handle: storedInCurrentTable) */

//                                String[] storedFiles = getContext().fileList(); //tmp; see stored strings:
                                boolean deleted = getContext().deleteFile(storedInCurrentTable); //todo: handle return boolean value if needed
//                                storedFiles = getContext().fileList(); //tmp; see stored strings:

                                /* Save selected Route into "CurrentTrip" Database Table. */
                                if (setCurrentRoute(tripRoute, FILENAME)) {
                                /* Once we persisted the current Route;
                                    start an intent with the FILENAMEwithHASH string to MapsActivity */

                                    //tmp: clear fragment backstack todo: once this is refactored into helper from MainActivity, simply call that helper method
                                    FragmentManager manager = getActivity().getSupportFragmentManager();
                                    if (manager.getBackStackEntryCount() > 0) {
                                        FragmentManager.BackStackEntry first = manager.getBackStackEntryAt(0);
                                        manager.popBackStack(first.getId(), FragmentManager.POP_BACK_STACK_INCLUSIVE);
                                    }

                                    Intent intent = new Intent(getActivity(), MapsActivity.class);
                                    intent.putExtra(ROUTE_FILENAME, FILENAME);
                                    getActivity().startActivity(intent);
                                } else {
                                    /* We failed to persist selectedRoute; give error and crash app */
                                    throw new AssertionError(DEBUG_TAG + "ERROR: Failed to setCurrentRoute; check log.e for point of failure");
                                }

                            }
                        });
                        alertDialogBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                /* user canceled, don't do anything */
                            }
                        });
                        alertDialogBuilder.show();
                    }

                }

            });

            holder.moreInfoItineraryButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //todo: same as "moreInfo" on itineraryList once it's complete
                    Toast.makeText(getContext(), "moreInfo-todo", Toast.LENGTH_SHORT).show();
                }
            });

        }


        @Override
        public int getItemCount() {
            /* Simply returns the number of data items (ie: number of routes for this trip) */
            return tripData.size();
        }

        //todo: this will be refactored; see "ItineraryList.java" code
        private boolean setCurrentRoute(Route tripRoute, String FILENAME) {
            /* When the user selects a route to start it; we persist it on the device's
                internal storage. We save the FileName accessor for later usage. */

            /* Get a PersistenceHelper object */
            Persistence persistenceHelper = new Persistence(getContext());

            /* First serialize the Route object */
            byte[] serializedTripRoute = persistenceHelper.serialize(tripRoute);
            assert serializedTripRoute != null;

            /* Now save it to the device's internal storage */
            if (!persistenceHelper.storeToInternalStorage(serializedTripRoute, FILENAME)) {
                return false;
            }

            /* Store file-name into database to keep track of currentRoute
                within device's internal storage */
            return persistenceHelper.storeCurrentRouteToDB(FILENAME);
        }

        private boolean deleteRoute(String FILENAME) {

            /* Delete FILENAME from SavedRoutes table in database */
            if (mDbHelper.deleteSavedRoute(FILENAME)) {

                /* Delete from internal storage */
                String[] storedFiles = getContext().fileList(); //tmp; see stored strings:
                boolean deleted = getContext().deleteFile(FILENAME);
                storedFiles = getContext().fileList(); //tmp; see stored strings:
                return true;

            } else {
                return false;
            }
        }

        private boolean resetCurrentRouteFILENAME() {
            /* Resets the FileHandle stored within the CurrentRoute table back to "" */
            return mDbHelper.resetCurrentRoute();
        }

    }


    private class LoadSavedRoutesTask extends AsyncTask<Void, Void, Boolean> {
        /* AsyncTask to retrieve all saved Routes from database to display */

        private ProgressDialog mProgressDialog;

        protected void onPreExecute() {
            /* Code to run before executing the task (This is in main thread) */
            mProgressDialog = new ProgressDialog(getContext());
            mProgressDialog.setMessage("Loading...");
            mProgressDialog.setCancelable(false); //prevent user from closing it
            mProgressDialog.show();
        }

        protected Boolean doInBackground(Void... params) {
            /* Code to run in a worker thread */

            /* populate saved Routes and their fileNames */
            return getAllSavedRoutes();
        }

        protected void onProgressUpdate(Void... values) {
            /* can show progress bar for route generation here */
        }

        protected void onPostExecute(Boolean result) {
            /* Code to run after the task is complete; in main thread */

            mProgressDialog.dismiss();

            if (!result) {
                /* failed to get all saved routes; crash app and fix */
                throw new AssertionError(DEBUG_TAG + "Failed to retrieve all Route objects for adapter, fix!");
            } else {
                assert mSavedRoutes != null;
                assert mSavedRouteFileNames != null;

                /* Create recyclerView adapter with trip info and attach it to the recyclerView */
                RecyclerViewSavedTripDisplayAdapter adapter = new RecyclerViewSavedTripDisplayAdapter(getContext(), mSavedRoutes, mSavedRouteFileNames);
                mRecyclerView.setAdapter(adapter);
            }

        }

    }


}
