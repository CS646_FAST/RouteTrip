package first.routetrip.helpers;

import android.content.Context;
import android.os.AsyncTask;
import android.support.annotation.Nullable;
import android.util.Log;
import com.google.android.gms.maps.model.LatLng;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;
import first.routetrip.R;
import first.routetrip.helpers.trip.Distance;
import first.routetrip.helpers.trip.Leg;
import first.routetrip.helpers.trip.PlaceLocation;
import first.routetrip.helpers.trip.Route;
import first.routetrip.helpers.trip.Step;

/**
 * A helper class to generate a trip between source and destination.
 * - Generates HTTP GET to the GoogleMapsDirections API.
 * - Parses received JSON response.
 * TODO: make sure we handle all cases when we encounter an error; a non-null trip only returns when everything went okay; otherwise gracefully handle error(s)
 * TODO:
 *
 * Learning credits go to: (references?)
 * https://www.youtube.com/watch?v=CCZPUeY94MU
 * https://github.com/hiepxuan2008/GoogleMapDirectionSimple/blob/master/app/src/main/java/com/itshareplus/googlemapdemo/MapsActivity.java
 */

public class TripGenerator {

    private static final String DEBUG_TAG = "TripGenerator";
    private final static String DIRECTIONS_API_JSON_REQUEST
            = "https://maps.googleapis.com/maps/api/directions/json";

    private String mSource;
    private String mDestination;
    private Context mContext; //Need to access string resources via getResources()
    private ArrayList<Route> mTrip; //Trip information from Source->Destination is a list of routes available.
    private handleTrip mListener;

    /* callback interface for listener(s) to get trip data */
    public interface handleTrip {
        void tripGenerationStarting();
        void tripGenerationSuccess(ArrayList<Route> trip);
    }

    public TripGenerator(handleTrip listener, String source, String destination, Context context) {
        this.mSource = source;
        this.mDestination = destination;
        this.mContext = context;
        this.mTrip = new ArrayList<>();
        this.mListener = listener;
    }

    public void startTripGenerator(){
        /* only public method here to start the tripGeneration process */

        getJSONResponse(); //start by generating raw JSON data via HTTP GET request.
    }

    private void getJSONResponse() {
        /* HTTP GET Request to retrieve raw data */

        //1: Get URL object:
        String httpURL = getStringURL();

        //2: Indicate to listener that tripGeneration is starting; they may show a progress bar here
        mListener.tripGenerationStarting();

        //3: Start AsyncTask to get raw JSON results & parse
        new HTTPGET().execute(httpURL);
    }

    @Nullable
    private String getStringURL() {
        /* Returns a GoogleMapsDirectionsAPI Request URL */

        /**
         * NOTE: Only driving directions supported for now; if that changes, need to change
         *          the URL HERE.
         * TODO: update supported transit types here
         */

        //TODO: update URL with waypoints if we choose to guide users through their venues of interest

        try {

            /* First, build a URL as a String */
            return DIRECTIONS_API_JSON_REQUEST
                    + "?origin="
                    + URLEncoder.encode(mSource, "utf8")
                    + "&destination="
                    + URLEncoder.encode(mDestination, "utf8")
                    + "&alternatives=true" //allow alternate routes of this trip: change value to true
                    + "&key=" //read Google API key from string resource:
                    + mContext.getResources().getString(R.string.google_maps_key);

            //todo: how to get more than ~3 routes from Waterloo -> Toronto? alternatives = true doesn't give a lot of alternatives

        } catch (UnsupportedEncodingException e) {
            Log.e(DEBUG_TAG, "Error: Incorrect HTTP URL encoding");
            return null;
        }
    }

    private class HTTPGET extends AsyncTask<String, Void, String> {

        protected void onPreExecute() {
            /* Code to run before executing the task (This is in main thread) */
        }

        protected String doInBackground(String... params) {
            /* perform HTTP GET request in a background thread */

            HttpURLConnection httpConnection = null;

            try {

                /* Convert stringURL into a URL object & make HTTP GET request */
                URL httpURL = new URL(params[0]);

                /* Create HTTP connection */
                httpConnection = (HttpURLConnection) httpURL.openConnection();

                /* Now read the results into a StringBuilder: */
                InputStreamReader inputStream = new InputStreamReader(httpConnection.getInputStream());

                /* TODO: Note to self, understand input buffer better once have time */
                StringBuilder inputBuffer = new StringBuilder();
                BufferedReader reader = new BufferedReader(inputStream);
                String line;
                while ((line = reader.readLine()) != null) {
                    inputBuffer.append(line).append("\n");
                }
                return inputBuffer.toString();

            } catch (MalformedURLException e) {
                Log.e(DEBUG_TAG, "Error: HTTP URL failed");
                return null;
            } catch (IOException e) {
                Log.e(DEBUG_TAG, "Error: Failed to open HTTP connection");
                return null;
            } finally {
            /* Close our HTTP connection at the end */
                if (httpConnection != null) {
                    httpConnection.disconnect();
                }
            }

        }

        protected void onProgressUpdate(Void... values) {
            /* TODO: can show progress bar for route generation here */
        }

        protected void onPostExecute(String result) {

            /* Now that we have our raw JSON data, parse it (this is in main thread) */
            parseRawJSON(result); //todo: in a new background thread?
        }

    }

    private void parseRawJSON(String rawData) {
        /* parse raw JSON data to extract information we need for route */

        //TODO: Note-To-Self: Learn how JSON parsers in greater detail; look at the JSON videos: https://www.youtube.com/watch?v=AmvuDTmt5yM&list=PLonJJ3BVjZW6CtAMbJz1XD8ELUs1KXaTD&index=39

        /* NOTE: Example of rawJSON Data:
         * https://maps.googleapis.com/maps/api/directions/json?origin=Waterloo,+ON&destination=Toronto+ON&key=AIzaSyBDb1PyehQRApT_cuK-C8WbzQF8JRB-8qA
         */

        if (rawData != null) {

            /* HTTP GET Request was successful: */
            try {
                JSONObject jsonObject = new JSONObject(rawData);

                /**
                 * IMPORTANT:
                 * - Only one route per trip [source->destination], UNLESS we add "alternatives=true" in the HTTP URL
                 * - MUST handle and display the "CopyRights" field inside "routes" field (see Route.java)
                 */

                //Populate the routes of this trip:
                JSONArray jsonRouteArray = jsonObject.getJSONArray("routes"); //get all routes of this trip
                for (int routeIndex = 0; routeIndex < jsonRouteArray.length(); routeIndex++) {
                    /* for each route of this trip */
                    JSONObject jsonRoute = jsonRouteArray.getJSONObject(routeIndex);
                    Route route = new Route();

                    //todo: refactor this into own method: (adding overview_polyline as doubles)
                    //Add the overViewPolyLinePoints to the route:
                    JSONObject routePolyLine = jsonRoute.getJSONObject("overview_polyline");
                    List<LatLng> tmp = decodePoly(routePolyLine.getString("points")); //temp to compare points in debug
                    route.setOverviewPolyLinePoints(decodePoly(routePolyLine.getString("points")));

                    //Now populate the legs of this route:
                    JSONArray jsonLegsArray = jsonRoute.getJSONArray("legs"); //Get all legs of this route
                    for (int legIndex = 0; legIndex < jsonLegsArray.length(); legIndex++){
                        /* for each leg of this route (only 1 if have no way-points) */
                        JSONObject jsonLeg = jsonLegsArray.getJSONObject(legIndex);
                        Leg leg = new Leg();

                        //todo: put all of these 3 setters into separate methods in this class for easier maintenance
                        /* Store leg distance information */
                        Distance legDistance = new Distance();
                        JSONObject jsonLegDist = jsonLeg.getJSONObject("distance");
                        legDistance.setStringDistance(jsonLegDist.getString("text"));
                        legDistance.setValueDistance(jsonLegDist.getInt("value"));
                        leg.setLegDistance(legDistance);

                        /* Store leg Starting Location */
                        PlaceLocation legStartLoc = new PlaceLocation();
                        legStartLoc.setLocAddress(jsonLeg.getString("start_address"));
                        JSONObject legStartLocLatLng = jsonLeg.getJSONObject("start_location");
                        legStartLoc.setLocLatLng(
                                legStartLocLatLng.getDouble("lat"),
                                legStartLocLatLng.getDouble("lng"));
                        leg.setLegStartLoc(legStartLoc);

                        /* Store leg Ending Location */
                        PlaceLocation legDstLoc = new PlaceLocation();
                        legDstLoc.setLocAddress(jsonLeg.getString("end_address"));
                        JSONObject legDstLocLatLng = jsonLeg.getJSONObject("end_location");
                        legDstLoc.setLocLatLng(
                                legDstLocLatLng.getDouble("lat"),
                                legDstLocLatLng.getDouble("lng"));
                        leg.setLegDstLoc(legDstLoc);

                        //Now populate the steps of this leg
                        JSONArray jsonStepsArray = jsonLeg.getJSONArray("steps");
                        for(int stepIndex = 0; stepIndex < jsonStepsArray.length(); stepIndex++){
                            /* for each step of this leg */
                            JSONObject jsonStep = jsonStepsArray.getJSONObject(stepIndex);
                            Step step = new Step();

                            //todo: refactor all of these step setters:
                            /* Store step distance information */
                            Distance stepDistance = new Distance();
                            JSONObject jsonStepDist = jsonStep.getJSONObject("distance");
                            stepDistance.setStringDistance(jsonStepDist.getString("text"));
                            stepDistance.setValueDistance(jsonStepDist.getInt("value"));
                            step.setStepDistance(stepDistance);

                            /* Store step starting location */
                            PlaceLocation stepStartLoc = new PlaceLocation();
                            JSONObject stepStartLocLatLng = jsonStep.getJSONObject("start_location");
                            stepStartLoc.setLocLatLng(
                                    stepStartLocLatLng.getDouble("lat"),
                                    stepStartLocLatLng.getDouble("lng"));
                            step.setStepStartLoc(stepStartLoc);

                            /* Store step ending location */
                            PlaceLocation stepDstLoc = new PlaceLocation();
                            JSONObject stepDstLocLatLng = jsonStep.getJSONObject("end_location");
                            stepDstLoc.setLocLatLng(
                                    stepDstLocLatLng.getDouble("lat"),
                                    stepDstLocLatLng.getDouble("lng"));
                            step.setStepDstLoc(stepDstLoc);

                            /* Store the step polyLineRoutes */
//                            JSONObject stepPolyLine  = jsonStep.getJSONObject("polyline");
//                            step.setStepPolyLinePoints(decodePoly(stepPolyLine.getString("points")));

                            //todo: assert step not null
                            leg.addLegStep(step); //store this step of our leg
                        }

                        //todo: assert leg not null
                        route.addRouteLeg(leg); //store this leg of our route
                    }

                    //todo: assert route not null AND mTrip not null
                    mTrip.add(route); //store this route into our mTrip
                    //todo: return trip info to whatever needs it
                }

                /* generation successful; pass trip (as an arrayList of routes) to whoever is listening */
                //assert mTrip not null
                mListener.tripGenerationSuccess(mTrip);

            } catch (JSONException e) {
                Log.e(DEBUG_TAG, "Error: Failed to process JSON HTTP results");
            }
        }
    }

    private List<LatLng> decodePoly(String encoded) {

        /* decode an encoded polyLine;
            from: http://stackoverflow.com/questions/14702621/answer-draw-path-between-two-points-using-google-maps-android-api-v2 */

        List<LatLng> poly = new ArrayList<>();
        int index = 0, len = encoded.length();
        int lat = 0, lng = 0;

        while (index < len) {
            int b, shift = 0, result = 0;
            do {
                b = encoded.charAt(index++) - 63;
                result |= (b & 0x1f) << shift;
                shift += 5;
            } while (b >= 0x20);
            int dlat = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
            lat += dlat;

            shift = 0;
            result = 0;
            do {
                b = encoded.charAt(index++) - 63;
                result |= (b & 0x1f) << shift;
                shift += 5;
            } while (b >= 0x20);
            int dlng = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
            lng += dlng;

            LatLng p = new LatLng( (((double) lat / 1E5)),
                    (((double) lng / 1E5) ));
            poly.add(p);
        }

        return poly;
    }

}
