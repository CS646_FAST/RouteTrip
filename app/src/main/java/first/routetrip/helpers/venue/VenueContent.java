package first.routetrip.helpers.venue;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class VenueContent {

    /**
     * An array of sample (venue) items.
     */
    public static final List<VenueItem> ITEMS = new ArrayList<>();

    /**
     * A map of sample (venue) items, by ID.
     */
    public static final Map<String, VenueItem> ITEM_MAP = new HashMap<>();

    private static final int COUNT = 25;

    static {
        // Add some sample items.
        for (int i = 1; i <= COUNT; i++) {
            addItem(createVenueItem(i));
        }
    }

    private static void addItem(VenueItem item) {
        ITEMS.add(item);
        ITEM_MAP.put(item.id, item);
    }

    private static VenueItem createVenueItem(int position) {
        return new VenueItem(String.valueOf(position), "Item " + position, makeDetails(position));
    }

    private static String makeDetails(int position) {
        StringBuilder builder = new StringBuilder();
        builder.append("Details about Item: ").append(position);
        for (int i = 0; i < position; i++) {
            builder.append("\nMore details information here.");
        }
        return builder.toString();
    }

    /**
     * A venue item representing a piece of content.
     */
    public static class VenueItem {
        public final String id;
        public final String content;
        public final String details;

        public VenueItem(String id, String content, String details) {
            this.id = id;
            this.content = content;
            this.details = details;
        }

        @Override
        public String toString() {
            return content;
        }
    }
}
