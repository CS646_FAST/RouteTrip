package first.routetrip.helpers;

import android.os.Parcel;
import android.os.Parcelable;

public class FourSquareCategory implements Parcelable{

    /**
     * Small Category object to hold a category description in a string,
     *  and an associated image's id in an int.
     * Implements the Parcelable interface so we can marshal/unmarshal this object
     *  and pass it between activities and fragments.
     * More info on Parcelable here: https://newfivefour.com/android-parcelable.html
     */

    private String name;
    private int imageID;

    /* NOTE: Remember that the constructor can be overloaded if needed */
    public FourSquareCategory(String categoryName, int categoryImageID){
        this.name = categoryName;
        this.imageID = categoryImageID;
    }

    private FourSquareCategory(Parcel in){
        /* Constructor to re-generate an object from a Parcel;
            private as it's called internally by CREATOR */

        //IMPORTANT: Access in the same order as they were written in writeToParcel!
        this.name = in.readString();
        this.imageID = in.readInt();
    }

    public String getCategoryName(){
        return this.name;
    }

    public void setCategoryName(String categoryName){
        this.name = categoryName;
    }

    public int getCategoryImgID(){
        return this.imageID;
    }

    public void setcategoryImageID(int categoryImageID){
        this.imageID = categoryImageID;
    }

    //========================================PARCELABLE============================================

    public static final Parcelable.Creator<FourSquareCategory> CREATOR =
            new Parcelable.Creator<FourSquareCategory>() {
                /* more info about this in link @ start of this class */

        public FourSquareCategory createFromParcel(Parcel in) {
            /* re-create this object from a parcel via special constructor */
            return new FourSquareCategory(in);
        }

        public FourSquareCategory[] newArray(int size) {
            /* allows an array of this object to be parceled */
            return new FourSquareCategory[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        /* writes object fields to a parcel in a particular order */
        dest.writeString(name);
        dest.writeInt(imageID);
    }
}
