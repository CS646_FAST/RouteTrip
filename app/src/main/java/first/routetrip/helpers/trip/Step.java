package first.routetrip.helpers.trip;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.android.gms.maps.model.LatLng;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * A helper class containing all information regarding a step of a leg.
 * See all available fields here: https://developers.google.com/maps/documentation/directions/intro#Steps
 * This is the most "Atomic Unit" of a route. Ie: "Turn left on University Ave onto Regina Street"
 */

public class Step implements Parcelable, Serializable {

    private Distance mStepDistance;
    private PlaceLocation mStepStartLoc;
    private PlaceLocation mStepDstLoc;
//    private List<LatLng> mStepPolyLinePoints; //Single points objects (polyline encoded) representing this step

    //duration of this step? (= Make object: value (int) and String representation)
    //human-readable-instructions? (As a string, information like: "Turn left onto University Avenue")

    public Step() {
        /* empty constructor; use setters to populate object */
    }

    private Step(Parcel in) {
        /* Constructor to re-generate an object from a Parcel;
            private as it's called internally by CREATOR */

//        mStepPolyLinePoints = new ArrayList<>();

        //IMPORTANT: Access in the same order as they were written in writeToParcel!
        this.mStepDistance = in.readParcelable(Distance.class.getClassLoader());
        this.mStepStartLoc = in.readParcelable(PlaceLocation.class.getClassLoader());
        this.mStepDstLoc = in.readParcelable(PlaceLocation.class.getClassLoader());
//        in.readList(this.mStepPolyLinePoints, LatLng.class.getClassLoader());
    }

    public Distance getStepDistance() {
        return this.mStepDistance;
    }

    public PlaceLocation getStepStartLoc() {
        return this.mStepStartLoc;
    }

    public PlaceLocation getStepDstLoc() {
        return this.mStepDstLoc;
    }

//    public List<LatLngSerializable> getStepPolyLinePoints() {
    //see trip.Route.java in how to fix these two methods should they be needed
//        return this.mStepPolyLinePoints;
//    }

//    public void setStepPolyLinePoints(List<LatLng> polyLinePoints) {
//        this.mStepPolyLinePoints = polyLinePoints;
//    }

    public void setStepDistance(Distance distance) {
        this.mStepDistance = distance;
    }

    public void setStepStartLoc(PlaceLocation start) {
        this.mStepStartLoc = start;
    }

    public void setStepDstLoc(PlaceLocation destionation) {
        this.mStepDstLoc = destionation;
    }

    //========================================PARCELABLE============================================

    public static final Parcelable.Creator<Step> CREATOR =
            new Parcelable.Creator<Step>() {
                /* more info about this in link @ start of this class */

                public Step createFromParcel(Parcel in) {
                    /* re-create this object from a parcel via special constructor */
                    return new Step(in);
                }

                public Step[] newArray(int size) {
                    /* allows an array of this object to be parceled */
                    return new Step[size];
                }
            };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        /* writes object fields to a parcel in a particular order */
        dest.writeParcelable(mStepDistance, 0);
        dest.writeParcelable(mStepStartLoc, 0);
        dest.writeParcelable(mStepDstLoc, 0);
//        dest.writeList(mStepPolyLinePoints);
    }

}


