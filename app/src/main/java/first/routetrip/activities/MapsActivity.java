package first.routetrip.activities;

import android.Manifest;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;

import java.util.ArrayList;
import java.util.Map;

import first.routetrip.R;
import first.routetrip.fragments.ItineraryList;
import first.routetrip.helpers.Persistence;
import first.routetrip.helpers.RetrieveRoute;
import first.routetrip.helpers.trip.Leg;
import first.routetrip.helpers.trip.Route;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback {

    private static final String DEBUG_TAG = MapsActivity.class.getSimpleName();


    /**
     * This is a temporary class to see how we interact with the TripGeneratorHelper; and how to display routes on maps:
     * TODO: if we want to actually guide people along a route; will we delegate that functionality to GoogleMaps?
     */

    /* somewhat of a tutorial: https://www.youtube.com/watch?v=CCZPUeY94MU
        for directions API, start at 10:43 */

    /*
        Example URL for HTTP GET request for route: (from UW to downtown TO)
         https://maps.googleapis.com/maps/api/directions/json?origin=43.475312,%20-80.552193&destination=43.650497,%20-79.386975&key=AIzaSyBUs_uJWkcGIBhFy30I78UvJPb5ho_DXB8
         or this for placeIDs instead of LatLng:
         https://maps.googleapis.com/maps/api/directions/json?origin=University%20of%20Waterloo&destination=Toronto&key=AIzaSyBUs_uJWkcGIBhFy30I78UvJPb5ho_DXB8

     */

    private GoogleMap mMap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);

        /* can add a MapFragment to code programmatically using transactions */

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this); //starts map service
    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {

        /* This is a callback method once the map is ready to be used */

        mMap = googleMap;

        mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL); //set Map types (satellite / hybrid, terain, etc..)

        /* Check for LOCATION permission, ask for it if not granted -> REQUIRED for functionality */

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

        /* TODO: Explicitly check and ask for permission here, see:
            https://blog.xamarin.com/requesting-runtime-permissions-in-android-marshmallow/
            https://inthecheesefactory.com/blog/things-you-need-to-know-about-android-m-permission-developer-edition/en */

            //For now, assume we have it or simply crash
            Toast.makeText(this, "Needs Location Permission!", Toast.LENGTH_SHORT).show();
            return;
        }

        mMap.setMyLocationEnabled(true); //set user's current location (adds a "myLocation" button on the map)

        displayRoute(); //display intent's route

    }

    public void displayRoute() {

        /* Retrieve FILENAME from intent, and retrieve it's corresponding Route object from
            internal storage. NOTE: We can retrieve FILENAME from database if needed
             on orientation change or other. */

        /* Get our saved Route object */
        String FILENAME = getIntent().getStringExtra(ItineraryList.ROUTE_FILENAME);
        RetrieveRoute retrieveRoute = new RetrieveRoute(this);
        Route route = retrieveRoute.getSavedRoute(FILENAME);
        assert route != null;

        /* have a non-null route object here, display it on map */

        ArrayList<Polyline> polyLineTripPath = new ArrayList<>();
        ArrayList<Marker> tripStartMarkers = new ArrayList<>();
        ArrayList<Marker> tripEndMarkers = new ArrayList<>();

        /* Extract first and last route leg to place markers with their location info */
        ArrayList<Leg> routeLegs = route.getRouteLegs();
        Leg startingLeg = routeLegs.get(0);
        Leg endingLeg = routeLegs.get(routeLegs.size() - 1);

        //Move & Zoom camera to starting location of first leg of this route:
        mMap.moveCamera(CameraUpdateFactory
                .newLatLngZoom(startingLeg
                                .getLegStartLoc()
                                .getLocLatLng(),
                        8)); //zoom between [2:20]

        //Add start location marker(s) in case we need future access
        tripStartMarkers.add(mMap.addMarker(new MarkerOptions()
                .title(startingLeg.getLegStartLoc().getLocAddress())
                .position(startingLeg.getLegStartLoc().getLocLatLng())));

        //Add ending location marker(s) in case we need future access
        tripEndMarkers.add(mMap.addMarker(new MarkerOptions()
                .title(endingLeg.getLegDstLoc().getLocAddress())
                .position(endingLeg.getLegDstLoc().getLocLatLng())));

        //Create and set various PolyLineObject Characteristics for this route's polyLine
        PolylineOptions polylineOptions = new PolylineOptions()
                .geodesic(true)
                .width(10);

        for (int polyIndex = 0;
             polyIndex < route.getOverviewPolyLinePoints().size();
             polyIndex++) {

            /* For each polyLine Point of our ROUTE (from overViewPolyLine JSONObject),
                add it to our polyLineOptions object; incase we want to access (and/or delete) the
                 displayed route*/
            polylineOptions.add(
                    route.getOverviewPolyLinePoints().get(polyIndex)); //have to convert from doubles to LatLng here
        }

        /* Now store this route's polyLine into an array for easy access (if needed),
            and add it to our map via the polyLineOptions object. */
        polyLineTripPath.add(mMap.addPolyline(polylineOptions));




    }

}
