package first.routetrip.helpers.trip;

import android.os.Parcel;
import android.os.Parcelable;

import java.io.Serializable;

/**
 * Helper object that represents a distance along a trip-route-leg/step. Contains:
 * 1: A String distance
 * 2: An int distance
 */
public class Distance implements Parcelable, Serializable {

    private String mStringDistance;
    private int mValueDistance; // in meters

    public Distance() {
        /* empty constructor; use setters to populate object */
    }

    private Distance(Parcel in) {
        /* Constructor to re-generate an object from a Parcel;
            private as it's called internally by CREATOR */

        //IMPORTANT: Access in the same order as they were written in writeToParcel!
        this.mStringDistance = in.readString();
        this.mValueDistance = in.readInt();
    }

    public String getStringDistance() {
        return this.mStringDistance;
    }

    public void setStringDistance(String distanceString) {
        this.mStringDistance = distanceString;
    }

    public int getValueDistance() {
        return this.mValueDistance;
    }

    public void setValueDistance(int distanceValue) {
        this.mValueDistance = distanceValue;
    }

    //========================================PARCELABLE============================================

    public static final Parcelable.Creator<Distance> CREATOR =
            new Parcelable.Creator<Distance>() {
                /* more info about this in link @ start of this class */

                public Distance createFromParcel(Parcel in) {
                    /* re-create this object from a parcel via special constructor */
                    return new Distance(in);
                }

                public Distance[] newArray(int size) {
                    /* allows an array of this object to be parceled */
                    return new Distance[size];
                }
            };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        /* writes object fields to a parcel in a particular order */
        dest.writeString(mStringDistance);
        dest.writeInt(mValueDistance);
    }

}
