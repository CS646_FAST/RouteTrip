package first.routetrip.fragments;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import first.routetrip.R;
import first.routetrip.helpers.GooglePlacesAPIHTTPGET;

/**
 * A fragment that contains all necessary code to handle user destination input.
 * Upon button click, will have to pass destination string to it's listeners.
 * <p/>
 * Next fragment in trip-selection hierarchy after this should be "CategoryInput"
 * TODO: two destination input methods: autoCompleteTextView, or picking physical location via map
 * TODO: Change autoCompleteTextView to only run queries after user stopped typing for 500ms or such.
 */

public class DestinationInput extends Fragment {

    private static final String DEBUG_TAG = DestinationInput.class.getSimpleName();
    private static final String FRAGMENT_TAG = "FragTag:";

    //Define Variables:
//    private AutoCompleteTextView userDestination;
    private EditText userDestination;
    private handleDestinationData fragmentListener; /* We define listeners to this fragment's
                                        actions here; can just as easily be a list of listeners */

    /*
     * listeners must implement this interface
     * By Polymorphism, this makes them of superType <interface> and above definition will
     * let us cast listeners to type "handleDestinationData"
     */
    public interface handleDestinationData {
        void destinationContinueButtonClicked(String userDestination);
    }

    public DestinationInput() {
        // Required empty public constructor
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d(DEBUG_TAG, "onDestroy");
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        //NOTE: we know we're attaching this to activity, therefore following cast is safe.
        //      Otherwise, this line could cause exceptions.
        Activity activity = (Activity) context;

        //We make sure listeners have successfully "identified" themselves before fragment functions
        //Otherwise, we throw an exception.

        try {
            fragmentListener = (handleDestinationData) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement handleDestinationData");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_destination_input, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

//        /*Initialize AutoCompleteTextView for userInput & set it's adapter to populate locations:*/
//        userDestination = (AutoCompleteTextView) view.findViewById(R.id.destination);
//        userDestination.setAdapter(new autoCompletePlacesAdapter(getContext(),
//                R.layout.autocomplete_list_item));
//        /* Now set onItemClickListener to record selected autocomplete item */
//        userDestination.setOnItemClickListener(autoCompleteItemClick);

        userDestination = (EditText) view.findViewById(R.id.destination);

        /* Allow soft-keyboard to push layout up;
    From: http://stackoverflow.com/questions/11292950/push-up-content-when-clicking-in-edit-text */
        getActivity().getWindow()
                .setSoftInputMode(WindowManager.LayoutParams
                        .SOFT_INPUT_STATE_HIDDEN | WindowManager.LayoutParams
                        .SOFT_INPUT_ADJUST_RESIZE);

        //Attach listener to continue button
        ImageButton continueButton = (ImageButton) view.findViewById(R.id.destinationContinueButton);
        continueButton.setOnClickListener(buttonContinueListener);

        //Attach listener to clear button
//        Button clearButton = (Button) view.findViewById(R.id.destinationClearButton);
        ImageButton clearButton = (ImageButton) view.findViewById(R.id.destinationClearButton);
        clearButton.setOnClickListener(buttonClearListener);
    }

    private void closeSoftKeyboard() {

        // TODO: first check if soft keyboard is showing
        InputMethodManager imm = (InputMethodManager) getContext()
                .getSystemService(Context.INPUT_METHOD_SERVICE);
        if (getActivity().getCurrentFocus() != null) {
            imm.hideSoftInputFromWindow(getActivity().getCurrentFocus().
                    getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        }
    }

    //========================================LISTENERS==============================================

    View.OnClickListener buttonContinueListener = new View.OnClickListener() {

        /*
         * Button click handler; will notify containing activity that button was pressed,
         * and pass the value within the editText to the containing activity.
         * It is up to the activity to handle it both events.
         * (In this case, swap this fragment to next one, and parse user's destination)
         */

        @Override
        public void onClick(View v) {

            String destination = userDestination.getText().toString();

            if (destination.isEmpty()) {
                Toast.makeText(getContext(),
                        "Please Enter a Destination!", Toast.LENGTH_SHORT).show();
            } else {
                //todo: This method is temporary for CS646; update with two methods mentioned above @ start
                /* Send button click event and userDestination to our listener(s) to handle */
                fragmentListener.destinationContinueButtonClicked(destination);
            }

        }
    };

    View.OnClickListener buttonClearListener = new View.OnClickListener() {
        /* An easy way for the user to clear any previously entered destination text */
        @Override
        public void onClick(View v) {
            userDestination.setText("");
        }
    };

//    AdapterView.OnItemClickListener autoCompleteItemClick = new AdapterView.OnItemClickListener() {
//        @Override
//        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//            closeSoftKeyboard();
//        }
//    };

    //========================================INNER CLASSES=========================================

    private class autoCompletePlacesAdapter extends ArrayAdapter<String> implements Filterable {

        /* adapter inner-class for autoCompleteTextView */
        //todo: implement autoCompleteTextView for destinations.

        private final class autoCompleteViewHolder {
            private TextView placeSuggestionText;
            private ImageView googleLogo;
        }

        private ArrayList<String> resultList;
        private int layoutResourceID;
        private GooglePlacesAPIHTTPGET placesAPI; //our placesAPI helper for HTTP GET requests.
        private LayoutInflater inflater;
        private autoCompleteViewHolder viewHolder;

        public autoCompletePlacesAdapter(Context context, int textViewResourceId) {
            super(context, textViewResourceId);
            this.layoutResourceID = textViewResourceId;
            placesAPI = new GooglePlacesAPIHTTPGET(context);
            this.inflater = LayoutInflater.from(context); //cache the inflater to avoid asking for it each time
        }

        @Override
        public int getCount() {
            return resultList.size();
        }

        @Override
        public String getItem(int position) {
            return resultList.get(position);
        }

        @Override
        public Filter getFilter() {
            Filter filter = new Filter() {

                @Override
                protected FilterResults performFiltering(CharSequence constraint) {

                    /*filters data based on provided constraint in a worker thread*/

                    /* FilterResults obj has two properties: values and count - must set them */
                    FilterResults filterResults = new FilterResults();
                    if (constraint != null) {

                        /* call the placesAPI to perform HTTP GET Request; populate filtered list */
                        resultList = placesAPI.autoCompleteDestinations(constraint.toString());

                        // Footer
                        resultList.add("footer");

                        /* Now assign ArrayList<String> results to filter */
                        filterResults.values = resultList;
                        filterResults.count = resultList.size();
                    }
                    return filterResults;
                }

                @Override
                protected void publishResults(CharSequence constraint, FilterResults results) {
                    /* worker thread passes results to this method, which passes it to UI thread. */
                    if (results != null && results.count > 0) {
                        notifyDataSetChanged();
                    } else {
                        notifyDataSetInvalidated();
                    }
                }

            };

            return filter;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            /* Show "Powered by Google" on last listEntry as per Places Requirements:
                https://developers.google.com/places/web-service/policies#logo_requirements */

            View view = convertView; //Recycle Views:

            if (view == null) {
                /* nothing to recycle: inflate view, create viewHolder and attach to view. */

                view = inflater.inflate(layoutResourceID, parent, false);
                viewHolder = new autoCompleteViewHolder();
                viewHolder.placeSuggestionText = (TextView) view.findViewById(
                        R.id.autocomplete_Text);
                viewHolder.googleLogo = (ImageView) view.findViewById(
                        R.id.autocomplete_powered_by_google);
                view.setTag(viewHolder);

            } else {
                /* re-use view, get attached viewHolder */
                viewHolder = (autoCompleteViewHolder) view.getTag();
            }

            if (resultList.get(position) != null) {
                /* as long as we have results */


                if (resultList.get(position).equals("footer")) {
                    /* for the footer, simply ignore the text all together and show the logo */
                    viewHolder.placeSuggestionText.setVisibility(View.GONE);
                    viewHolder.googleLogo.setVisibility(View.VISIBLE);
                } else {
                    /* show actual autoComplete text */
                    viewHolder.googleLogo.setVisibility(View.GONE);
                    viewHolder.placeSuggestionText.setVisibility(View.VISIBLE);
                    viewHolder.placeSuggestionText
                            .setText(resultList.get(position));
                }

            }
            return view;
        }
    }
}
