package first.routetrip.helpers;

import android.content.Context;

import first.routetrip.helpers.trip.Route;

/**
 * A simple helper object to retrieve saved Routes from the device's internal storage
 * stored at a specified FILENAME.
 */
public class RetrieveRoute {

    private static final String DEBUG_TAG = RetrieveRoute.class.getSimpleName();
    private Context mContext;

    public RetrieveRoute(Context context){
        this.mContext = context;
    }

    public Route getSavedRoute(String FILENAME){
        /* Retrieves the route object stored at FILENAME within device's internal storage */

        /* Get a PersistenceHelper object */
        Persistence persistenceHelper = new Persistence(mContext);

        /* First, retrieve the serialized route from internalStorage */
        byte[] serializedRoute = persistenceHelper
                .readFromInternalStorage(FILENAME);

        /* Then de-serialize the retrieved route: */
        assert serializedRoute != null;
        Object object = persistenceHelper.deSerialize(serializedRoute);

        //need to check if object is an instance of a route object, and then cast, otherwise throw error
        if (object instanceof Route) {
            /* Now we have our saved object; work with it */
            return (Route) object;
        } else {
            throw new AssertionError(DEBUG_TAG + "Error, failed to retrieve saved route!");
        }
    }
}
