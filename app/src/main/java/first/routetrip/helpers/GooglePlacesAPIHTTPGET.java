package first.routetrip.helpers;

import android.content.Context;
import android.support.annotation.Nullable;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;

import first.routetrip.R;

/**
 * A helper class for textView autocompletion using GooglePlacesAPI. Sends HTTP Get request
 * and parse the JSON result as an ArrayList of strings.
 * (can store "description", "place_id", etc..)
 * <p/>
 * Results may be used to suggest destination names for the AutoCompleteTextView,
 * or get place_ids and convert them to LatLng, etc..
 * <p/>
 * More Info:
 * http://codetheory.in/google-place-api-autocomplete-service-in-android-application/
 * https://developers.google.com/places/web-service/autocomplete
 *
 * todo: update this whole class; not used as of 2016-07-07
 * todo: limit request timeout
 * todo: limit route support to only CANADA for now
 *
 * NOTE: Example autocomplete places request:
 * https://maps.googleapis.com/maps/api/place/autocomplete/json?input=toron&types=(cities)&key=AIzaSyBDb1PyehQRApT_cuK-C8WbzQF8JRB-8qA
 */

public class GooglePlacesAPIHTTPGET {

    private static final String DEBUG_TAG = "PlacesHTTPGETHelper";
    /* all HttpGet requests with json output format start with this */
    private static final String PLACES_API_REQUEST_JSON
            = "https://maps.googleapis.com/maps/api/place/autocomplete/json";

    private Context context; //Need context to access string resources via getResources()

    public GooglePlacesAPIHTTPGET(Context context) {
        this.context = context;
    }

    @Nullable
    private StringBuilder apiHTTPGETRequest(String input) {

        /* This method returns the HTTP GET request JSON data as a string */
        HttpURLConnection httpConnection = null;
        StringBuilder jsonHTTPResults = new StringBuilder(); //To store our raw JSON results

        try {

            /* First, build a URL as a String */
            String stringURL = PLACES_API_REQUEST_JSON
                    + "?input="
                    + URLEncoder.encode(input, "utf8")
                    + "&types=(cities)" //filter only cities, look up more types as needed
                    + "&key=" //read Google API key from string resource:
                    + context.getResources().getString(R.string.google_maps_key);

            /* Now convert it into a URL object & make HTTP GET request */
            URL httpURL = new URL(stringURL);

            /* Create HTTP connection */
            httpConnection = (HttpURLConnection) httpURL.openConnection();

            /* Now read the results into a StringBuilder: */
            InputStreamReader inputStream = new InputStreamReader(httpConnection.getInputStream());
            int read;
            char[] buff = new char[1024];
            while ((read = inputStream.read(buff)) != -1) {
                jsonHTTPResults.append(buff, 0, read);
            }

        } catch (UnsupportedEncodingException e) {
            Log.e(DEBUG_TAG, "Error: Incorrect HTTP URL encoding");
            return null;
        } catch (MalformedURLException e) {
            Log.e(DEBUG_TAG, "Error: HTTP URL failed");
            return null;
        } catch (IOException e) {
            Log.e(DEBUG_TAG, "Error: Failed to open HTTP connection");
            return null;
        } finally {
            /* Close our HTTP connection at the end */
            if (httpConnection != null) {
                httpConnection.disconnect();
            }
        }

        return jsonHTTPResults;
    }

    public ArrayList<String> autoCompleteDestinations(String input) {

        ArrayList<String> results = null; //ArrayList to hold descriptions of each prediction

        /* Now that we have our JSON-HTTP-Results, parse it and populate the results ArrayList<> */
        try {

            /*first get the HTTP results as a string*/
            StringBuilder jsonHTTPResults = apiHTTPGETRequest(input);

            /*make sure no errors occurs (ie: StringBuilder isn't null*/
            if (jsonHTTPResults != null) {
                JSONObject jsonObject = new JSONObject(jsonHTTPResults.toString());

                //we're only interested in the predictions section:
                JSONArray jsonPredictionsArray = jsonObject.getJSONArray("predictions");

            /* so we create our results ArrayList to be as big as # of predictions */
                results = new ArrayList<>(jsonPredictionsArray.length());
                for (int i = 0; i < jsonPredictionsArray.length(); i++) {
                /* for each prediction, extract and store it's description value (as a string) */
                    //todo: can just as easily store the "place_id" instead of "description":
                    results.add(jsonPredictionsArray.getJSONObject(i).getString("description"));
                }
            } else {
                /* we encountered an HTTP GET error, return a null result list */
                return null;
            }

        } catch (JSONException e) {
            Log.e(DEBUG_TAG, "Error: Failed to process JSON HTTP results");
        }

        return results;
    }


}
