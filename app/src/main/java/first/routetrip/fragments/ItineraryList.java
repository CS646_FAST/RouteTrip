package first.routetrip.fragments;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.model.LatLng;

import java.util.ArrayList;

import first.routetrip.R;
import first.routetrip.activities.MainActivity;
import first.routetrip.activities.MapsActivity;
import first.routetrip.helpers.FourSquareCategory;
import first.routetrip.helpers.Persistence;
import first.routetrip.helpers.TripGenerator;
import first.routetrip.helpers.sql.TripDatabase;
import first.routetrip.helpers.trip.Route;

/**
 * This fragment gets the userDestination and selectedCategories information as arguments, and
 * retrieves device's current location as the userOrigin location. Generates and displays trip
 * information in a cardView.
 * Users may then save, view more info, and start their trip.
 * todo: create a "trip" object, make it a singleton (see how that would work AFTER functionality done)
 * todo: async task database initialization ( see book example )
 * <p/>
 * Reference for getting current LatLng of device:
 * http://stackoverflow.com/questions/17519198/how-to-get-the-current-location-latitude-and-longitude-in-android
 */

public class ItineraryList extends Fragment implements
        TripGenerator.handleTrip,
        GoogleApiClient.OnConnectionFailedListener,
        GoogleApiClient.ConnectionCallbacks {

    private static final String DEBUG_TAG = ItineraryList.class.getSimpleName();
    private static final String FRAGMENT_TAG = "FragTag:";
    //    private static final String TMP_ORIGIN_LOCATION_WATERLOO = "Waterloo, Ontario, Canada";
    public static final String ROUTE_FILENAME = "routeFile";

    private String mUserDestination;
    private LatLng mUserOrigin;
    private ArrayList<FourSquareCategory> mSelectedCategories;
    private RecyclerView mRecyclerView;
    private ArrayList<Route> mTrip; //our "DataModel"
    private ProgressDialog mProgressDialog;
    private TripGenerator mTripGenerator; //handler to generate a trip
    private GoogleApiClient mGoogleApiClient;
    private TripDatabase mDbHelper;

    public ItineraryList() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_itinerary_list, container, false);

        //Retrieve Parameters:
        Bundle bundle = getArguments();
        mUserDestination = bundle.getString(MainActivity.FRAG_PARAMETER_DESTINATION); //get user destination
        mSelectedCategories = bundle.getParcelableArrayList(MainActivity.FRAG_PARAMETER_CATEGORIES); //get selected category ArrayList

        /* Assert that two parameters aren't null */
        assert mUserDestination != null;
        assert mSelectedCategories != null;

        /* Create GoogleApiClient object for LocationServices to get Origin LatLng */
        mGoogleApiClient = new GoogleApiClient.Builder(getContext())
                /* The next two lines tell the new client that
                    “this” current class will handle connection stuff */
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                /* Fourth line adds the LocationServices API
                    endpoint from GooglePlayServices */
                .addApi(LocationServices.API)
                .build();

        /* Open GoogleApiClient connection */
        mGoogleApiClient.connect();

        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        /* Get references to RecyclerView */
        mRecyclerView = (RecyclerView) view
                .findViewById(R.id.itineraryRecyclerView);

        /* After onResume() is called; will connect to GoogleApiClient
            to get user's current Location; rest of setup is handled in onConnected */

    }

    @Override
    public void onPause() {
        super.onPause();
        Log.d(DEBUG_TAG, "onPause");

        /* When fragment is paused, close GoogleAPIConnection if connected */
        if (mGoogleApiClient.isConnected()) {
            mGoogleApiClient.disconnect();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d(DEBUG_TAG, "onDestroy");
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.d(DEBUG_TAG, "onResume");

        //todo: figure out if this connection resume is needed; if fragment is not being reused, won't be needed.. BUT what about when we come back from MapActivity?

        /* When fragment is resumed, restore GoogleAPIConnection if not already */
        if (!mGoogleApiClient.isConnected()) {
            mGoogleApiClient.connect();
        }
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        /* Failed to connect to GoogleApiClient; cause an error and display ErrorCode */
        Log.e(DEBUG_TAG, "GoogleApiClient failed to connect with code " + connectionResult
                .getErrorCode());
        Toast.makeText(getContext(), "Failed to connect to GoogleApiClient!, Please re-start.",
                Toast.LENGTH_LONG).show();
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        /* Successfully connected to GoogleApiClient; populate RecyclerView with objects */

        /* Explicitly indicate Recycler-Adapter won't change recyclerView size;
            -> improves performance */
        mRecyclerView.setHasFixedSize(true);

        /* Create a LayoutManager (Linear) and assign it to the RecyclerView */
        RecyclerView.LayoutManager linearLayoutMngr = new LinearLayoutManager(getContext());
        mRecyclerView.setLayoutManager(linearLayoutMngr);

        /* Set a default item animator to the RecyclerView */
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());

        /* Check for LOCATION permission, ask for it if not granted */
        if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            /* TODO: Explicitly check and ask for permission here, see:
            https://blog.xamarin.com/requesting-runtime-permissions-in-android-marshmallow/
            https://inthecheesefactory.com/blog/things-you-need-to-know-about-android-m-permission-developer-edition/en */

            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            Toast.makeText(getContext(), "Need Location Permissions", Toast.LENGTH_SHORT).show();
            return;
        }

        /* OriginLocation: Get user's current location as the Origin Location of the trip & store as LatLng */
        //todo: properly check for connection before getting location: http://stackoverflow.com/questions/17519198/how-to-get-the-current-location-latitude-and-longitude-in-android
        Location location = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
        mUserOrigin = new LatLng(location.getLatitude(), location.getLongitude());

        /* generate a trip with mUserDestination */
        mTripGenerator = new TripGenerator(
                this,
                mUserOrigin.latitude + "," + mUserOrigin.longitude, //LatLng object from LocationServices as a String
//                TMP_ORIGIN_LOCATION_WATERLOO, //use this when can't get device location
                mUserDestination,
                getContext());
        mTripGenerator.startTripGenerator();

    }

    @Override
    public void onConnectionSuspended(int i) {
    }

    @Override
    public void tripGenerationStarting() {
        /* tripGeneration is starting, show a mProgressDialog to the user */

        mProgressDialog = new ProgressDialog(getContext());
        mProgressDialog.setMessage("Generating Trip, Please Wait");
        mProgressDialog.setCancelable(false); //prevent user from closing it
        mProgressDialog.show();
    }

    @Override
    public void tripGenerationSuccess(ArrayList<Route> trip) {
        /* A trip was successfully generated, close progress dialog & save trip field */

        this.mTrip = trip;
        assert mTrip != null;

        /* If mTrip contains no routes but reached this point,
            it means there were no drivable routes found to destination */
        //todo: handle such a case
        if (mTrip.size() == 0) {
            /* No trips found; todo: check MaterialDesign and display a "blank data screen" */
            Toast.makeText(getContext(), "No Trips Found for: " + mUserDestination,
                    Toast.LENGTH_LONG).show();
            mProgressDialog.dismiss();
            return;
        }

        /* Also close GoogleApiClient now that we've got the trip object */
        if (mGoogleApiClient.isConnected()) {
            mGoogleApiClient.disconnect();
        }

        //todo: need to incorporate trip info into foursquare now (use mSelectedCategories)

        /* Create recyclerView adapter with trip info and attach it to the recyclerView */
        RecyclerViewTripDisplayAdapter adapter = new RecyclerViewTripDisplayAdapter(mTrip, getContext());
        mRecyclerView.setAdapter(adapter);

        /* Get a reference to our SQLiteDatabaseHelper */
        mDbHelper = new TripDatabase(getContext());

        mProgressDialog.dismiss();
    }

    private class RecyclerViewTripDisplayAdapter extends
            RecyclerView.Adapter<RecyclerViewTripDisplayAdapter.ItineraryViewHolder> {

        private ArrayList<Route> tripData;
        private LayoutInflater layoutInflater;

        public RecyclerViewTripDisplayAdapter(ArrayList<Route> tripData, Context context) {
            /* RecyclerView-Adapter constructor takes
                in dataSet for the RecyclerView, and context to
                 cache a LayoutInflater so we don't ask for it multiple times */

            //todo: assert tripData != null (NOTE: also assert in all other inner classes of other .java files)
            assert tripData != null;
            this.tripData = tripData;
            this.layoutInflater = LayoutInflater.from(context);
        }

        public final class ItineraryViewHolder extends RecyclerView.ViewHolder {
            /* RecyclerView ViewHolder inner-class */

            //todo: finish declaring views to save
            private TextView itineraryPositionTextDisplay;
            private TextView itineraryDestinationTextDisplay;
            private Button saveItineraryButton;
            private Button startItineraryButton;
            private Button moreInfoItineraryButton;

            public ItineraryViewHolder(View itemView) {
                /* RecyclerView-ViewHolder constructor takes in
                    an inflated-itemView that contains views to cache
                     (view for each row of the RecyclerView) */
                super(itemView);

                /* NOTE: Always have access to layout's RootView
                    in ANY RecyclerView.ViewHolder via the "itemView"
                     parameter; no need to explicitly store */

                //todo: finish saving views after they have been declared

//                cardView = (CardView) itemView //this might have to go, depending on rootView of card.
//                        .findViewById(R.id.itineraryCardView);

                //The CardView view is the itemView, see "NOTE" above.

                itineraryPositionTextDisplay = (TextView) itemView
                        .findViewById(R.id.itineraryRoutePositionText);

                itineraryDestinationTextDisplay = (TextView) itemView
                        .findViewById(R.id.itineraryRouteDestinationText);

                saveItineraryButton = (Button) itemView
                        .findViewById(R.id.itinerarySaveButton);

                startItineraryButton = (Button) itemView
                        .findViewById(R.id.itineraryStartButton);

                moreInfoItineraryButton = (Button) itemView
                        .findViewById(R.id.itineraryMoreInfoButton);

            }
        }

        @Override
        public ItineraryViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            /* Inflates a view from layout that will
                be used for each RecyclerView-row, and creates
                 (+ returns) a new RecyclerView-ViewHolder for
                  the inflated view */

            View itemView = layoutInflater
                    .inflate(R.layout.card_itinerary_route, parent, false);

            return new ItineraryViewHolder(itemView);
        }

        @Override
        public void onBindViewHolder(ItineraryViewHolder holder, int position) {
            /* This method is used to populate ViewHolder-views with
                appropriate data. Think of the "getView" method
                 from simple adapters. */

            //NOTE: each position is a route within our tripData
            final Route tripRoute = tripData.get(position);
            assert tripRoute != null;

            //Get a string representation of this route for a fileName when/if storing it.
            //Extract HashCode of Destination for unique database filenames
            //todo: FUNCTIONALITY BREAKING BUG HERE: position is not a reliable method to uniquely identify an object, need to add a field to the Route Object and uniquely keep track of it there, see: http://stackoverflow.com/questions/8938528/how-do-i-get-a-unique-id-per-object-in-java
            //todo:                                 Displaying "route 1" using posistion is fine, but need to uniquely ID each route object when checking if it's stored / the current trip. use that unique id as the FILENAME
            //todo:                                 Also messes up delete button on savedRoutes.java.

            //temporary "unique" FileNames. Find a better method
            /* NOTE: "routePosition" and "routeDestination"
                (any strings that will be used for .hashCode()) MUST BE FINAL STRINGS.
                -> To keep consistent hashCodes. */

//            final String routePosition = "Route " + Integer.toString(position + 1); // ex: "Route 2"
//            final String routeDestination = Integer.toString(("Destination = " + tripRoute.getRouteLegs() // ex: "Destination = Toronto, Ontario, Canada"
//                    .get(tripRoute.getRouteLegs().size() - 1).getLegDstLoc().getLocAddress()).hashCode());


            StringBuilder routePosition = new StringBuilder();
            routePosition
                    .append("Route ")
                    .append(Integer.toString(position + 1));

            StringBuilder routeDestination = new StringBuilder();
            routeDestination
                    .append("Destination = ")
                    .append(tripRoute.getRouteLegs().get(tripRoute.getRouteLegs().size() - 1).getLegDstLoc().getLocAddress());

            StringBuilder routePositionLabel = new StringBuilder(); // ex: "Route 2 - To: "
            routePositionLabel
                    .append(routePosition)
                    .append(" - To: ");

            StringBuilder routeDestinationLabel = new StringBuilder(); // ex: "Toronto, Ontario, Canada"
            routeDestinationLabel
                    .append(tripRoute.getRouteLegs()
                            .get(tripRoute.getRouteLegs().size() - 1).getLegDstLoc().getLocAddress());//destination address at last leg of trip

//            final String routeDestinationHash = Integer.toString(routeDestination.hashCode()); // ex: "123456789"

            final StringBuilder fileName = new StringBuilder();
            fileName
                    .append(routePosition)
                    .append("(") //must contain parentheses: (<text>) because when reading, we remove everything from within parentheses!
                    .append(routeDestinationLabel)
                    .append(Integer.toString(position + 1))
                    .append(")");

            //Create unique filename based on Route Position + Route Destination Address (NOTE: THis is only unique as long as destination STRING is unique; (adding commas or other characters changes it), see todo below
            //TODO: IMPORTANT; FIND ANOTHER WAY TO MAKE FILENAMES FOR INTERNAL STORAGE UNIQUE BASED ON ROUTE INDEX & DESTINATION (maybe hash of LAT/LNG of position/destination)? -> DONT USE ORIGIN, or all trips will be unique b/c of current position as location
            //TODO: use three-tuple fields to generate UNIQUE UUID: see object -> uuid reference page
            //TODO: see similarities between this and "SavedRoutes.java" fragment; refactor them into a helper. DO NOT HAVE SAME CODE FOR SAME PURPOSE IN TWO PLACES
            //TODO: also refactor onClickListeners into helpers here; start and more info share functionality with the "SavedRoutes.java" buttons (inside "onclicklisteners" package)
            //todo: update below String with UUID, see todo above

//            final String FILENAMEwithHASH = routePosition + "(" + routeDestinationHash + ")"; // ex: "Route 1(123456789)" = "unique" fileName
            //need to append "SAVE)" or "CURRENT)" before using for now.. (we seriously need to refactor this once I get the time)

            //display some temp info about this route:
            holder.itineraryPositionTextDisplay.setText(routePositionLabel);
            holder.itineraryDestinationTextDisplay.setText(routeDestinationLabel);

            /* SaveItinerary button; attach a listener */
            holder.saveItineraryButton.setOnClickListener(new View.OnClickListener() {
                /* When the user presses the "Save Itinerary" button, we
                    will save all trip information (route at index "position") */

                @Override
                public void onClick(View v) {
                    /* saves currently selected route for future access */

                    /* First make sure FILENAME doesn't exist in database
                        BEFORE we write to internal storage or we will eventually get an SQLException
                         from UNIQUE fields and write to internal storage for no reason */

//                    String FILENAMEwithHASH_SaveRoute = FILENAMEwithHASH + "SAVE)";//todo: update this with UUID, see todo above
//                    String FILENAMEwithHASH_SaveRoute = FILENAMEwithHASH;
                    if (mDbHelper.checkIfSavedRouteExists(fileName.toString())) {
                        /* FILENAME already stored */
                        Toast.makeText(getContext(), "Failed - Already Saved", Toast.LENGTH_SHORT).show();
                        //todo: update toasts with something better (stay too long, and successive toasts overlap in time)
                        return;
                    }

                    if (saveRoute(tripRoute, fileName.toString())) {
                        Toast.makeText(getContext(), "Saved Successfully!", Toast.LENGTH_SHORT).show();
                    } else {
                        /* We failed to save a route to the database; give error and crash app */
                        throw new AssertionError(DEBUG_TAG + "ERROR: Failed to storeRoute; check log.e for point of failure");
                    }

                }
            });

            /* StartItinerary button; attach a listener */
            holder.startItineraryButton.setOnClickListener(new View.OnClickListener() {
                /* When the user pressed the "Start Itinerary" button,
                    we want to choose the route at index "position" as the
                     selected route and start their trip */

                //TODO: IMPORTANT; delegate trip to google maps via maps activity

                @Override
                public void onClick(View v) {
                    /* temporarily (see above), we will start the MapsActivity that shows the route, and
                        may also delegate directions to gMaps. We store the FILENAMEwithHASH accessor into
                        database. We can then access it whenever to retrieve FileHandle and get
                        it's corresponding RouteObject from internal storage */

                    //TODO: delegate ALL database stuff to asynctask; as is.. we're producing too many frames on main thread; see below
                    // I/Choreographer: Skipped 33 frames!  The application may be doing too much work on its main thread.

                    /* First check what is currently stored inside the CurrentTable of our database.
                        If it's the same thing, the user is already on this trip.
                        If it is not, the user must want to start a new trip: delete old one and update */

//                    final String FILENAMEwithHASH_CurrentRoute = FILENAMEwithHASH;// + "CURRENT)"; //todo: update this with UUID, see todo above
                    final String FILENAMEwithHASH_CurrentRoute = fileName.toString();
                    final String storedInCurrentTable = mDbHelper.checkCurrentTableFileName();
                    if (storedInCurrentTable.compareTo(FILENAMEwithHASH_CurrentRoute) == 0) {
                        /* FILENAME already stored; above comparison == 0 means
                            strings are lexicographically equal */
                        Toast.makeText(getContext(), "Already on this route!", Toast.LENGTH_SHORT).show();
                        //todo: user should go to navItem "continueRoute" which basically starts mapsActivity for now with the CurrentRoute... todo: update properly based on whatever function we want
                        //todo: add alert dialog telling them to access the current route via nav? or ask them if they wanna go to current route via alert dialog "yes" button
                    } else {

                        final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getContext());
                        alertDialogBuilder.setMessage("This will replace any current trip. \n\nContinue?");
                        alertDialogBuilder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                /* continue starting trip */

                                /* Selecting a new CurrentRoute; first delete old one from
                                    the device's internal storage (handle: storedInCurrentTable) */

                                String[] storedFiles = getContext().fileList(); //tmp; see stored strings:
                                //TODO: This next line creates a bug, see test-case on paper, fix is to have different fileName handles in "SavedTrips" and "CurrentTrips"
                                //BUG: If we have a current route NOT saved, (so it's in currentRoute, and NOT savedRoutes) and then we save it (now it's in both tables), and start a new route (the "storedInCurrentTable" is also inside savedRoutes database table, however next line deletes it from internal storage and when savedRoutes table searches for it, won't find it) -> fix: need separate filehandles for "current" trip, so we only delete from internal storage on next line if it has the "current" trip handle.
                                boolean deleted = getContext().deleteFile(storedInCurrentTable); //todo: handle return boolean value if needed
//                                storedFiles = getContext().fileList(); //tmp; see stored strings:

                                /* Save selected Route into "CurrentTrip" Database Table. */
                                if (setCurrentRoute(tripRoute, FILENAMEwithHASH_CurrentRoute)) {
                                /* Once we persisted the current Route;
                                    start an intent with the FILENAMEwithHASH string to MapsActivity */

                                    //tmp: clear fragment backstack todo: once this is refactored into helper from MainActivity, simply call that helper method
                                    FragmentManager manager = getActivity().getSupportFragmentManager();
                                    if (manager.getBackStackEntryCount() > 0) {
                                        FragmentManager.BackStackEntry first = manager.getBackStackEntryAt(0);
                                        manager.popBackStack(first.getId(), FragmentManager.POP_BACK_STACK_INCLUSIVE);
                                    }

                                    Intent intent = new Intent(getActivity(), MapsActivity.class);
                                    intent.putExtra(ROUTE_FILENAME, FILENAMEwithHASH_CurrentRoute);
                                    getActivity().startActivity(intent);
                                } else {
                                    /* We failed to persist selectedRoute; give error and crash app */
                                    throw new AssertionError(DEBUG_TAG + "ERROR: Failed to setCurrentRoute; check log.e for point of failure");
                                }

                            }
                        });
                        alertDialogBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                /* don't do anything */
                            }
                        });
                        alertDialogBuilder.show();
                    }

                }
            });

            /* MoreInfo button; attach a listener */
            holder.moreInfoItineraryButton.setOnClickListener(new View.OnClickListener() {
                /* Pressing the more info button will launch a separate activity
                    that will display route info (start/end, distance, etc..)
                     and will also display venue information along this route.

                     todo IMPORTANT:
                      (use google places to get pictures, etc..)
                       --> SIMILAR to how clicking on a place on
                       google maps bring up the dialog with more info. */

                @Override
                public void onClick(View v) {
                    //todo - open new activity; display info: origin/destination/distance/venues/etc...:
                    Toast.makeText(getContext(), "moreInfo-todo", Toast.LENGTH_SHORT).show();


                }
            });
        }

        @Override
        public int getItemCount() {
            /* Simply returns the number of data items (ie: number of routes for this trip) */
            return tripData.size();
        }

        //TODO: REFACTOR THESE TWO METHODS INTO HELPERS call it "HandleRoute"
        private boolean saveRoute(Route tripRoute, String FILENAME) {
            /* Save selected route as a file onto device's internal storage for later access
               Returns true is file successfully saved, false otherwise.
                NOTE: Filename should be unique! */

            /*  more info about internal storage here:
                https://developer.android.com/guide/topics/data/data-storage.html#filesInternal
                credits for serialization / deserialization here:
                http://stackoverflow.com/questions/7803762/storing-large-arraylists-to-sqlite */

            //TODO: with destination as a hash in the file name, this ignores the origin location. So can't save a trip from two different origins to save place.

            /* Get a PersistenceHelper object */
            Persistence persistenceHelper = new Persistence(getContext());

            /* First serialize the Route object */
            byte[] serializedTripRoute = persistenceHelper.serialize(tripRoute);
            assert serializedTripRoute != null;

            /* Now save it to the device's internal storage */
            if (!persistenceHelper.storeToInternalStorage(serializedTripRoute, FILENAME)) {
                return false;
            }

            /* Store file-name into database to keep track of all saved routes
                    within device's internal storage */
            return persistenceHelper.storeSavedRouteToDB(FILENAME);

        }

        private boolean setCurrentRoute(Route tripRoute, String FILENAME) {
            /* When the user selects a route to start it; we persist it on the device's
                internal storage. We save the FileName accessor for later usage. */

            /* Get a PersistenceHelper object */
            Persistence persistenceHelper = new Persistence(getContext());

            /* First serialize the Route object */
            byte[] serializedTripRoute = persistenceHelper.serialize(tripRoute);
            assert serializedTripRoute != null;

            /* Now save it to the device's internal storage */
            if (!persistenceHelper.storeToInternalStorage(serializedTripRoute, FILENAME)) {
                return false;
            }

            /* Store file-name into database to keep track of currentRoute
                within device's internal storage */
            return persistenceHelper.storeCurrentRouteToDB(FILENAME);
        }

    }

}
