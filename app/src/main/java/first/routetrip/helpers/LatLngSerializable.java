package first.routetrip.helpers;

import android.os.Parcel;
import android.os.Parcelable;

import java.io.Serializable;
import java.util.ArrayList;

import first.routetrip.helpers.trip.Leg;

/**
 * Temporary wrapper class for the LatLng class, since that one doesn't implement Serializable;
 * Create Google's LatLng object using these double values.
 */
public class LatLngSerializable implements Parcelable, Serializable{

    public double mLatitude;
    public double mLongitude;

    public LatLngSerializable(double lat, double lng){
        this.mLatitude = lat;
        this.mLongitude = lng;
    }

    private LatLngSerializable(Parcel in) {
        /* Constructor to re-generate an object from a Parcel;
            private as it's called internally by CREATOR */

        //IMPORTANT: Access in the same order as they were written in writeToParcel!
        this.mLatitude = in.readDouble();
        this.mLongitude = in.readDouble();
    }

    //========================================PARCELABLE============================================

    public static final Parcelable.Creator<LatLngSerializable> CREATOR =
            new Parcelable.Creator<LatLngSerializable>() {
                /* more info about this in link @ start of this class */

                public LatLngSerializable createFromParcel(Parcel in) {
                    /* re-create this object from a parcel via special constructor */
                    return new LatLngSerializable(in);
                }

                public LatLngSerializable[] newArray(int size) {
                    /* allows an array of this object to be parceled */
                    return new LatLngSerializable[size];
                }
            };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        /* writes object fields to a parcel in a particular order */
        dest.writeDouble(mLatitude);
        dest.writeDouble(mLongitude);
    }

}
